import { LoginService } from './../../providers/login/login.service';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { LoadingController, AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage {
  title: string = this.translate.instant('FORGOT_TITLE');
  form: any = {
    'email': null,
    'email2': null,
    'code': null,
    'password': null,
    'repeatPassword': null
  }

  errorMsgEmail: string = null;
  errorMsgChangePassword: string = null;
  isLoading: boolean = true;
  loading: any;
  cardSelected: string = 'email';

  constructor(
    private router: Router,
    private loadingController: LoadingController,
    private translate: TranslateService,
    private loginService: LoginService,
    private alertController: AlertController
  ) {
  }

  ionViewWillEnter() {
    this.cardSelected = 'email';
    // this.presentLoading();
  }

  changeCardSelected(value: string){
    this.cardSelected = value;
  }

  ionViewWillLeave() {
    this.form.email = null;
    this.form.code = null;
    this.form.password = null;
    this.form.repeatPassword = null;
    this.errorMsgChangePassword = null;
    this.errorMsgEmail = null;
    this.isLoading = true;
  }

  async presentLoading() {
    this.isLoading = true;
    this.loading = await this.loadingController.create({
      message: ''
    });
    return await this.loading.present();
  }

  dismissLoading() {
    this.loading.dismiss();
    this.isLoading = false;
  }

  checkMail() {
    if (!this.validateEmail(this.form.email)) {
      return false;
    } else {
      return true;
    }
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  checkForm1IsValid(){
    if (this.validateEmail(this.form.email)) {
      this.errorMsgEmail = null;
      return true;
    }
    this.errorMsgEmail = 'Error validating email';
    this.presentAlertError('GENERIC_POPUP_BODY');
    return false;
  }

  checkForm2IsValid(){
    this.errorMsgChangePassword = null;
    if (!this.validateEmail(this.form.email2)) {
      this.errorMsgChangePassword = 'Email wrong';
    }
    if(this.form.password != this.form.repeatPassword){
      this.errorMsgChangePassword = 'Passwords are diferents';
    }
    if(!this.form.code){
      this.errorMsgChangePassword = 'Error with code';
    }
    if(this.errorMsgChangePassword){
      this.presentAlertError('GENERIC_POPUP_BODY');
      return false;
    }
    return true;
  }

  sendCode() {
    if(this.checkForm1IsValid()){
      this.presentLoading();
      this.loginService.resetPasswordSendCode(this.form.email).subscribe((data: any) => {
        this.dismissLoading();
        this.errorMsgEmail = null;
        this.presentAlertCodeSend();
      }, error => { 
        this.presentAlertError('GENERIC_POPUP_BODY');
        this.errorMsgEmail = 'Error with email';  console.error(error); this.dismissLoading(); });
    }
  }

  sendPassword() {
    if (this.checkForm2IsValid()) {
      this.errorMsgChangePassword = null;
      this.presentLoading();
      this.loginService.resetPassword(this.form.email2, this.form.code, this.form.password).subscribe((data: any) => {
        this.dismissLoading();
        this.presentAlertPasswordChanged();
      }, error => { 
        this.errorMsgChangePassword='Error changing password'; 
        this.presentAlertError('GENERIC_POPUP_BODY');
        console.error(error); this.dismissLoading(); });

    } else {
      this.errorMsgChangePassword = 'Error changing password';
    }
  }


  async presentAlertPasswordChanged() {
    const alert = await this.alertController.create({
      header: this.translate.instant('GENERIC_POPUP_HEADER_OK'),
      buttons: [
        {
          text: this.translate.instant('FORGOT_MESSAGE_RESET_OK'),
          handler: () => {
            this.goToLogin();
          }
        }
      ]
    });
    await alert.present();
  }


  async presentAlertCodeSend() {
    const alert = await this.alertController.create({
      header: this.translate.instant('FORGOT_MESSAGGE_SENT'),
      subHeader: this.translate.instant('FORGOT_MESSAGGE_SENT_SUBLINE'),
      buttons: [this.translate.instant('GENERIC_POPUP_BUTTON_OK')]
    });
    await alert.present();
  }

  async presentAlertError(error:string = this.translate.instant('GENERIC_POPUP_BODY')) {
    const alert = await this.alertController.create({
      header: this.translate.instant('GENERIC_POPUP_HEADER_ERROR'),
      subHeader: this.translate.instant(error),
      buttons: [this.translate.instant('GENERIC_POPUP_BUTTON_ERROR')]
    });
    await alert.present();
  }

  goToLogin() {
    this.router.navigateByUrl('login');
  }

}