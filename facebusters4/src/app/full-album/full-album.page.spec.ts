import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FullAlbumPage } from './full-album.page';

describe('FullAlbumPage', () => {
  let component: FullAlbumPage;
  let fixture: ComponentFixture<FullAlbumPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FullAlbumPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FullAlbumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
