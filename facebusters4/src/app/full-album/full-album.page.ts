import { TranslateService } from '@ngx-translate/core';
import { LoadingController } from '@ionic/angular';
import { PostService } from './../../providers/post-service/post-service';
import { StoreService } from './../../providers/store/store';
import { Album, Photo, User, Location, Tag, HashTag } from './../../models/models';
import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-full-album',
  templateUrl: './full-album.page.html',
  styleUrls: ['./full-album.page.scss'],
})
export class FullAlbumPage {
  @Input() album: Album;
  public isMobile: boolean;
  public loading: any;
  public isLoading: boolean = false;

  constructor(
    private translate: TranslateService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private _store: StoreService,
    private _postService: PostService,
    private loadingController: LoadingController
  ) {
    this.isMobile = this._store.get('isMobile');
  }

  getAlbumInfo(pkAlbum: string){
    this._postService.getAlbum(pkAlbum).subscribe((data: any) => {
      this.album.title = data.title;
      this.album.dateRange = data.date_range;
      this.getAllPicturesOfAlbum(this.album.id);
    }, error => { console.error(error); this.dismissLoading(); });
  }

  ionViewWillLeave(){
    this.isLoading = true;
    this.album = null;
  }

  async presentLoading() {
    this.isLoading = true;
    this.loading = await this.loadingController.create({
      message: this.translate.instant('Loading...')
    });
    return await this.loading.present();
  }

  dismissLoading() {
    this.loading.dismiss();
    this.isLoading = false;
  }

  getAllPicturesOfAlbum(identifier: any) {
    this._postService.getPicturesOfAlbum(identifier).subscribe((data: any) => {
      this.album.photos = [];
      data.results.forEach(el => {
        this.album.photos.push(this.initPhoto(el));
      });
      this.sortArray('order', this.album.photos);
      this.dismissLoading();
    }, error => { console.error(error); this.dismissLoading(); });
  }

  sortArray(property, array) {
    property = property.split('.');
    var l = property.length;
    return array.sort(function (a, b) {
      var i = 0;
      while (i < l) {
        a = a[property[i]];
        b = b[property[i]];
        i++;
      }
      return a < b ? -1 : 1;
    });
  }

  initPhoto(data: any) {
    const photo = new Photo();
    photo.id = data.identifier;
    photo.title = data.title;
    photo.image = data.image;
    photo.thumbnail = data.image_thumbnail;
    photo.isPublic = data.public;
    photo.createdAt = data.created_at;
    photo.hashTags = this.initHastags(data.image_tags);
    photo.location = this.initLocation(data);
    photo.user = this.initUser(data.user_info);
    photo.filter = data.picture_filter;
    if (!photo.filter) { photo.filter = ''; }
    return photo;
  }
  initUser(data: any) {
    let user: User = new User();
    user = new User();
    if (data) {
      user.avatar = data.avatar_thumbnail;
      user.nickname = data.username;
      user.id = data.identifier;
    }
    return user;
  }
  initLocation(data: any) {
    let location: Location = new Location();
    location.address = data.address_en;
    location.latitude = data.meta_location.latitude;
    location.longitude = data.meta_location.longitude;
    return location;
  }
  initHastags(data: any[]) {
    let hashTags: HashTag[] = [];
    data.forEach(t => {
      const hashtag = new HashTag();
      hashtag.hashTag = t;
      hashTags.push(hashtag);
    });
    return hashTags;
  }

  goToDetailPhoto(photo: Photo) {
    // this.router.navigateByUrl('detail-photo/'+ photo.id);
  }

}
