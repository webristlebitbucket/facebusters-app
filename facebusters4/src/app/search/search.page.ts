import { ModalSearchAddressComponent } from './../shared/modal-search-address/modal-search-address.component';
import { PostService } from './../../providers/post-service/post-service';
import { UserService } from './../../providers/user-service/user-service';
import { DataService } from './../../services/data-observable';
import { Component } from '@angular/core';
import { AlertController, ModalController, Events } from '@ionic/angular';
import { StoreService } from './../../providers/store/store';
import { User, HashTag, Location } from './../../models/models';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { SearchAddressService } from './../../providers/search/search';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'page-search',
    templateUrl: 'search.page.html',
    styleUrls: ['./search.page.scss']
})
export class SearchPage {
    filters: any = {
        'startDate': new Date(Date.now() - (864e5*365)).toISOString(),
        'endDate': new Date().toISOString(),
        'startTime': null,
        'endTime': null,
        'address': null,
        'geometry': null,
        'range': 50,
        'hashtags': [],
        'user': null
    };

    public inputAddress: string = null;
    public addressSelected: any;
    public myRange = { "lower": "0", "upper": "20" };
    public isMobile: boolean;
    public segmentSelected: string = 'photos' || 'users';

    //search Locations/Address
    addressResults: any[] = [];
    location: Location = null;


    //search HashTags
    inputHashtag: string = null;
    hashtagResults: HashTag[] = [];

    //search Author
    inputAuthor: string = null;
    authorResults: any[] = [];

    //search user
    inputUser: string = null;
    userResults: any[] = [];
    lock: boolean = false;

    usersConnected: User[] = [];
    usersNotConnected: User[] = [];


    rangeSearch: any = {
        'lower': 0,
        'upper': 100
    }

    constructor(
        public alertCtrl: AlertController,
        private _postService: PostService,
        private _store: StoreService,
        public modalCtrl: ModalController,
        private _userService: UserService,
        private router: Router,
        private _searchAddressService: SearchAddressService,
        private _data: DataService,
        private route: ActivatedRoute,
        private translate: TranslateService,
        public events: Events
    ) {
        this.segmentSelected = 'photos';
        this.isMobile = this._store.get('isMobile');
        this.events.subscribe('tab-users',() => {
            this.segmentSelected = 'users';
        });
    }

    ngOnInit() {
    }

    setSegmentSelected(value: string) {
        this.segmentSelected = value;
    }

    segmentChanged(event) {
        this.setSegmentSelected(event.detail.value);
    }

    searchAddress(event: any) {
        let val = event.detail.value;
        if (val && val.trim() !== '' && val.length >= 3) {
            this._searchAddressService.searchAddress(this.filters.address).subscribe((data: any) => {
                this.addressResults = data.results;
            }, error => { console.error(error); });
        }
    }

    setLocation(location: any) {
        this.filters.address = location.formatted;
        this.filters.geometry = {
            'lat': location.geometry.lat,
            'lng': location.geometry.lng
        }
        setTimeout(() => { this.addressResults = []; }, 500);
    }

    searchHashtags(event: any) {
        let val = event.detail.value;
        if (val && val.trim() !== '' && val.length >= 3) {
            this._postService.searchHashtag(this.inputHashtag).subscribe((data: any) => {
                this.hashtagResults = [];
                data.results.forEach(e => {
                    let hashTag: HashTag = new HashTag();
                    hashTag.id = e.id;
                    hashTag.hashTag = e.name;
                    hashTag.createdAt = e.created_at;
                    this.hashtagResults.push(hashTag);
                });
            }, error => { console.error(error); });
        }
    }

    useHashTag(hashtag: HashTag) {
        this.filters.hashtags.push(hashtag);
        this.inputHashtag = '';
        this.hashtagResults = [];
    }

    removeHashTag(hashtag_id: number) {
        this.filters.hashtags = this.filters.hashtags.filter(hashtag => hashtag.id !== hashtag_id);
    }


    //search user and author
    searchUser(event: any, tab: string) {
        let val = event.detail.value;
        if (val && val.trim() !== '' && val.length >= 3) {
            this._userService.searchUser(val).subscribe((data: any) => {
                this.userResults = [];
                this.usersConnected = [];
                this.usersNotConnected = [];
                this.authorResults = [];
                if (data) {
                    data.forEach(e => {
                        let user: User = new User();
                        user.id = e.identifier;
                        user.nickname = e.username;
                        user.avatar = e.avatar_thumbnail;
                        user.firstName = e.first_name;
                        user.lastName = e.last_name;
                        user.connected = e.connection;
                        if (user.connected) {
                            this.usersConnected.push(user);
                        } else {
                            this.usersNotConnected.push(user);
                        }
                        if (tab == 'photo') {
                            this.authorResults.push(user);
                        } else {
                            this.userResults.push(user);
                        }
                    });
                }
            }, error => { console.error(error); });
        }
    }

    setUser(user) {
        this.authorResults = [];
        this.filters.user = user;
    }

    goPublicProfile(user) {
        if (this._store.get('myUser').id === user.id) {
            this.router.navigateByUrl('tabs/my-profile');
        } else {
            this.router.navigateByUrl('tabs/profile/' + user.id);
        }
    }

    formatDateToSend() {
        this.filters.startDate;
        this.filters.endDate;
        this.filters.startTime;
        this.filters.endTime;
        return null;
    }

    search() {
        // this.goToSearchPhotosResultsPage();
        let dataToSend: any = {};
        let count = 0;
        dataToSend['limit'] = '20';

        if (this.filters.range) {
            dataToSend['distance'] = this.filters.range;
        }
        if (this.filters.user) {
            dataToSend['author'] = this.filters.user.id;
            count++;
        }
        if (this.filters.startDate && this.filters.endDate) {
            dataToSend['startdate'] = this.filters.startDate.replace('Z','');
            dataToSend['enddate'] = this.filters.endDate.replace('Z','');
            count++;
        }
        else if(!this.filters.startDate && this.filters.endDate){
            dataToSend['startdate'] = this.filters.endDate.replace('Z','');
            dataToSend['enddate'] = dataToSend['startdate'];
            count++;
        }
        else if (this.filters.startDate && !this.filters.endDate) {
            dataToSend['startdate'] = this.filters.startDate.replace('Z','');
            dataToSend['enddate'] = dataToSend['startdate'];
            count++;
        }
        if (this.filters.address) {
            dataToSend['lat'] = this.location.latitude;
            dataToSend['lon'] = this.location.longitude;
            count++;
        }
        if (this.filters.hashtags.length > 0) {
            let hashtagsIds = '';
            this.filters.hashtags.forEach(el => { hashtagsIds += el.id + ','; });
            dataToSend['hashtags'] = hashtagsIds;
            dataToSend['hashtags'] = dataToSend['hashtags'].replace(/,(\s+)?$/, '');
            count++;
        }
        if(count===0){
            this.showAlertRequired();
            return;
        }

        this._data.updateSearchFilters(this.filters);
        this._store.filters = this.filters;
        this._postService.search(dataToSend).subscribe((data: any) => {
            if (data.count === 0) {
                this.showAlertNoData();
            } else {
                this._data.updateSearchResults(data);
                this.goToSearchPhotosResultsPage();
            }
        }, error => { console.error(error); });
    }

    goToSearchPhotosResultsPage() {
        this.router.navigateByUrl('tabs/search/result');
    }


    async openSearchAddressModal() {
        const modal = await this.modalCtrl.create({
            component: ModalSearchAddressComponent,
            componentProps: {}
        });
        modal.onDidDismiss().then((data: any) => {
            if (data.hasOwnProperty('data')) {
                if(!data.data || data.data===undefined){ return; }
                this.location = new Location();
                this.location.address = data.data.formated;
                this.location.latitude = data.data.geometry.lat.toString();
                this.location.longitude = data.data.geometry.lng.toString();
                this.filters.address = data.data.formated;
                this.filters.geometry = {
                    'lat': this.location.latitude,
                    'lng': this.location.longitude
                }
            }
        });
        return await modal.present();
    }



    resetFilters() {
        this.filters.startDate = null;
        this.filters.endDate = null;
        this.filters.startTime = null;
        this.filters.endTime = null;
        this.filters.address = null;
        this.filters.geometry = null;
        this.filters.range = 50;
        this.filters.hashtags = [];
        this.filters.user = null;
    }

    clearResults() {
        this.userResults = [];
        this.usersConnected = [];
        this.usersNotConnected = [];
    }

    async showAlertNoData() {
        const alert = await this.alertCtrl.create({
            header: this.translate.instant('SEARCH_NO_DATA_POPUP'),
            buttons: [
                {
                    text: this.translate.instant('GENERIC_POPUP_BUTTON_ERROR'),
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                    }
                }
            ]
        });
        await alert.present();
    }

    async showAlertRequired() {
        const alert = await this.alertCtrl.create({
            header: this.translate.instant('SEARCH_NO_FILTER_POPUP'),
            buttons: [
                {
                    text: this.translate.instant('GENERIC_POPUP_BUTTON_ERROR'),
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                    }
                }
            ]
        });
        await alert.present();
    }
}
