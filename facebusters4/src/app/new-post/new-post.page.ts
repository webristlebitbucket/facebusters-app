import { StoreService } from './../../providers/store/store';
import { ModalSearchAddressComponent } from './../shared/modal-search-address/modal-search-address.component';
import { PostService } from './../../providers/post-service/post-service';
import { UserService } from './../../providers/user-service/user-service';
import { UtilsService } from './../../services/utils';
import { ModalTagUserComponent } from './../shared/modal-tag-user/modal-tag-user.component';
import { Platform, NavParams, ActionSheetController, ModalController } from '@ionic/angular';
import { Component, OnDestroy, ElementRef, Input, OnInit } from '@angular/core';
import { SearchAddressService } from './../../providers/search/search';
import { Post, Photo, User, Album, Location, HashTag, Tag, TagCoords } from './../../models/models';
import * as EXIF from 'exif-js';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Router } from '@angular/router';
import { File, FileEntry, IFile } from '@ionic-native/file/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import * as moment from 'moment';


// export interface CameraExifDetail {
//   aperture: string;
//   datetime: string;
//   exposureTime: string;
//   flash: string;
//   focalLength: string;
//   gpsAltitude: string;
//   gpsAltitudeRef: string;
//   gpsDateStamp: string;
//   gpsLatitude: string;
//   gpsLatitudeRef: string;
//   gpsLongitude: string;
//   gpsLongitudeRef: string;
//   gpsProcessingMethod: string;
//   gpsTimestamp: string;
//   iso: string;
//   make: string;
//   model: string;
//   orientation: string;
//   whiteBalance: string;
// }
// export interface Output {
//   base64: string;
//   exifDetail: CameraExifDetail;
// }

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.page.html',
  styleUrls: ['./new-post.page.scss'],
})

export class NewPostPage implements OnInit {
  public step: number = 1;
  public post: Post = new Post();
  public base64ImageList: any[] = [];
  public metaList: any[] = [];
  public filters: any = {
    address: null,
    location: '',
    description: '',
    hashTagInput: ''
  }
  public metaData: any = null;
  public myLocation: any = { 'latitude': null, 'longitude': null };
  public origin: string = 'gallery' || 'camera';
  public hasMeta: boolean = false;
  public myUser: User = null;

  // 2 slide
  public filtersList: any[] = [];
  public photoSelected: Photo;
  public filterSelected: string = 'no-filter';

  // 3 slide
  myUserId: any;
  photoActualPosition: number = 0;
  dataToSend: any = {}; // Data To Send Backend
  postError: any = null;
  modalTagPhoto: any = null;
  postType: string = 'picture' || 'album';
  completedUpload: boolean = false;

  // loaders
  loading: any;

  // Edit Data
  title: string = 'PUBLISH_EDIT_PHOTO_MESSAGE';
  isEdit: boolean = false;
  photoEdit: Photo;
  albumEdit: Album;

  constructor(
    public _platform: Platform,
    private _navParams: NavParams,
    private _actionSheetCtrl: ActionSheetController,
    private modalCtrl: ModalController,
    private geolocation: Geolocation,
    private _searchAddressService: SearchAddressService,
    private _utilsService: UtilsService,
    private _userService: UserService,
    private _postService: PostService,
    private alertController: AlertController,
    private translate: TranslateService,
    public loadingController: LoadingController,
    private camera: Camera,
    private file: File,
    private router: Router,
    public base64: Base64,
    public store: StoreService
  ) { }

  ngOnInit() {
    console.log('-------')
    console.log('PHOTO ==> ', this.photoEdit);
    console.log('ALBUM ==> ', this.albumEdit);
    console.log('-------')

    this.myUser = this.store.get('myUser');


    if (this.photoEdit) {
      this.title = this.translate.instant('PUBLISH_EDIT_PHOTO_MESSAGE');
      this.isEdit = true;
      this.post = this.initPostToEditPhoto(this.photoEdit);
    } else if (this.albumEdit) {
      this.title = this.translate.instant('PUBLISH_EDIT_ALBUM_MESSAGE');
      this.isEdit = true;
      this.post = this.initPostToEditAlbum(this.albumEdit);
    }
    else {
      this.title = this.translate.instant('PUBLISH_TITLE');
      this.post = this.initPost(this.post);

      this._platform.ready().then(() => this.getCurrentLocation());

      this.presentActionSheetSelectOption();
    }
    this.filtersList = this._utilsService.getFilters();
    this.getMyUser();

  }

  takePicture() {
    const options: CameraOptions = {
      quality: 30,
      targetWidth: 1024,
      targetHeight: 1024,
      sourceType: this.camera.PictureSourceType.CAMERA, //PHOTOLIBRARY
      destinationType: this.camera.DestinationType.FILE_URI,
      correctOrientation: true
    }
    this.camera.getPicture(options).then((imageData) => {
      var thisResult = JSON.parse(imageData);

      // alert(JSON.stringify(thisResult));
      // alert(JSON.stringify(metadata));

      var metaGPS = {
        'Latitude': null,
        'Longitude': null,
        'LatitudeRef': null,
        'LongitudeRef': null,
        'datetime': null
      }

      if (thisResult.json_metadata != "{}" && thisResult.json_metadata != "") {
        var metadata = JSON.parse(thisResult.json_metadata);
        if (this._platform.is('ios')) {
          try {
            if (!metadata.GPS) {
              this.hasMeta = false;
              // alert(this.translate.instant('La imagen no tiene datos exif. Aplicamos los datos locales.'));
              var lat: string = parseFloat(this.post.location.latitude).toFixed(6);
              var lon: string = parseFloat(this.post.location.longitude).toFixed(6);
            } else {
              this.hasMeta = true;
              if (metadata.GPS.LatitudeRef == "S" || metadata.GPS.LatitudeRef == "W") { metadata.GPS.Latitude = metadata.GPS.Latitude * -1; }
              if (metadata.GPS.LongitudeRef == "S" || metadata.GPS.LongitudeRef == "W") { metadata.GPS.Longitude = metadata.GPS.Longitude * -1; }
              var lat: string = metadata.GPS.Latitude.toFixed(6);
              var lon: string = metadata.GPS.Longitude.toFixed(6);
            }
            const metaDataParsed: any = {
              'latitude': lat,
              'longitude': lon,
              'date': metadata.GPS.DateStamp,
              'time': metadata.GPS.TimeStamp,
              'datetime': metadata.Exif.DateTimeOriginal,
              'address': null
            }
            this.metaList.push(metaDataParsed);
            this.getAddressFromCoordinates(metaDataParsed.latitude, metaDataParsed.longitude);
            var myThis = this;
            let fileName = thisResult.filename.split('/').pop();
            let path = thisResult.filename.substring(0, thisResult.filename.lastIndexOf("/") + 1);
            this.file.readAsDataURL(path, fileName).then(base64File => {
              this.addBase64ImageToList(base64File);
              this.presentLoadingText(500);
            }).catch(() => { console.error('Error reading file'); })
          } catch (error) { console.error(error); }

        } else {  // ANDROID CAMARA
          try {
            try {
              metaGPS = {
                'Latitude': metadata.gpsLatitude,
                'Longitude': metadata.gpsLongitude,
                'LatitudeRef': metadata.gpsLatitudeRef,
                'LongitudeRef': metadata.gpsLongitudeRef,
                'datetime': metadata.datetime
              }
            } catch (error) { alert('error'); metaGPS = null; }
            if (!metaGPS) { // console.log('NO TIENE META GPS');
              this.hasMeta = false;
              var lat: string = parseFloat(this.post.location.latitude).toFixed(6);
              var lon: string = parseFloat(this.post.location.longitude).toFixed(6);
              var datetimeFormated = null;
            } else { // console.log('SI TIENE META GPS');
              this.hasMeta = true;
              // 39/1,28/1,1308/10000
              metaGPS.Latitude = this.convertDMSToDD(
                (Number(metadata.gpsLatitude.split(',')[0].split('/')[0]) / Number(metadata.gpsLatitude.split(',')[0].split('/')[1])),
                (Number(metadata.gpsLatitude.split(',')[1].split('/')[0]) / Number(metadata.gpsLatitude.split(',')[1].split('/')[1])),
                (Number(metadata.gpsLatitude.split(',')[2].split('/')[0]) / Number(metadata.gpsLatitude.split(',')[2].split('/')[1])),
                metaGPS.LatitudeRef.toString()
              );
              metaGPS.Longitude = this.convertDMSToDD(
                (Number(metadata.gpsLongitude.split(',')[0].split('/')[0]) / Number(metadata.gpsLongitude.split(',')[0].split('/')[1])),
                (Number(metadata.gpsLongitude.split(',')[1].split('/')[0]) / Number(metadata.gpsLongitude.split(',')[1].split('/')[1])),
                (Number(metadata.gpsLongitude.split(',')[2].split('/')[0]) / Number(metadata.gpsLongitude.split(',')[2].split('/')[1])),
                metaGPS.LongitudeRef.toString()
              );
              var lat: string = metaGPS.Latitude.toString();
              var lon: string = metaGPS.Longitude.toString();
              var datetimeFormated = metaGPS.datetime;
              // YYYY:MM:DD HH:MM:SS ==>  DD/MM/YY HH:MM
              var mDate = datetimeFormated.split(' ')[0];
              var mTime = datetimeFormated.split(' ')[1];
              datetimeFormated = mDate.split(':')[0] + '-' + mDate.split(':')[1] + '-' + mDate.split(':')[2] + 'T' + mTime.split(':')[0] + ':' + mTime.split(':')[1] + 'Z'; // 2018-07-10T21:50:28+02:00
            }

            const metaDataParsed: any = {
              'latitude': lat.toString(),
              'longitude': lon.toString(),
              'address': null,
              'datetime': datetimeFormated
            }
            this.metaList.push(metaDataParsed);
            this.getAddressFromCoordinates(metaDataParsed.latitude, metaDataParsed.longitude);

            let fileName = thisResult.filename.split('/').pop();
            let path = thisResult.filename.substring(0, thisResult.filename.lastIndexOf("/") + 1);
            this.file.readAsDataURL(path, fileName).then(base64File => {
              this.addBase64ImageToList(base64File);
              this.presentLoadingText(500);
            }).catch(() => { console.error('Error reading file'); })
          } catch (error) { }
          //alert('Lat: '+metadata.gpsLatitude+' Lon: '+metadata.gpsLongitude+' Date: '+metadata.gpsDateStamp);
        }
      } else { /* no hay metadata */
        this.hasMeta = false;
      }  // no tenemos metadatas en el json_metadata
    }, (err) => { /* alert('Cancelado por el usuario'); */ });
    function onFail(failed) { console.error(failed); }
  }

  convertDMSToDD(degrees, minutes, seconds, direction) {
    var dd = degrees + (minutes/60) + (seconds/3600);
    if (direction == "S" || direction == "W") { dd = dd * -1; }
    return dd;
}

  takeGallery() {
    const options: CameraOptions = {
      quality: 30,
      targetWidth: 1024,
      targetHeight: 1024,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      correctOrientation: true
    }
    this.camera.getPicture(options).then((imageData) => {
      var thisResult = JSON.parse(imageData);
      // alert(JSON.stringify(metadata));
      var metaGPS = {
        'Latitude': null,
        'Longitude': null,
        'LatitudeRef': null,
        'LongitudeRef': null,
        'datetime': null
      }

      if (thisResult.json_metadata != "{}" && thisResult.json_metadata != "") {
        var metadata = JSON.parse(thisResult.json_metadata);
        if (this._platform.is('ios')) {
          if (!metadata.GPS) { // console.log('NO TIENE META GPS');
            this.hasMeta = false;
            var lat: string = parseFloat(this.post.location.latitude).toFixed(6);
            var lon: string = parseFloat(this.post.location.longitude).toFixed(6);
            var time = null;
            var date = null;
          } else { // console.log('SI TIENE META GPS');
            this.hasMeta = true;
            if (metadata.GPS.LatitudeRef == "S" || metadata.GPS.LatitudeRef == "W") { metadata.GPS.Latitude = metadata.GPS.Latitude * -1; }
            if (metadata.GPS.LongitudeRef == "S" || metadata.GPS.LongitudeRef == "W") { metadata.GPS.Longitude = metadata.GPS.Longitude * -1; }
            var lat: string = metadata.GPS.Latitude.toFixed(6);
            var lon: string = metadata.GPS.Longitude.toFixed(6);
            var date = metadata.GPS.DateStamp;
            var time = metadata.GPS.TimeStamp;
          }
          let dateTimeFormated = null;
          try {
            dateTimeFormated = metadata.Exif.DateTimeOriginal.split(' ')[0].replace(/:/g, '-') + 'T' + metadata.Exif.DateTimeOriginal.split(' ')[1];
          } catch (error) { dateTimeFormated = null; }
          const metaDataParsed: any = {
            'latitude': lat,
            'longitude': lon,
            'date': date,
            'time': time,
            'address': null,
            'datetime': dateTimeFormated
          }
          this.metaList.push(metaDataParsed);
          this.getAddressFromCoordinates(metaDataParsed.latitude, metaDataParsed.longitude);
          let fileName = thisResult.filename.split('/').pop();
          let path = thisResult.filename.substring(0, thisResult.filename.lastIndexOf("/") + 1);
          this.file.readAsDataURL(path, fileName).then(base64File => {
            this.addBase64ImageToList(base64File);
            this.presentLoadingText(500);
          }).catch(() => { console.log('Error reading file'); })
        } else {   // ANDROID GALLERY
          try {
            try {
              metaGPS = {
                'Latitude': metadata.gpsLatitude,
                'Longitude': metadata.gpsLongitude,
                'LatitudeRef': metadata.gpsLatitudeRef,
                'LongitudeRef': metadata.gpsLongitudeRef,
                'datetime': metadata.datetime
              }
            } catch (error) { alert('error'); metaGPS = null; }
            if (!metaGPS) { // console.log('NO TIENE META GPS');
              this.hasMeta = false;
              var lat: string = parseFloat(this.post.location.latitude).toFixed(6);
              var lon: string = parseFloat(this.post.location.longitude).toFixed(6);
              var datetimeFormated = null;
            } else { // console.log('SI TIENE META GPS');
              this.hasMeta = true;
              // 39/1,28/1,1308/10000
              metaGPS.Latitude = this.convertDMSToDD(
                (Number(metadata.gpsLatitude.split(',')[0].split('/')[0]) / Number(metadata.gpsLatitude.split(',')[0].split('/')[1])),
                (Number(metadata.gpsLatitude.split(',')[1].split('/')[0]) / Number(metadata.gpsLatitude.split(',')[1].split('/')[1])),
                (Number(metadata.gpsLatitude.split(',')[2].split('/')[0]) / Number(metadata.gpsLatitude.split(',')[2].split('/')[1])),
                metaGPS.LatitudeRef.toString()
              );
              metaGPS.Longitude = this.convertDMSToDD(
                (Number(metadata.gpsLongitude.split(',')[0].split('/')[0]) / Number(metadata.gpsLongitude.split(',')[0].split('/')[1])),
                (Number(metadata.gpsLongitude.split(',')[1].split('/')[0]) / Number(metadata.gpsLongitude.split(',')[1].split('/')[1])),
                (Number(metadata.gpsLongitude.split(',')[2].split('/')[0]) / Number(metadata.gpsLongitude.split(',')[2].split('/')[1])),
                metaGPS.LongitudeRef.toString()
              );
              var lat: string = metaGPS.Latitude.toString();
              var lon: string = metaGPS.Longitude.toString();
              var datetimeFormated = metaGPS.datetime;
              // YYYY:MM:DD HH:MM:SS ==>  DD/MM/YY HH:MM
              var mDate = datetimeFormated.split(' ')[0];
              var mTime = datetimeFormated.split(' ')[1];
              datetimeFormated = mDate.split(':')[0] + '-' + mDate.split(':')[1] + '-' + mDate.split(':')[2] + 'T' + mTime.split(':')[0] + ':' + mTime.split(':')[1] + 'Z'; // 2018-07-10T21:50:28+02:00
            }

            const metaDataParsed: any = {
              'latitude': lat.toString(),
              'longitude': lon.toString(),
              'address': null,
              'datetime': datetimeFormated
            }

            this.metaList.push(metaDataParsed);
            this.getAddressFromCoordinates(metaDataParsed.latitude, metaDataParsed.longitude);

            let fileName = thisResult.filename.split('?')[0];
            fileName = fileName.split('/').pop();
            let path = thisResult.filename.substring(0, thisResult.filename.lastIndexOf("/") + 1);
            this.file.readAsDataURL(path, fileName).then(base64File => {
              this.addBase64ImageToList(base64File);
              this.presentLoadingText(500);
            }).catch((err) => { alert(JSON.stringify(err)); console.error('Error reading file'); });
          } catch (error) {
            // alert(error.toString())
          }

          //alert('Lat: '+metadata.gpsLatitude+' Lon: '+metadata.gpsLongitude+' Date: '+metadata.gpsDateStamp);
        }
      } else { // NO HAY metaData
        this.hasMeta = false;
        var today = new Date();
        if (this._platform.is('ios')) {
          // alert(this.translate.instant('La imagen no tiene datos exif. Aplicamos los datos locales.'));
          var lat: string = parseFloat(this.post.location.latitude).toFixed(6);
          var lon: string = parseFloat(this.post.location.longitude).toFixed(6);
          var time = this.post._time;
          var date = this.post._date;
          const metaDataParsed: any = {
            'latitude': lat,
            'longitude': lon,
            'date': date,
            'time': time,
            'datetime': today.toISOString(),
            'address': null
          }
          this.metaList.push(metaDataParsed);
          this.getAddressFromCoordinates(metaDataParsed.latitude, metaDataParsed.longitude);
          var myThis = this;
          let fileName = thisResult.filename.split('/').pop();
          let path = thisResult.filename.substring(0, thisResult.filename.lastIndexOf("/") + 1);
          this.file.readAsDataURL(path, fileName).then(base64File => {
            this.addBase64ImageToList(base64File);
            this.presentLoadingText(500);
          }).catch(() => { console.log('Error reading file'); });
        } else { // ANDROID
          var lat: string = parseFloat(this.post.location.latitude).toFixed(6);
          var lon: string = parseFloat(this.post.location.longitude).toFixed(6);
          var time = this.post._time;
          var date = this.post._date;
          const metaDataParsed: any = {
            'latitude': lat,
            'longitude': lon,
            'date': date,
            'time': time,
            'datetime': today.toISOString(),
            'address': null
          }
          this.metaList.push(metaDataParsed);
          try {
            let fileName = thisResult.filename.split('?')[0];
            fileName = fileName.split('/').pop();
            let path = thisResult.filename.substring(0, thisResult.filename.lastIndexOf("/") + 1);
            this.file.readAsDataURL(path, fileName).then(base64File => {
              this.addBase64ImageToList(base64File);
              this.presentLoadingText(500);
            }).catch((err) => { console.log(err); console.error('Error reading file'); });
          } catch (error) { }
        }
      }
    }, (err) => { console.log(err); });
    function onFail(message) { }
  }


  initPost(post: Post = new Post()) {
    post.id = 1;
    post.location = new Location();
    post.photos = [];
    post.postType = 'photo';
    post.title = '';
    post.isPublic = false;
    return post;
  }


  initPostToEditPhoto(photo: Photo) {
    let post = new Post();
    post.id = photo.id;
    post.postType = 'photo';
    post.title = photo.title;
    post.location = photo.location;
    post.isPublic = !photo.isPublic;
    post.photos = [];
    photo._datetime = photo.metaDate;
    let photoClone = Object.assign({}, photo);
    post.photos.push(photoClone);
    return post;
  }

  initPostToEditAlbum(album: Album) {
    let post = new Post();
    post.id = album.id;
    post.postType = 'album';
    post.title = album.title;
    post.location = album.location;
    post.isPublic = !album.isPublic;
    post.photos = [];
    album.photos.forEach(photo => {
      photo._datetime = photo.metaDate;
      let photoClone = Object.assign({}, photo);
      post.photos.push(photoClone);
    });
    return post;
  }

  addBase64ImageToList(urlBase64Image: any) {
    this.base64ImageList.push(urlBase64Image);
    this.addNewPhotoToPost(urlBase64Image);
  }
  addNewPhotoToPost(urlBase64Image: any) {
    let photo = new Photo();
    photo.id = this.post.photos.length;
    photo.image = urlBase64Image;
    photo.thumbnail = urlBase64Image;
    photo.filter = 'no-filter';
    photo._origin = this.origin;
    photo._hasMeta = this.hasMeta;
    photo.user = this.myUser;
    photo.isPublic = !this.post.isPublic;
    this.convertToBase64(photo.image, 'image/png').then(data => {
      // console.log('ADD photo b64 to post');
      photo._imageB64 = data.toString();
      this.post.photos.push(photo);
      try {
        this.post.photos[(this.post.photos.length - 1)].location.address = this.metaList[(this.metaList.length - 1)].address;
      } catch (error) {}
      try {
        this.post.photos[(this.post.photos.length - 1)]._date = this.metaList[(this.metaList.length - 1)].date;
      } catch (error) {}
      try {
        this.post.photos[(this.post.photos.length - 1)]._time = this.metaList[(this.metaList.length - 1)].time;
      } catch (error) {}
      try {
        this.post.photos[(this.post.photos.length - 1)]._datetime = this.metaList[(this.metaList.length - 1)].datetime;
      } catch (error) {}
    });
    this.photoSelected = this.post.photos[0];
  }
  convertToBase64(url, outputFormat) {
    return new Promise((resolve, reject) => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = function () {
        let canvas = <HTMLCanvasElement>document.createElement('CANVAS'), ctx = canvas.getContext('2d'), dataURL;
        canvas.height = img.height;
        canvas.width = img.width;
        ctx.drawImage(img, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        canvas = null;
        resolve(dataURL);
      };
      img.src = url;
    });
  }
  goBack() {
    this.presentLoadingText(500);
    if (this.step > 1) {
      this.step -= 1;
    }
  }
  goToNext() {
    if (this.step === 1 && this.post.photos.length > 1 && (!this.post.title || this.post.title === '')) {
      this.presentAlertError(true, false, false);
      return;
    }
    this.presentLoadingText(500);
    let myThis = this;
    setTimeout(function () {
      if (myThis.post.photos.length > 0) {
        myThis.setPhotoSelected(myThis.post.photos[0]);
        if (myThis.step < 3) { myThis.step += 1; }
        if (myThis.step === 3) { myThis.initStep3(); }
      }
    }, 500);
  }
  randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  getBase64(file) {
    let myThis = this;
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () { myThis.addNewPhotoToPost(reader.result); };
    reader.onerror = function (error) { console.log('Error: ', error); };
  }

  async presentActionSheetSelectOption() {
    const actionSheet = await this._actionSheetCtrl.create({
      buttons: [
        {
          text: this.translate.instant('PUBLISH_INPUT_CAMERA'),
          icon: 'camera',
          handler: () => {
            this.origin = 'camera';
            this.takePicture();
          }
        }, {
          text: this.translate.instant('PUBLISH_INPUT_GALLERY'),
          icon: 'images',
          handler: () => {
            this.origin = 'gallery';
            this.takeGallery();
          }
        }, {
          text: this.translate.instant('PUBLISH_INPUT_CANCEL'),
          icon: 'close',
          role: 'cancel',
          handler: () => {
            this.origin = null;
            if (this.post.photos.length === 0) {
              this.closeModal();
            }
          }
        }]
    });
    await actionSheet.present();
  }

  async presentActionSheetDelete(photo: Photo) {
    if (this.isEdit) {
      return;
    }
    const actionSheet = await this._actionSheetCtrl.create({
      buttons: [
        {
          text: this.translate.instant('PUBLISH_BUTTON_DELETE'),
          icon: 'trash',
          handler: () => {
            this.deletePicture(photo);
          }
        }, {
          text: this.translate.instant('PUBLISH_BUTTON_DELETE_CANCEL'),
          icon: 'close',
          role: 'cancel',
          handler: () => {
            // console.log('Cancel Delete clicked');
          }
        }]
    });
    await actionSheet.present();
  }

  deletePicture(index) {
    this.post.photos.splice(index, 1);
  }

  async openCamera() {
    const options: CameraOptions = {
      quality: 30,
      targetWidth: 1024,
      targetHeight: 1024,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpg;base64,' + imageData;
      this.addBase64ImageToList(base64Image);
      this.presentLoadingText(500);
    }, (err) => {
    });
  }
  openGallery() {
    const options: CameraOptions = {
      quality: 30,
      targetWidth: 1024,
      targetHeight: 1024,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpg;base64,' + imageData;
      this.addBase64ImageToList(base64Image);
      this.presentLoadingText(500);
    }, (err) => { console.log(err); });
  }

  getAddressFromCoordinates(latitude: Number, longitude: Number) {
    let myThis = this;
    let address: string = null;
    this._searchAddressService.searchAddressByCoords({ 'lat': latitude, 'lon': longitude }).subscribe((data: any) => {
      // console.log('Address found ==> ', data);
      // console.log(myThis.metaList)
      myThis.metaList[myThis.metaList.length - 1].address = data.address;
      this.post.photos[this.post.photos.length - 1].location.address = this.metaList[this.metaList.length - 1].address;
      this.post.photos[this.post.photos.length - 1].location.latitude = this.metaList[this.metaList.length - 1].latitude;
      this.post.photos[this.post.photos.length - 1].location.longitude = this.metaList[this.metaList.length - 1].longitude;
      this.post.photos[this.post.photos.length - 1]._datetime = this.metaList[this.metaList.length - 1].datetime;
      this.post.photos.forEach(photo => {
        if (photo.location.address) {
          this.filters.address = photo.location.address;
          // console.log('Address to show ==> ', this.filters.address);
          // console.log('Datetime to show ==> ', photo._datetime);
          return;
        }
      });
    }, error => { console.error(error); });
  }

  async openSearchAddressModal() {
    const modal = await this.modalCtrl.create({
      component: ModalSearchAddressComponent,
      componentProps: {}
    });
    modal.onDidDismiss().then((data: any) => {
      // console.log('From modal search address ==> ', data);
      if (data.hasOwnProperty('data')) {
        let addressSelected = data;
        this.filters.address = data.data.formatted;
        this.post.location = new Location();
        this.post.location.address = data.data.formatted;
        this.post.location.latitude = data.data.geometry.lat.toString();
        this.post.location.longitude = data.data.geometry.lng.toString();
      }
    });
    return await modal.present();
  }

  async openSearchAddressModalPhoto(photoActualPosition) {
    const modal = await this.modalCtrl.create({
      component: ModalSearchAddressComponent,
      componentProps: {}
    });
    modal.onDidDismiss().then((data: any) => {
      if (data.data.formated) {
        let addressSelected = data;
        this.post.photos[photoActualPosition].location = new Location();
        this.post.photos[photoActualPosition].location.address = data.data.formated;
        this.post.photos[photoActualPosition].location.latitude = data.data.geometry.lat.toString();
        this.post.photos[photoActualPosition].location.longitude = data.data.geometry.lng.toString();
      }
    });
    return await modal.present();
  }



  getCurrentLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      try {
        this.myLocation.latitude = resp.coords.latitude;
        this.myLocation.longitude = resp.coords.longitude;
        this.post.location.latitude = this.myLocation.latitude;
        this.post.location.longitude = this.myLocation.longitude;
        this._searchAddressService.searchAddressByCoords({ 'lat': this.myLocation.latitude, 'lon': this.myLocation.longitude }).subscribe((data: any) => {
          this.filters.address = data.address;
          this.post.location.address = data.address;
          // alert(this.post.location.address);
        }, error => { console.error(error); });
      } catch (error) { console.error(error); }
    }).catch((error) => {
      // alert(JSON.stringify(error));
      console.error('Error getting location', error);
    });
  }

  closeModal() {
    if (!this.completedUpload) {
      this.modalCtrl.dismiss(null);
      return null;
    }
    if (this.post.postType === 'photo') {
      this.post.photos[0].metaDate = this.post.photos[0]._datetime;
      this.modalCtrl.dismiss(this.post.photos[0]);
    }
    else if (this.post.postType === 'album') {
      this.post.photos.forEach(photo => {
        photo.metaDate = photo._datetime;
      });
      this.modalCtrl.dismiss(this.post);
    }
  }

  // Slide 2
  slideOpts = {
    zoom: false,
    effect: 'flip',
    slidesPerView: 4
  };
  slideOptsFilters = {
    zoom: false,
    effect: 'flip',
    slidesPerView: 3
  };

  setPhotoSelected(photo: Photo = new Photo()) {
    this.photoSelected = photo;
    this.filterSelected = photo.filter;
  }

  setFilterSelected(photo: Photo, filter: string) {
    photo.filter = filter;
    this.filterSelected = filter;
  }

  goToNewPostInfo() {
    // this._navCtrl.push('NewPostInfoPage', { post: this.post });
  }

  goToBack() {
    if (this.step > 1) {
      this.step--;
    }
  }


  // step 3
  initStep3() {
    this.photoActualPosition = 0;
    this.setPhotoSelected(this.post.photos[this.photoActualPosition]);
    this.post.photos.forEach(photo => {
      switch (photo._origin) {
        case 'camera':
          // photo.location = new Location();     // Current location
          photo._datetime = moment().format();
          break;
        case 'gallery':
          if (photo._hasMeta) {
          } else {
            photo.location = new Location(); // Location null
          }
          break;
      }
    });
  }

  initPostType() {
    if (this.post) {
      if (this.post.photos.length > 1) { this.postType = 'album'; }
      else { this.postType = 'picture'; }
    }
  }

  initFilters() {
    this.filters = {
      location: '',
      description: '',
      isPrivate: true,
      hashTagInput: ''
    }
  }

  getMyUser() {
    this._userService.getMyUser().subscribe((data: any) => {
      this.myUserId = data.id;
    }, error => { console.error(error); });
  }

  nextPhoto() {
    // Validations
    if (!this.post.photos[this.photoActualPosition]._datetime && !this.post.photos[this.photoActualPosition].location.address) {
      this.presentAlertError(false, true, true);
    } else if (!this.post.photos[this.photoActualPosition]._datetime) {
      this.presentAlertError(false, true, false);
    } else if (!this.post.photos[this.photoActualPosition].location.address) {
      this.presentAlertError(false, false, true);
    } else {
      if (this.photoActualPosition === this.post.photos.length - 1) {
        this.presentConfirm();
      } else {
        this.photoActualPosition += 1;
      }
    }
  }

  previousPhoto() {
    if (this.photoActualPosition === 0) {
      this.photoActualPosition = this.post.photos.length - 1;
    } else {
      this.photoActualPosition -= 1;
    }
  }
  async presentConfirm() {
    // Hay que verificar la fecha obligatoria
    const alert = await this.alertController.create({
      header: this.translate.instant('PUBLISH_CONFIRM_UPLOAD_MESSAGE'),
      buttons: [
        {
          text: this.translate.instant('PUBLISH_CONFIRM_UPLOAD_BUTTON_CANCEL'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // console.log('Confirm Cancel Upload');
          }
        }, {
          text: this.translate.instant('PUBLISH_CONFIRM_UPLOAD_BUTTON_CONFIRM'),
          handler: () => {
            this.uploadPost();
          }
        }
      ]
    });
    await alert.present();
  }

  async presentCompleteUpload() {
    const alert = await this.alertController.create({
      header: this.translate.instant('PUBLISH_CONFIRM_UPLOAD_MESSAGE_UPLOADED'),
      buttons: [
        {
          text: this.translate.instant('PUBLISH_CONFIRM_UPLOAD_BUTTON_UPLOADED'),
          handler: (blah) => {
            this.dismissLoadingText();
            this.closeModal();
          }
        }
      ]
    });
    await alert.present();
  }

  async copyPreviousAlert() {
    const alert = await this.alertController.create({
      message: this.translate.instant('PUBLISH_COPY_ALERT'),
      buttons: [
        {
          text: this.translate.instant('PUBLISH_COPY_ALERT_BUTTON_CONFIRM'),
          handler: (blah) => {
            this.copyPrevious();
          }
        },
        {
          text: this.translate.instant('PUBLISH_COPY_ALERT_BUTTON_CANCEL'),
          role: 'cancel',
          handler: () => {
            // console.log('Copy previous cancel clicked');
          }
        }
      ]
    });
    await alert.present();
  }

  async photoUploaded() {
    const alert = await this.alertController.create({
      header: this.translate.instant('GENERIC_POPUP_HEADER_OK'),
      message: this.translate.instant('PUBLISH_CONFIRM_UPLOAD_MESSAGE_UPLOADED'),
      buttons: [
        {
          text: this.translate.instant('GENERIC_POPUP_BUTTON_OK'),
          handler: (blah) => {
            document.getElementById('description').click();
          }
        }
      ]
    });
    await alert.present();
  }

  copyPrevious() {
    if (this.photoActualPosition === 0) {
      this.post.photos[this.photoActualPosition].title = this.post.title;
      this.post.photos[this.photoActualPosition]._date = this.post._date;
      this.post.photos[this.photoActualPosition]._time = this.post._time;
      this.post.photos[this.photoActualPosition].location = this.post.location;
      return;
    }
    if (this.post.photos.length > 1) {
      this.post.photos[this.photoActualPosition].title = this.post.photos[this.photoActualPosition - 1].title;
      this.post.photos[this.photoActualPosition]._date = this.post.photos[this.photoActualPosition - 1]._date;
      this.post.photos[this.photoActualPosition]._time = this.post.photos[this.photoActualPosition - 1]._time;
      this.post.photos[this.photoActualPosition].location = this.post.photos[this.photoActualPosition - 1].location;

      // this.post.photos[this.photoActualPosition].tags = Object.assign({}, this.post.photos[this.photoActualPosition - 1].tags);
      // this.post.photos[this.photoActualPosition].hashTags = Object.assign({}, this.post.photos[this.photoActualPosition - 1].hashTags);
    }
  }

  uploadPost() {
    var myThis = this;
    this.presentLoadingText().then(function () {
      if (myThis.post.photos.length === 1) {  // Single Picture
        if (!myThis.isEdit) { // new picture
          if (!myThis.post.photos[0].title) { myThis.post.photos[0].title = ''; }
          myThis.dataToSend = {
            "title": myThis.post.photos[0].title,
            "image": myThis.post.photos[0]._imageB64,
            "picture_filter": myThis.post.photos[0].filter,
            "public": !myThis.post.isPublic,
            "meta_location": {
              "latitude": myThis.post.photos[0].location.latitude,
              "longitude": myThis.post.photos[0].location.longitude
            },
            "hashtags": [],
            "meta_date": myThis.post.photos[0]._datetime.replace('Z', '').replace('-00:00', ''),
            "user": myThis.myUserId
          };
          console.log('meta_date ==> ', myThis.dataToSend.meta_date);
          if (!myThis.dataToSend.meta_location.latitude || !myThis.dataToSend.meta_location.longitude) {
            myThis.dataToSend.meta_location.latitude = myThis.myLocation.latitude;  // latitud por defecto si no tiene
            myThis.dataToSend.meta_location.longitude = myThis.myLocation.longitude; // longitud por defecto si no tiene
          }
          myThis.post.photos[0].hashTags.forEach(hashtag => {
            myThis.dataToSend.hashtags.push(hashtag.id)
          });
          myThis._postService.createNewPicture(myThis.dataToSend).subscribe(data => {
            console.log('Single Photo Uploaded ID ==> ', data);
            myThis.saveTags(myThis.post.photos[0], data['identifier']);
          }, error => {
            console.error(error);
            myThis.postError = error;
          });
        } else { // edit picture
          console.log('--- Edit picture  ---');
          if (!myThis.post.photos[0].title) { myThis.post.photos[0].title = ''; }
          myThis.dataToSend = {
            "title": myThis.post.photos[0].title,
            "picture_filter": myThis.post.photos[0].filter,
            "public": !myThis.post.isPublic,
            "meta_location": {
              "latitude": myThis.post.photos[0].location.latitude,
              "longitude": myThis.post.photos[0].location.longitude
            },
            "meta_date": myThis.post.photos[0]._datetime.replace('Z', '').replace('-00:00', ''),
            "user": myThis.myUserId
          };
          if (!myThis.dataToSend.meta_location.latitude || !myThis.dataToSend.meta_location.longitude) {
            myThis.dataToSend.meta_location.latitude = myThis.myLocation.latitude;  // latitud por defecto si no tiene
            myThis.dataToSend.meta_location.longitude = myThis.myLocation.longitude; // longitud por defecto si no tiene
          }
          myThis._postService.editPicture(myThis.photoEdit.id, myThis.dataToSend).subscribe(data => {
            console.log('Single Photo Edited ==> ', data);
            myThis.completedUpload = true;
          }, error => {
            console.error(error);
            myThis.postError = error;
          });
        }
      }
      if (myThis.post.photos.length > 1) {  // Album
        if (!myThis.isEdit) {  // create an album
          if (!myThis.post.title) { myThis.post.title = ''; }
          myThis.dataToSend = {
            "title": myThis.post.title,
            "private": myThis.post.isPublic,
            "public": !myThis.post.isPublic,
            "meta_location": {
              "latitude": myThis.post.location.latitude,
              "longitude": myThis.post.location.longitude
            }
          };
          myThis._postService.createNewAlbum(myThis.dataToSend).subscribe((data: any) => { // Album Post
            var idAlbum = data.id;
            var count = 0;
            let order = 0;
            myThis.post.photos.forEach(photo => { // Picture Post
              order++;
              if (!photo.location.latitude || !photo.location.longitude) {
                photo.location.latitude = myThis.post.location.latitude;
                photo.location.longitude = myThis.post.location.longitude;
              }
              if (!photo.title) { photo.title = ''; }
              myThis.dataToSend = {
                "album": idAlbum,
                "title": photo.title,
                "image": photo._imageB64,
                "picture_filter": photo.filter,
                "public": !myThis.post.isPublic,
                "meta_location": {
                  "latitude": photo.location.latitude,
                  "longitude": photo.location.longitude
                },
                "hashtags": [],
                "meta_date": photo._datetime.replace('Z', '').replace('-00:00', ''), // 2018-07-10T21:50:28+02:00
                "user": myThis.myUserId,
                "order": order
              };
              photo.hashTags.forEach(hashtag => { // BUG
                myThis.dataToSend.hashtags.push(hashtag.id)
              });
              myThis._postService.createNewPicture(myThis.dataToSend).subscribe((data: any) => {
                count++;
                console.log('Album Photo Uploaded ID ==> ', data);
                myThis.saveTags(photo, data['identifier']);
                console.log(count, myThis.post.photos.length);

                if (count >= myThis.post.photos.length) {
                  console.log('Album Upload complete');
                  myThis.completedUpload = true;
                  myThis.dismissLoadingText();
                  myThis.presentCompleteUpload();
                }
              }, error => { myThis.dismissLoadingText(); myThis.presentCompleteUpload(); console.error(error); myThis.postError = error; });
            });
          }, error => { myThis.dismissLoadingText(); myThis.presentCompleteUpload(); console.error(error); myThis.postError = error; });
        } else if (myThis.isEdit) { // Edit Album
          console.log('--- Edit Album ---');
          if (!myThis.post.title) { myThis.post.title = ''; }
          myThis.dataToSend = {
            "title": myThis.post.title,
            "private": myThis.post.isPublic,
            "public": !myThis.post.isPublic
          };
          myThis._postService.editAlbum(myThis.post.id, myThis.dataToSend).subscribe((data: any) => {
            console.log('Album Updated Completed', data);
          }, error => { myThis.dismissLoadingText(); myThis.presentCompleteUpload(); console.error(error); myThis.postError = error; });
          console.log('--- Edit photos on album  ---');
          var count = 0;
          myThis.post.photos.forEach(photo => { // Picture Post
            if (!photo.location.latitude || !photo.location.longitude) {
              photo.location.latitude = myThis.post.location.latitude;
              photo.location.longitude = myThis.post.location.longitude;
            }
            if (!photo.title) { photo.title = ''; }
            myThis.dataToSend = {
              "title": photo.title,
              "picture_filter": photo.filter,
              "public": !myThis.post.isPublic,
              "meta_location": {
                "latitude": photo.location.latitude,
                "longitude": photo.location.longitude
              },
              "meta_date": photo._datetime.replace('Z', '').replace('-00:00', ''), // 2018-07-10T21:50:28+02:00
            };
            myThis._postService.editPicture(photo.id, myThis.dataToSend).subscribe((data: any) => {
              count++;
              console.log('Album Photo Updated ==> ', data);
              if (count == myThis.post.photos.length) {
                console.log('Album Updated Completed');
                myThis.completedUpload = true;
                myThis.dismissLoadingText();
                myThis.presentCompleteUpload();
              }
            }, error => { myThis.dismissLoadingText(); myThis.presentCompleteUpload(); console.error(error); myThis.postError = error; });
          });
        }
      } else {
        myThis.dismissLoadingText();
        myThis.presentCompleteUpload();
      }
    });
  }

  async presentLoadingText(duration: number = null) {
    if (duration) {
      this.loading = await this.loadingController.create({
        message: '',
        duration: duration
      });
      setTimeout(function () {
        try { document.getElementById('content').click(); }
        catch (error) { console.error(error); }
      }, duration);
    } else {
      this.loading = await this.loadingController.create({
        message: ''
      });
    }
    return await this.loading.present();
  }

  dismissLoadingText() {
    this.loading.dismiss();
  }

  addHashTag(photo: Photo) {
    const hashTag: HashTag = new HashTag();
    this._postService.searchHashtag(this.filters.hashTagInput).subscribe((data: any) => {
      if (data.count === 0) { // Not exists
        this._postService.createHashtag(this.filters.hashTagInput).subscribe((data: any) => {
          hashTag.id = data.id;
          hashTag.hashTag = data.name;
          hashTag.createdAt = data.created_at;
          photo.hashTags.push(hashTag);
          this.filters.hashTagInput = '';
        }, error => { console.error(error); });
      } else { // Exists
        hashTag.id = data.results[0].id;
        hashTag.hashTag = data.results[0].name;
        hashTag.createdAt = data.results[0].created_at;
        photo.hashTags.push(hashTag);
        this.filters.hashTagInput = '';
      }
    }, error => { console.error(error); });
  }

  removeHashTag(hashtag: HashTag) {
    this.post.photos[this.photoActualPosition].hashTags.forEach((el, index) => {
      if (el === hashtag) {
        this.post.photos[this.photoActualPosition].hashTags.splice(index, 1);
        return;
      }
    });
  }


  async showModalTagPhoto(photo: Photo) {
    const modal = await this.modalCtrl.create({
      component: ModalTagUserComponent,
      componentProps: { 'photo': photo, 'album_index': 0, 'isNewPhoto': true }
    });

    modal.onDidDismiss().then((data: any) => {
      photo.tags = data.data.tags;
      // console.log('Data from modal tag ==> ', data);
      // photo.tags = [];
      // data.data.tags.forEach(el => {
      //   const tag: Tag = new Tag();
      //   tag.user = el.user;
      //   tag.coords = new TagCoords();
      //   tag.coords.row = el.coords.row;
      //   tag.coords.col = el.coords.col;
      //   photo.tags.push(tag);
      // });
    })
    return await modal.present();
  }

  saveTags(photo, idPhoto) {
    console.log('save tags ==> ', idPhoto);
    console.log('tags of photo ==> ', photo.tags.length)

    photo.tags.forEach(tag => {
      let dataToSend: any = {};
      if (tag.nonUser) {
        dataToSend = {
          'picture': idPhoto,
          'status': 'add',
          'top': tag.coords.row,
          'left': tag.coords.col,
          'non_user': tag.nonUser,
          'hide': false
        }
      } else {
        dataToSend = {
          'picture': idPhoto,
          'user': tag.user.id,
          'status': 'add',
          'top': tag.coords.row,
          'left': tag.coords.col
        }
      }
      this._postService.addUserTag(dataToSend).subscribe((data: any) => {
        console.log('Usertag added ==> ', data);
      }, error => {
        console.log('ERROR ADDING TAG');
        console.error(error);
        console.log(error);

      });
    });
  }

  async presentAlertError(titleM: boolean = false, dateM: boolean = false, locationM: boolean = false) {
    let textError: string = '';
    if (titleM) {
      textError = this.translate.instant('PUBLISH_ALBUM_TITLE');
    }
    if (locationM && dateM) {
      textError = this.translate.instant('PUBLISH_PHOTO_SHEET_DATE_TIME');
    } else if (dateM) {
      textError = this.translate.instant('PUBLISH_PHOTO_SHEET_DATE_TIME');
    } else if (locationM) {
      textError = this.translate.instant('PUBLISH_PHOTO_SHEET_PLACE');
    }
    const alert = await this.alertController.create({
      header: this.translate.instant('PUBLISH_REQUIRED_ALERT'),
      message: textError,
      buttons: [
        {
          text: this.translate.instant('GENERIC_POPUP_BUTTON_OK'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // console.log('Confirm Cancel fields mantatory');
          }
        }
      ]
    });
    await alert.present();
  }

  setPublicOrPrivate() {
    console.log('post public ==> ', this.post.isPublic)
    this.post.photos.forEach(photo => {
      photo.isPublic = !this.post.isPublic;
      // Borramos tags existentes si la foto es privada (volvemos atras entre las fotos)
      // Borramos solo tags de usuario ???
    });
  }

}
