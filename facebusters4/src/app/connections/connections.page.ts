import { Subscription } from 'rxjs/Subscription';
import { UserService } from './../../providers/user-service/user-service';
import { TranslateService } from '@ngx-translate/core';
import { Events } from '@ionic/angular';
import { FollowService } from './../../providers/follow/follow.service';
import { Router } from '@angular/router';
import { StoreService } from './../../providers/store/store';
import { User, Connection } from './../../models/models';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-connections',
  templateUrl: './connections.page.html',
  styleUrls: ['./connections.page.scss'],
})
export class ConnectionsPage implements OnInit, OnDestroy{
  public user: User = null;
  public segmentSelected: string = 'following' || 'follower' || 'connection';
  public connectionsFollowingList: Connection[] = [];
  public connectionsFollowersList: Connection[] = [];
  public isMobile: boolean;
  private myUserSubscribe: Subscription;
  private followersLoading: boolean = false;
  private followingLoading: boolean = false;


  constructor(
    private router: Router,
    private _store: StoreService,
    private _followService: FollowService,
    private translate: TranslateService,
    private userService: UserService,
    public events: Events
  ) {
    this.segmentSelected = 'following';
    this.isMobile = this._store.get('isMobile');
    this.myUserSubscribe = this._store.$myUser.subscribe((user: User) => {
      this.user = user;
      if(!this.user || this.user===undefined){ this.initUser(); }else{ this.user._fullName(); }
    }, error => { console.error(error); });
    this.events.subscribe('refresh-connections',() => {
      this.doRefresh(null);
    });
  }

  ngOnInit(){
    this.doRefresh(null);
  }
  ngOnDestroy(){
    try {
      this.myUserSubscribe.unsubscribe();
    } catch (error) {
      console.error(error);
    }
  }

  ionViewWillEnter() {
    this.doRefresh(null);
  }

  setSegmentSelected(value: string) {
    this.segmentSelected = value;
  }
  initUser() {
    this.user = this._store.myUser;
    if (!this.user) {
      this.userService.getMyUser().subscribe((data: any) => {
        this.user = this._store.parserApiToModel('user', data);
        this._store.eventChangeMyUser(this.user);
      }, error => {
        console.error(error);
      });
    }
  }

  initConnection(data: any, connectionType: string = 'following' || 'follower' || 'connection') {
    let connection: Connection = new Connection();
    if (data.follow_back) { connection.connectionType = 'connection'; }
    else { connection.connectionType = connectionType; }
    let userData: any = null;
    if (connectionType === 'follower') { userData = data.follow_to_info; }
    else if (connectionType === 'following') { userData = data.user_info; }
    connection.user = new User();
    connection.user.id = userData.identifier;
    connection.user.avatar = userData.avatar_thumbnail; 
    connection.user.nickname = userData.username;
    connection.fb_follow = userData.fb_follow;
    return connection;
  }

  initConnectionsFollowingList() {
    if(this.followersLoading){ return; }
    this.followersLoading = true;
    this.connectionsFollowersList = [];
    this._followService.getFollowers().subscribe((data: any) => {
      data.forEach(e => { 
        if(e.user_info){
          if(this.user.id != e.user_info.identifier){
            this.connectionsFollowersList.push(this.initConnection(e, 'following')); 
          }
        }
      });
      this.followersLoading = false;
    }, error => {
      this.followersLoading = false;
      console.error(error);
    });
  }

  initConnectionsConnectedList() {
    if(this.followingLoading){ return; }
    this.followingLoading = true;
    this.connectionsFollowingList = [];
    this._followService.getFollowing().subscribe((data: any) => {
      data.forEach(e => { 
        if(e.user_info && e.follow_to_info){
          if(this.user.id == e.user_info.identifier){
            this.connectionsFollowingList.push(this.initConnection(e, 'follower')); 
          }
        }
      });
      this.followingLoading = false;
    }, error => {
      this.followingLoading = false;
      console.error(error);
    });
  }

  doRefresh(event) { // Up
    this.initConnectionsFollowingList();
    this.initConnectionsConnectedList();
    setTimeout(() => { 
      try { 
        event.target.complete(); 
      } catch (error) { } }, 1000);
  }

  goToSearchContacts(){
    this.router.navigateByUrl('/tabs/search');
    setTimeout(() => { this.events.publish('tab-users'); }, 200);
  }
  
  linkFacebook(){
    
  }
}
