import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TranslateService, TranslateModule } from '@ngx-translate/core';

import { SearchResultComponent } from './search-result.component';

const routes: Routes = [
  {
    path: '',
    component: SearchResultComponent
  }
];

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [SearchResultComponent]
})
export class SearchResultComponentModule {}
