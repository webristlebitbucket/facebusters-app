import { SearchResultMapComponent } from './../search-result-map/search-result-map.component';
import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from './../../services/data-observable';
import { StoreService } from './../../providers/store/store';
import { User, HashTag, Photo } from './../../models/models';
import { PostService } from './../../providers/post-service/post-service';
@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {

  public filters: any = null;
  public results: any = null;
  public isMobile: boolean;
  public photos: Photo[] = [];
  public photos_loaded: number = 0;
  public next_url: string = null;
  public prev_url: string = null;
  public segmentSelected: string = 'gallery' || 'map';
  public infinitScroll: any = null;

  constructor(
    private _data: DataService,
    private _store: StoreService,
    private router: Router,
    private _postService: PostService,
    public modalCtrl: ModalController
  ) {
    this.isMobile = this._store.get('isMobile');
    this.segmentSelected = 'gallery';
  }

  ngOnInit() {
    this._data.currentSearchFilters.subscribe(data => this.filters = data);
    this._data.currentSearchResults.subscribe(data => {
      this.results = data;
      if ('results' in this.results){
        if (this.results.results.length > 0) {
          this.initPhotos(this.results.results);

        }
        if (this.results.count > 20) {
          this.next_url = this.results.next;
        }
      }
    });
  }

  initPhotos(data) {
    this.photos = [];
    let i = 0;
    data.forEach(e => {
      this.photos.push(this.initPhoto(e));
      i++;
    });
    this.photos_loaded = i;
  }

  initPhoto(data: any) {
    const photo = new Photo();
    photo.id = data.identifier;
    photo.title = data.title;
    photo.image = data.image;
    photo.thumbnail = data.image_thumbnail;
    photo.isPublic = data.public;
    photo.createdAt = data.created_at;
    photo.hashTags = this.initHastags(data.image_tags);
    //photo.location = this.initLocation(data);
    photo.user = this.initUser(data.user_info);
    photo.filter = data.picture_filter;
    if (!photo.filter) { photo.filter = ''; }
    return photo;
  }


  initUser(data: any) {
    let user: User = new User();
    user = new User();
    if (data) {
      user.avatar = data.avatar_thumbnail;
      user.nickname = data.username;
      user.id = data.identifier;
    }
    return user;
  }

  initHastags(data: any[]) {
    let hashTags: HashTag[] = [];
    data.forEach(t => {
      const hashtag = new HashTag();
      hashtag.hashTag = t;
      hashTags.push(hashtag);
    });
    return hashTags;
  }

  addPhotosToArray(data) {
    let i = 0;
    data.forEach(e => {
      this.photos.push(this.initPhoto(e));
      i++;
    });
    this.photos_loaded += i;

    // this._data.updateSearchResults(this.photos);
  }

  loadMorePhotos(infinitScroll) {
    this._postService.searchNext(this.next_url).subscribe((data: any) => {
      this.next_url = data.next;
      this.prev_url = data.previous;
      this.addPhotosToArray(data.results);
      infinitScroll.target.complete();
      if(data.results.length==0){
        try {
          infinitScroll.target.disabled = true;
        } catch (error) {
        }
      }
    }, error => {
      try {
        infinitScroll.target.disabled = true;
      } catch (error) {
      }
    });
  }

  goToDetailPhoto(id_photo: number) {
    this._store.eventChangeTimelineDetailPhotoActive(null);
    this.router.navigateByUrl('tabs/photo/' + id_photo);
  }

  async goToSearchResultsMap() {
    const modal = await this.modalCtrl.create({
      component: SearchResultMapComponent
    });
    return await modal.present();

    // this.router.navigateByUrl('tabs/(timeline:full-album/' + idAlbum + '/' + pkAlbum+')');
    // this.router.navigateByUrl('full-album/' + idAlbum + '/' + pkAlbum);
  }

  // goToSearchResultsMap(){
  //   this.router.navigateByUrl('tabs/(search:search-result-map)');
  // }

  setSegmentSelected(value: string){
    this.segmentSelected = value;
  }

}
