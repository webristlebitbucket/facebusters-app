import { EditProfileComponent } from './../../edit-profile/edit-profile.component';
import { FollowService } from './../../../providers/follow/follow.service';
import { Router } from '@angular/router';
import { StoreService } from './../../../providers/store/store';
import { User, Connection } from './../../../models/models';
import { Component, OnInit, Input } from '@angular/core';
import { ToastController, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'c-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  @Input() dataInput: any = null;
  @Input() user: User = null;
  @Input() btnName: string = 'EDIT PROFILE';

  public txtBtnName: string = 'EDIT PROFILE';
  public isMobile: boolean;

  @Input() isMyUser: boolean = false;

  constructor(private modalCtrl: ModalController, private router: Router, private _followService: FollowService, private toastController: ToastController, private _store: StoreService, private translate : TranslateService) {
    this.isMobile = this._store.get('isMobile');
  }

  ngOnInit() {
    this.changeButtonFollowStatus();
  }
  changeButtonFollowStatus(){
    if(this.isMyUser){
      return;
    }
    if(this.user.following){
      this.btnName = 'UnFollow';
    }else if (!this.user.following){
      this.btnName = 'Follow';
    }

    if(this.btnName === 'Follow'){
      this.txtBtnName = this.translate.instant('USER_INFO_BUTTON_FOLLOW');
    }
    else if(this.btnName === 'UnFollow'){
      this.txtBtnName = this.translate.instant('USER_INFO_BUTTON_UNFOLLOW');
    }
    else{
      this.txtBtnName = this.translate.instant('USER_INFO_BUTTON_EDIT');
    }
  }

  actionButton(){
    switch (this.btnName) {
      case 'Follow':
        this.follow();
        break;
      case 'UnFollow':
        this.unFollow();
        break;
      case 'EDIT PROFILE':
        this.goEditProfile();
        break;
    }
  }

  goEditProfile(){
    this.presentModalEditProfile();
    // this.router.navigateByUrl('tabs/profile/edit');
  }

  unFollow() {
    this._followService.unfollowUser(this.user.id).subscribe(
      data => {
        this.user.following = false;
        this.presentToastUnFollow();
        this.changeButtonFollowStatus();
      }, error => { console.error(error); });
  }
  follow() {
    this._followService.followUser(this.user.id).subscribe(
      data => {
        this.user.following = true;
        this.presentToastFollow();
        this.changeButtonFollowStatus();
      }, error => { console.error(error); });
  }


  async presentToastFollow() {
    const toast = await this.toastController.create({
      message: this.translate.instant('CONNECTIONS_FOLLOWING_ADDED'),
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  async presentToastUnFollow() {
    const toast = await this.toastController.create({
      message: this.translate.instant('CONNECTIONS_FOLLOWING_REMOVED'),
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  async presentModalEditProfile() {
    const modal = await this.modalCtrl.create({
      component: EditProfileComponent
    });
    return await modal.present();
  }

}
