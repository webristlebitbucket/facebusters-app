import { ModalController } from '@ionic/angular';
import { Photo, Comment, User } from './../../../models/models';
import { PostService } from './../../../providers/post-service/post-service';
import { Component, OnInit, Input } from '@angular/core';
import { StoreService } from './../../../providers/store/store';

@Component({
  selector: 'app-modal-comments',
  templateUrl: './modal-comments.component.html',
  styleUrls: ['./modal-comments.component.scss']
})
export class ModalCommentsComponent implements OnInit {
  @Input() photo: Photo;
  comments: Comment[] = [];
  newComment: string = null;
  commentsVisibles: boolean = true;
  public isMobile: boolean;

  constructor(private _postService: PostService,
    private modalCtrl: ModalController,
    private _store: StoreService) {
    this.isMobile = this._store.get('isMobile');
  }

  ngOnInit() {
    this.getComments();
  }

  initComment(data: any) {
    const comment = new Comment();
    comment.id = data.identifier;
    comment.user = new User();
    comment.user.avatar = data.user_info.avatar_thumbnail;
    comment.user.nickname = data.user_info.username;
    comment.user.id = data.user_info.identifier;
    comment.comment = data.comment;
    comment.createdAt = data.created_at;
    return comment;
  }

  getComments() {
    this.comments = [];
    this._postService.getCommentsOfPicture(this.photo.id).subscribe((data: any) => {
      if (data.count > 0) {
        data.results.forEach(e => { this.comments.push(this.initComment(e)); });
      }
    }, error => { console.error(error); });
  }

  addComment() {
    if (!this.photo || !this.newComment) { return; }
    const data: any = {
      'picture': this.photo.id,
      'comment': this.newComment
    }
    this._postService.newComment(data).subscribe((data: any) => {
      this.newComment = null;
      this.getComments();
      this.presentToast();
    }, error => { console.error(error); });
  }

  presentToast(){

  }

  toogleShowHideComments(value: boolean){
    this.commentsVisibles = value;
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

}
