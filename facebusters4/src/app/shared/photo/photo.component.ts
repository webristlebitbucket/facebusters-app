import { StoreService } from './../../../providers/store/store';
import { Router } from '@angular/router';
import { Photo, Tag, User } from './../../../models/models';
import { Component, OnInit } from '@angular/core';
import { PostService } from './../../../providers/post-service/post-service';
import { Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss']
})
export class PhotoComponent implements OnInit {

  @Input() photo: Photo;
  modalTagPhoto: any;
  reticula: any = [];
  showTags: Boolean = false;
  showTextNoUserTags: Boolean = false;
  public isMobile: Boolean;
  loading: Boolean = false;

  constructor(
    private _modalCtrl: ModalController,
    private _postService: PostService,
    private router: Router,
    private _store: StoreService
  ) {
    this.isMobile = this._store.get('isMobile');
  }

  ngOnInit() {}

  goToDetailPhoto(photo: Photo) {
    // this._store.eventChangeTimelineDetailPhotoActive(photo);
    this.router.navigateByUrl('/tabs/photo/' + photo.id);
    // this.router.navigateByUrl('detail-photo/' + photo.id);
  }

  showUserTags(photo: Photo) {

    this.showTags = !this.showTags;
    if (this.showTags && this.photo.tags.length === 0) {
      this.loading = true;
      this._postService.getUserTags(this.photo.id).subscribe((data: any) => {
        if (data.count === 0 && !this.showTextNoUserTags) {
          this.showTextNoUserTags = true;
          const myThis = this;
          setTimeout(function () { myThis.showTextNoUserTags = false; this.showTags = false; }, 3000);
        } else {
          data.results.forEach(element => {
            const tagUser: Tag = new Tag();
            tagUser.id = element.identifier;
            tagUser.coords.row = element.top;
            tagUser.coords.col = element.left;
            tagUser.hide = element.hide;
            if (element.user) {
              tagUser.user = new User();
              tagUser.user.id = element.user_info.identifier;
              tagUser.user.avatar = element.user_info.avatar_thumbnail;
              tagUser.user.nickname = element.user_info.username;
            }
            if (element.non_user) {
              tagUser.nonUser = element.non_user;
            }
            this.photo.tags.push(tagUser);
          });
        }
        this.loading = false;
      }, error => { console.error(error); this.loading = false; });
    }
  }

  testCoordinates(event: any) {
    this.showTags = !this.showTags;
  }

  goPublicProfile() {
    if (this._store.get('myUser').id === this.photo.user.id) {
      this.router.navigateByUrl('tabs/my-profile');
    } else {
      this.router.navigateByUrl('tabs/profile/' + this.photo.user.id );
    }
  }

}
