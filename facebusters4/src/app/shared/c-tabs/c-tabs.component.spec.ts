import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CTabsComponent } from './c-tabs.component';

describe('CTabsComponent', () => {
  let component: CTabsComponent;
  let fixture: ComponentFixture<CTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
