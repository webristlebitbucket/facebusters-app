import { NewPostPage } from './../../new-post/new-post.page';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'c-tabs',
  templateUrl: './c-tabs.component.html',
  styleUrls: ['./c-tabs.component.scss']
})
export class CTabsComponent implements OnInit {

  constructor(private router: Router, private modalController: ModalController) { }

  ngOnInit() {
  }

  goTimeLine() {
    this.router.navigateByUrl('timeline');
  }
  goSearch() {
    this.router.navigateByUrl('search');
  }
  goConnections() {
    this.router.navigateByUrl('connections');
  }
  goMyProfile() {
    this.router.navigateByUrl('my-profile');
  }
  goNotifications() {
    this.router.navigateByUrl('notifications');
  }

  async openModalNewpost() {
    const modal = await this.modalController.create({
      component: NewPostPage,
      componentProps: { value: 123 }
    });
    modal.onDidDismiss().then((data: any) => {
      this.router.navigateByUrl('timeline');
    })
    return await modal.present();
  }

}
