import { UserService } from './../../../providers/user-service/user-service';
import { User } from './../../../models/models';
import { Component, Input } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { StoreService } from './../../../providers/store/store';
@Component({
  selector: 'app-modal-search-user',
  templateUrl: './modal-search-user.component.html',
  styleUrls: ['./modal-search-user.component.scss']
})
export class ModalSearchUserComponent {
  @Input() isMyPhoto: boolean = false;
  @Input() isPhotoPrivate: boolean = false;
  searchInput: string = null;
  users: User [] = [];
  usersConnected: User [] = [];
  usersNotConnected: User [] = [];
  myUser: User = null;
  showMyUser: boolean = false;

  constructor(private navParams: NavParams, private store:StoreService, private modalCtrl: ModalController, private _userService: UserService) {
    this.myUser = this.store.get('myUser');
    // this.isPhotoPrivate= this.navParams.get('isPrivate');

    console.log('myUser', this.myUser.id);
    console.log('isPhotoPrivate', this.isPhotoPrivate);
    console.log('isMyPhoto', this.isMyPhoto);
  }

  searchUsers(event: any){
    this.users = [];
    this.usersConnected = [];
    this.usersNotConnected = [];
    this.showMyUser = false;
    this.searchInput = event.detail.value;

    console.log(this.searchInput);
    console.log(this.isPhotoPrivate);

    if(this.isPhotoPrivate){ return; } // if is private only nonusertags

    if (this.searchInput && this.searchInput.length>3) {
      this._userService.searchUser(this.searchInput).subscribe((data: any) => {
        // console.log(data)
        data.forEach(e => {
          console.log(e.username)
          let user: User = new User();
          user.id = e.identifier;
          user.nickname = e.username;
          user.avatar = e.avatar_thumbnail;
          user.firstName = e.first_name;
          user.lastName = e.last_name;
          user.connected = e.connection;

          if(user.id === this.myUser.id){
            this.showMyUser = true;
            this.myUser = user;
          }else{
            if(user.connected){
              this.usersConnected.push(user);
            }else{
                this.usersNotConnected.push(user);
            }
          }
          
          
        });
        console.log('Results ==> ', data.length)
        console.log(this.usersConnected.length);
        console.log(this.usersNotConnected.length);
        
      }, error => { console.error(error); });
    }
  }


  selectUser(user: User){
    this.modalCtrl.dismiss(user);
  }

  selectNonUser(){
    const nonUser: any = {
      'nonUser': true,
      'name': this.searchInput
    }
    this.modalCtrl.dismiss(nonUser);
  }

  dismiss(){
    this.modalCtrl.dismiss();
  }

}
