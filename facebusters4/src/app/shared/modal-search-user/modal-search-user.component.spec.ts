import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSearchUserComponent } from './modal-search-user.component';

describe('ModalSearchUserComponent', () => {
  let component: ModalSearchUserComponent;
  let fixture: ComponentFixture<ModalSearchUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSearchUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSearchUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
