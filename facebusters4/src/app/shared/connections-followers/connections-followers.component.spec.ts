import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectionsFollowersComponent } from './connections-followers.component';

describe('ConnectionsFollowersComponent', () => {
  let component: ConnectionsFollowersComponent;
  let fixture: ComponentFixture<ConnectionsFollowersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectionsFollowersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionsFollowersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
