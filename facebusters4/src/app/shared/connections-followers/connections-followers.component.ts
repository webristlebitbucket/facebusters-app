import { StoreService } from './../../../providers/store/store';
import { Router } from '@angular/router';
import { Connection } from './../../../models/models';
import { FollowService } from './../../../providers/follow/follow.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToastController, Events } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-connections-followers',
  templateUrl: './connections-followers.component.html',
  styleUrls: ['./connections-followers.component.scss']
})
export class ConnectionsFollowersComponent implements OnInit {

  @Input() connectionsFollowers: Connection[];
  @Output() change: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private _store: StoreService,
    private router: Router,
    private toastController: ToastController,
    private _followService: FollowService,
    private toastCtrl: ToastController,
    private translate : TranslateService,
    private events: Events
  ){}

  ngOnInit() {
  }

  follow(connection: Connection) {
    this._followService.followUser(connection.user.id).subscribe(
      data => {
        this.change.emit(true);
        this.presentToastFollow();
      }, error => { console.error(error); });
  }

  goToPublicProfile(connection: any) {
    this.router.navigateByUrl('tabs/profile/' + connection.user.id);
  }

  async presentToastFollow() {
    const toast = await this.toastController.create({
      message: this.translate.instant('CONNECTIONS_FOLLOWING_ADDED'),
      position: 'bottom',
      duration: 2000
    });
    toast.present();
    this.events.publish('refresh-connections');
  }

}
