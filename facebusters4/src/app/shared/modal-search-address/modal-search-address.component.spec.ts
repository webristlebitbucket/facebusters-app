import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSearchAddressComponent } from './modal-search-address.component';

describe('ModalSearchAddressComponent', () => {
  let component: ModalSearchAddressComponent;
  let fixture: ComponentFixture<ModalSearchAddressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalSearchAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSearchAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
