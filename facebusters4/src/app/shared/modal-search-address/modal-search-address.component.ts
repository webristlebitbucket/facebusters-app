/// <reference path="../../../../node_modules/@types/googlemaps/index.d.ts" />
/// <reference types="@types/googlemaps" />

import { Photo, Location } from './../../../models/models';
import { DataService } from './../../../services/data-observable';
import { StoreService } from './../../../providers/store/store';


import { SearchAddressService } from './../../../providers/search/search';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Platform } from '@ionic/angular';

import { Router } from '@angular/router';



@Component({
  selector: 'app-modal-search-address',
  templateUrl: './modal-search-address.component.html',
  styleUrls: ['./modal-search-address.component.scss']
})
export class ModalSearchAddressComponent implements OnInit {

  //needed for googlemaps
  map: google.maps.Map;
  markers = [];


  public isMobile: boolean;
  public filters: any = null;
  public results: any = null;
  public photos: Photo[] = [];

  inputAddress: string = null;
  addressResults: any[] = [];
  myLocation: any = {
    'longitude': null,
    'latitude': null,
    'address': null,
    'place_id': null
  };


  myLocationData: any = {};
  markerActive: any = null;
  locationSelected: any = {};

  constructor(
    private _searchAddressService: SearchAddressService,
    private modalCtrl: ModalController,
    private geolocation: Geolocation,
    private _data: DataService,
    private _store: StoreService,
    private router: Router,
    public platform: Platform
  ) {
    this.isMobile = this._store.get('isMobile');
    this.getCurrentLocation();
  }

  ngOnInit() {
    this.getCurrentLocation();

    this.platform.ready().then(() => {
      this.geolocation.getCurrentPosition().then((resp) => {
        this.myLocation.latitude = resp.coords.latitude;
        this.myLocation.longitude = resp.coords.longitude;
        this.initGoogleMaps();

      });
    });
  }

  searchAddress(event) {
    if (this.inputAddress.length < 3) {
      this.clearAddresses();
      return;
    }
    this._searchAddressService.searchAddress(this.inputAddress).subscribe((data: any) => {
      this.addressResults = data.results;
    }, error => { console.error(error); });
  }

  selectAddresFromList(item: any) {
    let dataLocation: any = {
      'longitude': null,
      'latitude': null,
      'address': item.formatted,
      'place_id': item.place_id   
    };
    let myThis = this;    
    this._searchAddressService.searchAddressByPlaceId(dataLocation.place_id).subscribe((data: any) => {
      myThis.addressResults = [];
      dataLocation.latitude = data.results[0].geometry.lat;
      dataLocation.longitude = data.results[0].geometry.lon;
      dataLocation.address = data.results[0].formatted;
      myThis.markerActive = dataLocation;
      let gmapLocation = new google.maps.LatLng(Number(dataLocation.latitude), Number(dataLocation.longitude))
      myThis.map.setCenter(gmapLocation);
      myThis.setGoogleMapLocation(myThis.map, gmapLocation);
    }, error => { console.error(error); });
  }

  dismiss(data: any) {
    this.modalCtrl.dismiss(data);
  }

  clearAddresses() {
    this.addressResults = [];
  }

  getCurrentLocation() {
    let myThis = this;
    this.geolocation.getCurrentPosition().then((resp) => {
      try {
        this.myLocation.latitude = resp.coords.latitude;
        this.myLocation.longitude = resp.coords.longitude;
        this._searchAddressService.searchAddressByCoords({ 'lat': this.myLocation.latitude, 'lon': this.myLocation.longitude }).subscribe((data: any) => {
          this.myLocationData = data;
          this.myLocation.address = data.address;
        }, error => { console.error(error); });
      } catch (error) { 
        console.error(error); 
        this.myLocation = null;
      }
    }).catch((error) => {
      console.error('Error getting location', error);
      this.myLocation = null;
    });
  }

  useCurrentLocation() {
    this.setGoogleMapLocation(this.map, this.myCurrentPosition);
  }

  setGoogleMapLocation(map: any, location: any) {
    // Add marker
    var marker = new google.maps.Marker({
      position: location,
      title: 'Current location: ',
    });

    this.markers = [];
    this.markers.push(marker);
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
    map.setCenter(location);
  }
  myCurrentPosition: any;

  initGoogleMaps() {
    this.myCurrentPosition = new google.maps.LatLng(Number(this.myLocation.latitude), Number(this.myLocation.longitude));
    var mapProp = {
      center: this.myCurrentPosition,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    };
    this.map = new google.maps.Map(document.getElementById('googleMapsWrapperSearch'), mapProp);
    this.map.setCenter(this.myCurrentPosition);
    this.setGoogleMapLocation(this.map, this.myCurrentPosition);
  }

  selectCurrentLocationToReturn() {
    const dataToReturn: any = {
      'formated': this.myLocation.address,
      'geometry': {
        'lat': this.myLocation.latitude,
        'lng': this.myLocation.longitude
      }
    }

    this._searchAddressService.searchAddressByCoords({ 'lat': dataToReturn.geometry.lat, 'lon': dataToReturn.geometry.lng }).subscribe((data: any) => {
      this.myLocationData = data;
      this.myLocation.address = data.address;

      dataToReturn.formated = data.address;
      this.dismiss(dataToReturn);

    }, error => { console.error(error); });
  }

  selectMarkerToReturn() {
    const dataToReturn: any = {
      'formated': this.markerActive.address,
      'geometry': {
        'lat': this.markerActive.latitude,
        'lng': this.markerActive.longitude
      }
    }
    this.dismiss(dataToReturn);
  }
}


