import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectionsFollowingComponent } from './connections-following.component';

describe('ConnectionsFollowingComponent', () => {
  let component: ConnectionsFollowingComponent;
  let fixture: ComponentFixture<ConnectionsFollowingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectionsFollowingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectionsFollowingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
