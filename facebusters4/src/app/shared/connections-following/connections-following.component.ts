import { StoreService } from './../../../providers/store/store';
import { Router } from '@angular/router';
import { Connection } from './../../../models/models';
import { FollowService } from './../../../providers/follow/follow.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToastController, Events } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-connections-following',
  templateUrl: './connections-following.component.html',
  styleUrls: ['./connections-following.component.scss']
})
export class ConnectionsFollowingComponent implements OnInit {
  @Input() connectionsFollowing: Connection[];
  @Output() change: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private _store: StoreService,
    private _followService: FollowService,
    private router: Router,
    private toastController: ToastController,
    private translate : TranslateService,
    private events: Events
  ) {}

  ngOnInit() {}

  unFollow(connection: Connection) {
    this._followService.unfollowUser(connection.user.id).subscribe(
      data => {
        this.change.emit(true);
        this.presentToastUnFollow();
      }, error => { console.error(error); });
  }

  goToPublicProfile(connection: any) {
    this.router.navigateByUrl('tabs/profile/' + connection.user.id);
  }

  async presentToastUnFollow() {
    const toast = await this.toastController.create({
      message: this.translate.instant('CONNECTIONS_FOLLOWING_REMOVED'),
      position: 'bottom',
      duration: 2000
    });
    toast.present();
    this.events.publish('refresh-connections');
  }

}
