import { ModalFacebookContactsComponent } from './modal-facebook/modal-facebook';
import { AppHeaderComponent } from './app-header/app-header.component';
// import { NotificationsPage } from './../notifications/notifications.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

import { PhotoComponent } from './photo/photo.component';
import { AlbumComponent } from './album/album';
import { UserInfoComponent } from './user-info/user-info.component';
import { ConnectionsFollowersComponent } from './connections-followers/connections-followers.component';
import { ConnectionsFollowingComponent } from './connections-following/connections-following.component';
import { ModalTagUserComponent } from './modal-tag-user/modal-tag-user.component';
import { ModalSearchUserComponent } from './modal-search-user/modal-search-user.component';
import { ModalCommentsComponent } from './modal-comments/modal-comments.component';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { ModalSearchAddressComponent } from './modal-search-address/modal-search-address.component';
import { CTabsComponent } from './c-tabs/c-tabs.component';


@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    IonicModule.forRoot(),
    TranslateModule
  ],
  declarations: [
    PhotoComponent,
    AlbumComponent,
    UserInfoComponent,
    // NotificationsPage,
    AppHeaderComponent,
    ConnectionsFollowersComponent,
    ConnectionsFollowingComponent,
    ModalTagUserComponent,
    ModalSearchUserComponent,
    ModalCommentsComponent,
    ModalSearchAddressComponent,
    CTabsComponent,
    ModalFacebookContactsComponent
  ],
  exports: [
    PhotoComponent,
    AlbumComponent,
    UserInfoComponent,
    // NotificationsPage,
    AppHeaderComponent,
    ConnectionsFollowersComponent,
    ConnectionsFollowingComponent,
    ModalTagUserComponent,
    ModalSearchUserComponent,
    ModalCommentsComponent,
    ModalSearchAddressComponent,
    CTabsComponent,
    ModalFacebookContactsComponent
  ],
  entryComponents: [
    // NotificationsPage,
    AppHeaderComponent,
    ConnectionsFollowersComponent,
    ConnectionsFollowingComponent,
    ModalTagUserComponent,
    ModalSearchUserComponent,
    ModalCommentsComponent,
    ModalSearchAddressComponent,
    CTabsComponent,
    ModalFacebookContactsComponent
  ],
})
export class SharedModule {}
