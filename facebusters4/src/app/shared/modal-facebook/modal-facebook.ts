import { TranslateService } from '@ngx-translate/core';
import { ModalSearchUserComponent } from './../modal-search-user/modal-search-user.component';
import { PostService } from './../../../providers/post-service/post-service';
import { ModalController, ToastController, AlertController } from '@ionic/angular';
import { Photo, User, Tag, TagCoords } from './../../../models/models';
import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { StoreService } from './../../../providers/store/store';

@Component({
  selector: 'app-modal-facebook-contacts',
  templateUrl: './modal-facebook.html',
  styleUrls: ['./modal-facebook.scss']
})
export class ModalFacebookContactsComponent implements OnInit {

  @Input() data: any;

  items: any [] = [];

  constructor(
    public modalCtrl: ModalController,
  
  ) {}

  ngOnInit() {
    this.initItems();
  }

  initItems(){
    let mock = [
      {'avatar': 'https://res.cloudinary.com/practicaldev/image/fetch/s--r_-U0Xh5--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://thepracticaldev.s3.amazonaws.com/i/2agepg2y2zb560ro2kcu.jpeg', 'name': 'Eric TB'},
      {'avatar': 'https://s3-eu-west-1.amazonaws.com/jeka-public/galerie/300/ITALIA/Italia%20-%20Colloseum.jpg', 'name': 'Sara WR'},
      {'avatar': 'https://static2.lasprovincias.es/www/multimedia/201801/08/media/cortadas/ciudad-artes-kXbB-U50574220968wkD-624x385@Las%20Provincias.jpg', 'name': 'Alberto Cactus'},
    ];
    this.items = [];

    mock.forEach(element => {
      this.items.push(element);
    });
    
    


  }

  dismiss() {
    this.modalCtrl.dismiss();
  }


}