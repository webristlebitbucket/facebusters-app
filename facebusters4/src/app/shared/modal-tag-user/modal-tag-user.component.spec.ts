import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTagUserComponent } from './modal-tag-user.component';

describe('ModalTagUserComponent', () => {
  let component: ModalTagUserComponent;
  let fixture: ComponentFixture<ModalTagUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTagUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTagUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
