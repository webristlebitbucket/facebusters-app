import { TranslateService } from '@ngx-translate/core';
import { ModalSearchUserComponent } from './../modal-search-user/modal-search-user.component';
import { PostService } from './../../../providers/post-service/post-service';
import { ModalController, ToastController, AlertController } from '@ionic/angular';
import { Photo, User, Tag, TagCoords } from './../../../models/models';
import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { StoreService } from './../../../providers/store/store';

@Component({
  selector: 'app-modal-tag-user',
  templateUrl: './modal-tag-user.component.html',
  styleUrls: ['./modal-tag-user.component.scss']
})
export class ModalTagUserComponent implements OnInit {

  @Input() photo: Photo;
  @Input() album_index: number;
  @Input() isNewPhoto: boolean = false;
  @Input() isMyPhoto: boolean = false;
  
  coordsTemp: TagCoords = new TagCoords();
  isMobile: boolean = true;
  myUser: User = null;

  constructor(
    public modalCtrl: ModalController,
    private _postService: PostService,
    private _store: StoreService,
    public toastController: ToastController,
    private translate: TranslateService,
    private alertController: AlertController,
  ) {}

  ngOnInit() {
    this.myUser = this._store.get('myUser');
    if(this.myUser.id == this.photo.user.id){
      this.isMyPhoto = true;
    }else{
      this.isMyPhoto = false;
    }
    this.getUserTags();
  }

  addTag(event) {
    var photoToTag = document.getElementById('photoToTag-' + this.album_index);
    // BUG: No sabemos el width y Height, posiblemente por no existir aún la etiqueta photoToTag

    var widthPicture = photoToTag.clientWidth;
    var heightPicture = photoToTag.clientHeight;

    const offsetX = (45 * 100 / widthPicture);
    const offsetY = (30 * 100 / widthPicture);
    this.coordsTemp.col = (event.offsetX * 100 / widthPicture) - offsetX;
    this.coordsTemp.row = (event.offsetY * 100 / heightPicture) - offsetY;
    // console.info("click -> X: " + this.coordsTemp.col + ". Y: " + this.coordsTemp.row);
    this.showModalSearchUser();

    if (!this.isMobile) {
      this.showWhoIs();
    }
  }

  showWhoIs() {
    let who_is_tag: any = document.getElementById('who_is_' + this.album_index);
    who_is_tag.style.left = this.coordsTemp.col + "%";
    who_is_tag.style.top = this.coordsTemp.row + "%";
    who_is_tag.classList.remove("d-none");
    document.getElementById('search-user-section-' + this.album_index).classList.remove("d-none");
  }

  async showModalSearchUser() {
    console.log(this.photo.isPublic)
    const modal = await this.modalCtrl.create({
      component: ModalSearchUserComponent,
      componentProps: { 'isMyPhoto': this.isMyPhoto, 'isPhotoPrivate': !this.photo.isPublic } 
    });
    let myThis = this;

    modal.onDidDismiss().then((data) => {
      if (data.data) {
        const tag: Tag = new Tag();
        tag.coords = Object.assign({}, this.coordsTemp);
        if ('nonUser' in data.data) { // Non User
          tag.nonUser = data.data.name;
          this.saveTagNonUser(tag);
          
        } else { // User from list
          tag.user = data.data;
          let check: boolean = false;  // check if user has exists
          this.photo.tags.forEach((el, index) => {
            if(el.user){
              if (el.user.id === tag.user.id) { // already exists
                // this.presentAlertError();
                // myThis.photo.tags[index] = tag;
                check = true;
              }
            }
          });
          // if (!check) {
          //   myThis.photo.tags.push(tag);
          // }
          myThis.saveTag(tag); // new tag
          
        }
      }
    })

    // Get returned data
    // const { data } = await modal.onWillDismiss();
    return await modal.present();

  }

  removeTag(tag: Tag) {
    this.photo.tags.forEach((element, index) => {
      if (element.user.id === tag.user.id) {
        const dataTosend: any = {
          'picture': this.photo.id,
          'user': element.user.id,
          'status': 'remove',
          'top': element.coords.row,
          'left': element.coords.col
        }
        this._postService.removeUserTag(dataTosend).subscribe((dataResponse: any) => {
          this.photo.tags.forEach((element, i) => {
            if (element.user.id === dataTosend.user) {
              this.photo.tags.splice(i, 1);
              document.getElementById('who_is_' + this.album_index).classList.add("d-none");
            }
          });
        }, error => {
          console.error(error);
          this.photo.tags.forEach((element, i) => {
            if (element.user.id === dataTosend.user) { this.photo.tags.splice(i, 1); }
          });
        });
      }
    });
  }

  getUserTags() {
    if(!this.isNewPhoto){
      let myThis = this;
      this._postService.getUserTags(myThis.photo.id).subscribe((data: any) => {
        myThis.photo.tags = [];
        data.results.forEach(element => {
          let tagUser: Tag = new Tag();
          tagUser.id = element.identifier;
          tagUser.coords.row = element.top;
          tagUser.coords.col = element.left;
          tagUser.hide = element.hide;
          if(element.user){
            tagUser.user = new User();
            tagUser.user.id = element.user_info.identifier;
            tagUser.user.avatar = element.user_info.avatar_thumbnail;
            tagUser.user.nickname = element.user_info.username;
          }
          if(element.non_user){
            tagUser.nonUser = element.non_user;
          }
          myThis.photo.tags.push(tagUser);
        });
      }, error => { console.error(error); });
    }else{

    }
    
  }

  saveTagNonUser(tag: Tag){
    if(!this.isNewPhoto){
      const dataToSend: any = {
        'picture': this.photo.id,
        'status': 'add',
        'top': tag.coords.row,
        'left': tag.coords.col,
        'non_user': tag.nonUser,
        'hide': false
      }
      this._postService.addUserTag(dataToSend).subscribe((data: any) => {
        // this.getUserTags();
        this.presentToast();
        this.photo.tags.push(tag);
      }, error => { console.error(error); });
    }else{ // New tag
      let exists: boolean = false;
      let count: number = 0;
      this.photo.tags.forEach(element => {
        if(tag.nonUser && element.nonUser){
          console.log('tag NON user')
          if(element.nonUser=== tag.nonUser){
            exists = true;
            this.photo.tags[count] = tag;
          }
        }
        count++;
      });
      if(!exists){
        this.photo.tags.push(tag);
      }
    }
  }

  saveTag(tag: Tag) {
    if(!this.isNewPhoto){  // Edit picture
      const dataToSend: any = {
        'picture': this.photo.id,
        'user': tag.user.id,
        'status': 'add',
        'top': tag.coords.row,
        'left': tag.coords.col
      }
      this._postService.addUserTag(dataToSend).subscribe((data: any) => {
        // this.getUserTags();
        this.presentToast();
        tag.hide = data.hide;
        this.photo.tags.push(tag);
      }, error => { 
        console.error(error); 
        this.presentAlertError();
      });
    }else{  // new photo
      // check if exists
      let exists: boolean = false;
      let count: number = 0;
      this.photo.tags.forEach(element => {
        if(tag.user && element.user){
          console.log('tag user')
          if(element.user.id === tag.user.id){
            exists = true;
            this.photo.tags[count] = tag;
          }
        }
        count++;
      });
      if(!exists){
        this.photo.tags.push(tag);
      }
    }
  }

  dismiss() {
    const data = { 'tags': this.photo.tags };
    this.modalCtrl.dismiss(data);
  }

  closeTagsPanel() {
    var tags_panel = document.getElementById('tag-panel-' + this.album_index);
    tags_panel.classList.add("d-none");
    document.getElementById('who_is_' + this.album_index).classList.add("d-none");
    document.getElementById('search-user-section-' + this.album_index).classList.add("d-none");
  }


  async presentToast() {
    const toast = await this.toastController.create({
      message: this.translate.instant('TAG_ADDED_MESSAGE'),
      duration: 2000
    });
    toast.present();
  }

  async presentAlertError() {
    const alert = await this.alertController.create({
      message: this.translate.instant('TAG_ERROR_MESSAGE'),
      buttons: [
        {
          text: this.translate.instant('GENERIC_POPUP_BUTTON_ERROR'),
          handler: () => {
            // this.goToMyProfile();
          }
        }
      ]
  });

  await alert.present();
}
}
