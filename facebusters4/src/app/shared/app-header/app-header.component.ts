import { UserService } from './../../../providers/user-service/user-service';
import { Router } from '@angular/router';
import { StoreService } from './../../../providers/store/store';
import { NotificationsPage } from './../../notifications/notifications.page';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Location } from '@angular/common';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { AlertController, ModalController, ActionSheetController, Events } from '@ionic/angular';
import { Photo, Tag, User } from '../../../models/models';
import { PostService } from '../../../providers/post-service/post-service';
import { Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent implements OnInit, OnDestroy {

  @Input() title: string = 'Facebusters';
  @Input() showAvatar: boolean = true;
  @Input() notifications: boolean = true;

  @Input() refreshIcon: boolean = false;

  @Input() back: boolean = false;
  @Input() close: boolean = false;
  @Output() evtDismiss: EventEmitter<any> = new EventEmitter();

  @Input() options: boolean = false;
  @Output() openOptions: EventEmitter<any> = new EventEmitter();
  @Input() saveButton: boolean = false;
  @Output() save: EventEmitter<any> = new EventEmitter();


  myUser: User;
  newNotifications: boolean = true;
  notificationsCount: number = 0;

  private subscriptionNotificationsCount: Subscription;
  private subscriptionMyUser: Subscription;
  
  constructor(
    private router: Router,
    private _store: StoreService,
    public modalController: ModalController,
    private alertCtrl: AlertController,
    private actionSheetCtrl: ActionSheetController,
    private _postService: PostService,
    private location: Location,
    private userService: UserService,
    private events: Events
  ) {
    this.subscriptionNotificationsCount = this._store.$notificationsCount.subscribe((data: any) => {
      this.notificationsCount = data;
    }, error => { console.error(error); });

    this.subscriptionMyUser = this._store.$myUser.subscribe((user: User) => {
      this.myUser = user;
    }, error => { console.error(error); });
  }

  ngOnInit() {
    if(!this.showAvatar){ return; }
    this.myUser = this._store.myUser;
    if (!this.myUser || this.myUser === undefined) {
      this.userService.getMyUser().subscribe((data: any) => {
        this.myUser = this._store.parserApiToModel('user', data)
        this._store.eventChangeMyUser(this.myUser);
      }, error => { console.error(error); 
      });
    }
  }

  refresh(){

    switch (this.title) {
      case 'Facebusters':
        this.events.publish('refresh-timeline');
        break;
      case 'Contacts':
        this.events.publish('refresh-connections');
        break;
      case 'My Profile':
        this.events.publish('refresh-my-profile');
        break;
    }
  }

  async presentModalNotifications() {
    const modal = await this.modalController.create({
      component: NotificationsPage,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }

  goToMyProfile() {
    if (this.showAvatar) {
      this.router.navigateByUrl('tabs/(my-profile:my-profile)');
      // this.router.navigateByUrl('my-profile');
    }
  }
  goBack() {
    if (this.close) {
      this.dismiss();
    }
    else {
      this.location.back();
    }
  }
  dismiss() {
    this.evtDismiss.emit(null);
    this.modalController.dismiss();
  }

  presentActionSheet() {
    this.openOptions.emit(null);
  }

  saveClick() {
    this.save.emit(null);
  }
  ngOnDestroy(){
    try {
      if(this.subscriptionNotificationsCount){
        this.subscriptionNotificationsCount.unsubscribe();
      }
      if(this.subscriptionMyUser){
        this.subscriptionMyUser.unsubscribe();
      }
    } catch (error) {
      console.error(error);
    }
  }

}
