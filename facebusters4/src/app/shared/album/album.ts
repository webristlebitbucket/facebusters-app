import { Router } from '@angular/router';
import { StoreService } from './../../../providers/store/store';
import { Album, Photo } from './../../../models/models';
import { Component, Input, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'app-album',
  templateUrl: 'album.html',
  styleUrls: ['./album.scss']
})
export class AlbumComponent implements OnInit {
  @Input() album: Album;
  public photoActual: Photo;
  private positionPhotoActual: number;
  public isMobile: boolean;
  public slideOpts = {
    zoom: false
  };

  constructor(private _store: StoreService, private router: Router) {
    this.positionPhotoActual = 0;
    this.isMobile = this._store.get('isMobile');
  }

  ngOnInit() {
    this.photoActual = this.setPhotoActual(this.album, this.positionPhotoActual);
  }

  setPhotoActual(album: Album, position: number) {
    if (album.photos.length === 0) { return null; }
    let photo: Photo = null;
    photo = album.photos[position];
    return photo;
  }

  goToDetailAlbum(album: Album) {
    this.router.navigateByUrl('tabs/album/' + album.id + '/' + album.pk);
  }

  goPublicProfile(user) {
    if (this._store.get('myUser').id === user.id) {
      this.router.navigateByUrl('tabs/my-profile');
    } else {
      this.router.navigateByUrl('tabs/profile/' + user.id );
    }
  }

}
