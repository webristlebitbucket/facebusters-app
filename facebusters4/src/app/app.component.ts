import { UserService } from './../providers/user-service/user-service';
import { Router } from '@angular/router';
import { StoreService } from './../providers/store/store';
import { Component } from '@angular/core';
import { Platform, ToastController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
// import { OneSignal } from '@ionic-native/onesignal';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public is_android: boolean = false;
  public is_ios: boolean = false;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private _store: StoreService,
    private _userService: UserService,
    private router: Router,
    private screenOrientation: ScreenOrientation,
    private translate: TranslateService,
    private globalization: Globalization,
    public toastController: ToastController,
    public alertController: AlertController,
    private oneSignal: OneSignal
  ) {
    this.initializeApp();

  }

  initializeApp() {

    var player_id = null;

    this.platform.ready().then(() => {
      if (this.platform.is('cordova') && (this.platform.is('ios') || this.platform.is('android'))) {
        try { this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT_PRIMARY); } catch (error) { console.error(error); }

        this.platform.backButton.subscribeWithPriority(1, () => {
          // do nothing here
          console.log('back button');
        });





        // var notificationOpenedCallback = function (jsonData) {
        //   console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        // };
        // try {
        //   window["plugins"].OneSignal
        //     .startInit("YOUR_APPID", "YOUR_GOOGLE_PROJECT_NUMBER_IF_ANDROID")
        //     .handleNotificationOpened(notificationOpenedCallback)
        //     .endInit();

        //   window["plugins"].OneSignal.getIds(function (ids) {
        //     player_id = ids.userId;
        //   });

        // } catch (error) { }
      }

      this.globalization.getPreferredLanguage().then(res => {
        if (res) {
          this._store.eventChangeLanguage(res.value);
        }
      }).catch(e => console.log(e));

      this._initTranslate();
      //this.statusBar.overlaysWebView(true);
      //this.statusBar.styleDefault();
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
      this.checkToken();
    });
  }

  checkToken() {
    this._store.set('token', localStorage.getItem('token')); // Set Token From LocalStorage
    if (!this._store.get('token')) {
      console.log('no token');
    } else {
      // Gey My user
      this._store.set('tokenTouchId', this._store.get('token'));
    }
  }

  openTimeLinePage() {
    // this.router.navigateByUrl('timeline');
    this.router.navigateByUrl('/tabs/timeline');
  }

  openAccessPage() {
    this.router.navigateByUrl('access');
  }

  private _initTranslate() {
    this.translate.setDefaultLang('en');
    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    }
    else {
      this.translate.use('en');
    }
  }


  private async presentToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 3000
    });
    toast.present();
  }


}
