/// <reference path="../../../node_modules/@types/googlemaps/index.d.ts" />
/// <reference types="@types/googlemaps" />
import { PostService } from './../../providers/post-service/post-service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Platform, ModalController } from '@ionic/angular';
import { StoreService } from './../../providers/store/store';
import { DataService } from './../../services/data-observable';
import { Photo, Location } from './../../models/models';
import { Router } from '@angular/router';
declare var MarkerClusterer: any;
@Component({
  selector: 'app-search-result-map',
  templateUrl: './search-result-map.component.html',
  styleUrls: ['./search-result-map.component.scss']
})
export class SearchResultMapComponent implements OnInit {

  // needed for googlemaps
  map: google.maps.Map;
  loading: any;

  public isMobile: Boolean;
  public filters: any = null;
  public results: any = null;
  public photos: Photo[] = [];
  public markers: any[] = [];


  constructor(
    private _postService: PostService,
    private _data: DataService,
    private _store: StoreService,
    private router: Router,
    public platform: Platform,
    public modalCtrl: ModalController
  ) {
    this.isMobile = this._store.get('isMobile');

    this.platform.ready().then(() => {
      // this.initNeededData();
    });
  }

  ngOnInit() {
    this.filters = this._store.filters;
    this.initNeededData();
  }


  /**
   * Obtener el centro de las fotos pintadas en el mapa
   * coordenadas polares
   * @param photos array de imagenes con su lat lng
   */
  getCenterOfPhotos(photos: Photo[]) {
    const latlng = [Number(photos[0].location.latitude), Number(photos[0].location.longitude)];
    if (photos.length == 1) {
      return latlng;
    }

    let x = 0;
    let y = 0;
    let z = 0;

    photos.forEach(photo => {
      let lat = Number(photo.location.latitude) * Math.PI / 180;
      const lng = Number(photo.location.longitude) * Math.PI / 180;

      x += Math.cos(lat) * Math.cos(lng);
      y += Math.cos(lat) * Math.sin(lng);
      z += Math.sin(lat);
    });

    const total = photos.length;
    x = x / total;
    y = y / total;
    z = z / total;

    const centralLng = Math.atan2(y, x);
    const centralSquareRoot = Math.sqrt(x * x + y * y);
    const centralLat = Math.atan2(z, centralSquareRoot);

    latlng[0] = centralLat * 180 / Math.PI;
    latlng[1] = centralLng * 180 / Math.PI;

    return latlng;
  }


  /**
   * ONLY DEBUG PURPOSES
   * @param photos
   */
  showCoordinates(photos: Photo[]) {
    let i = 1;
    photos.forEach(photo => {
      // console.log("Photo " + i + ": (" + photo.location.latitude + ", " + photo.location.longitude + ")");
      i++;
    });
  }

  /**
   * Returns an Array with only significant locations
   *
   * @param photos array of all imgs to show in the map
   * @param threshold_lat threshold to calculate a significant latitude
   * @param threshold_lng threshold to calculate a significant longitude
   */
  getSignificantCoords(photos: Photo[], threshold_lat, threshold_lng) {
    const result: Photo[] = [];
    let i = 0;
    photos.forEach(photo => {
      const photo_lat = Math.abs(Number(photo.location.latitude));
      const photo_lng = Math.abs(Number(photo.location.longitude));

      if (i === 0) { // first photo always is in array
        result.push(photo);
      } else {
        let points = 0;
        result.forEach(elem => {
          const elem_lat = Math.abs(Number(elem.location.latitude));
          const elem_lng = Math.abs(Number(elem.location.longitude));
          if (Math.abs(photo_lat - elem_lat) >= threshold_lat && Math.abs(photo_lng - elem_lng) >= threshold_lng) {
            points++;
          }
        });
        if (points === result.length) {
          result.push(photo);
        }
      }

      i++;
    });
    return result;
  }

  initGoogleMaps(photos: Photo[]) {
    let myPosition: any = null;
    const myThis = this;
    const significant_coordinates = this.getSignificantCoords(photos, 0.05, 0.05);
    const centerPoint = this.getCenterOfPhotos(significant_coordinates);
    myPosition = new google.maps.LatLng(centerPoint[0], centerPoint[1]);
    const mapProp = {
      center: myPosition,
      zoom: 2,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true,
      styles: [{"stylers": [{ "saturation": -100 }]}],
    };
    this.map = new google.maps.Map(document.getElementById('map_canvas'), mapProp);

    let changeds = 0;
    let count = 0;
    this.photos.forEach(elementPhoto => {
      const latLng = new google.maps.LatLng(Number(elementPhoto.location.latitude), Number(elementPhoto.location.longitude));
      const icon = {
        // url: elementPhoto.thumbnail, // url
        scaledSize: new google.maps.Size(90, 90), // scaled size
        origin: new google.maps.Point(0, 0), // origin
        anchor: new google.maps.Point(0, 0), // anchor,

        url: window.devicePixelRatio > 1 ?  elementPhoto.thumbnail :  elementPhoto.thumbnail,
        // path: google.maps.SymbolPath.CIRCLE,
        scale: 10, //tamaño
        strokeColor: '#f00', //color del borde
        strokeWeight: 5, //grosor del borde
        fillColor: '#00f', //color de relleno
        fillOpacity:1// opacidad del relleno
      };
      for (let i = count; i < photos.length; i++) {
        const existingMarker = this.photos[i];
        const pos = new google.maps.LatLng(Number(existingMarker.location.latitude), Number(existingMarker.location.longitude));
        if (latLng.lat() === pos.lat() && latLng.lng() === pos.lng() && existingMarker.id !== elementPhoto.id) {
          changeds++;
          const a = 360.0 / this.photos.length;
          const newLat = pos.lat() + -.00004 * Math.cos((+a * i) / 180 * Math.PI);  // x
          const newLng = pos.lng() + -.00004 * Math.sin((+a * i) / 180 * Math.PI);  // Y
          const latLng = new google.maps.LatLng(newLat, newLng);
          i = photos.length;
        }
      }
      count++;
      const marker = new google.maps.Marker({
        position: latLng,
        title: 'Photo',
        icon: icon,
      });
    

      // var contentString = '<div id="content">'+
      // '<img (click)=" '+'" style="width:100%; height:100%; object-fit:cover;" src="'+ elementPhoto.thumbnail + '"></img>' + 
      // '</div>';

      // var infowindow = new google.maps.InfoWindow({
      //   content: contentString
      // });

      this.markers.push(marker);
      google.maps.event.addListener(marker, 'click', function () {

        // infowindow.open(this.map, marker);

        myThis._store.eventChangeTimelineDetailPhotoActive(null);
        myThis.goToDetailPhoto(elementPhoto.id);
        myThis.goToResultPhotos();
      });
    });



    // const clusterStyles = [
    //   {
    //     textColor: '#FF4365',
    //     url: 'assets/markers-map/m4.png',
    //     background: 'white !important',
    //     height: 35,
    //     width: 35
    //   },
    //  {
    //     textColor: '#FF4365',
    //     url: 'assets/markers-map/m4.png',
    //     height: 35,
    //     width: 35
    //   },
    //  {
    //     textColor: '#FF4365',
    //     url: 'assets/markers-map/m4.png',
    //     height: 35,
    //     width: 35
    //   }
    // ];

    const markerCluster = new MarkerClusterer(this.map, this.markers, {
      zoomOnClick: true,
      maxZoom: 10,
      gridSize: 20,
      imagePath: 'assets/markers-map/m',
      // styles: clusterStyles
      // imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
    });

  }

  /**
   * Gets all data of filter and results.
   */
  initNeededData() {
    this.search();
  }

  initPhotos(data) {
    this.photos = [];
    data.forEach(e => {
      this.photos.push(this.initPhoto(e));
    });
    this.initGoogleMaps(this.photos);
  }

  initPhoto(data: any) {
    const photo = new Photo();
    photo.id = data.identifier;
    photo.title = data.title;
    photo.image = data.image;
    photo.thumbnail = data.image_thumbnail;
    photo.isPublic = data.public;
    photo.createdAt = data.created_at;
    // photo.hashTags = this.initHastags(data.image_tags);
    photo.location = this.initLocation(data);
    // photo.user = this.initUser(data.user_info);
    photo.filter = data.picture_filter;
    if (!photo.filter) { photo.filter = ''; }
    return photo;
  }

  initLocation(data: any) {
    const location: Location = new Location();
    location.address = data.address_en;
    location.latitude = data.meta_location.latitude;
    location.longitude = data.meta_location.longitude;
    return location;
  }

  goToResultPhotos() {
    this.modalCtrl.dismiss();
    // this.router.navigateByUrl('tabs/(post:search-result)');
  }
  goToDetailPhoto(id_photo: string) {
    this.router.navigateByUrl('tabs/photo/' + id_photo);
  }


  search() {
    if(!this.filters){ return; }
    // this.goToSearchPhotosResultsPage();
    const dataToSend: any = {};

    if (this.filters.range) {
      dataToSend['distance'] = this.filters.range;
    }
    if (this.filters.user) {
      dataToSend['author'] = this.filters.user.id;
    }
    if (this.filters.startDate && this.filters.endDate) {
      dataToSend['startdate'] = this.filters.startDate.replace('Z', '');
      dataToSend['enddate'] = this.filters.endDate.replace('Z', '');
    }
    if (this.filters.address) {
      dataToSend['lat'] = this.filters.geometry.lat;
      dataToSend['lon'] = this.filters.geometry.lng;
    }
    if (this.filters.hashtags.length > 0) {
      let hashtagsIds = '';
      this.filters.hashtags.forEach(el => { hashtagsIds += el.id + ','; });
      dataToSend['hashtags'] = hashtagsIds;
      dataToSend['hashtags'] = dataToSend['hashtags'].replace(/,(\s+)?$/, '');
    }
    dataToSend['map'] = '';
    dataToSend['limit'] = '1000';

    this._postService.search(dataToSend).subscribe((results: any) => {
      if (results.count > 0) {
        this.initPhotos(results.results);
      }
    }, error => { console.error(error); });
  }

}
