import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TranslateService, TranslateModule } from '@ngx-translate/core';

import { SearchResultMapComponent } from './search-result-map.component';

const routes: Routes = [
  {
    path: '',
    component: SearchResultMapComponent
  }
];

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [SearchResultMapComponent]
})
export class SearchResultMapComponentModule {}
