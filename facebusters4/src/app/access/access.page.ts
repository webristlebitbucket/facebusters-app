import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { StoreService } from './../../providers/store/store';


@Component({
  selector: 'app-access',
  templateUrl: './access.page.html',
  styleUrls: ['./access.page.scss'],
})
export class AccessPage implements OnInit {

  public isMobile: boolean;

  constructor(private router: Router, public _store: StoreService) {
    this.isMobile = this._store.get('isMobile');
  }

  ngOnInit() {
  }

  goToLogin() {
    this.router.navigateByUrl('login');
  }

  goToRegister() {
    this.router.navigateByUrl('register');
  }
}
