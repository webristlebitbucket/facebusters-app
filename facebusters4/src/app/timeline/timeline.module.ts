import { DetailAlbumPage } from './../detail-album/detail-album.page';
import { DetailPhotoPage } from './../detail-photo/detail-photo.page';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { TimelinePage } from './timeline.page';

import { TranslateService, TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  {
    path: '',
    component: TimelinePage
  },
  // {
  //   path: 'photo/:id',
  //   component: DetailPhotoPage
  // },
  // {
  //   path: 'album:/id',
  //   component: DetailAlbumPage
  // },
];

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    TranslateModule
  ],
  declarations: [TimelinePage]
})
export class TimelinePageModule {}
