import { StoreService } from './../../providers/store/store';
import { Subscription } from 'rxjs';
import { NotificationsPage } from './../notifications/notifications.page';
import { Router, ActivatedRoute, Params, NavigationEnd } from '@angular/router';
import { ViewChild, ViewChildren, QueryList, Component, ElementRef, OnInit } from '@angular/core';
import { Photo, Album, User, Tag, Comment, HashTag, Location } from "../../models/models";
import { PostService } from "../../providers/post-service/post-service";
import { Events } from '@ionic/angular';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.page.html',
  styleUrls: ['./timeline.page.scss'],
})
export class TimelinePage implements OnInit{
  public items: Array<any> = new Array();
  public listItems: any[] = [];
  public listItemsActive: any[] = [];
  public photos: Photo[] = [];
  public albums: Album[] = [];
  public hasMoreResultsNext: string = '';
  public hasMoreResultsPrevious: string = '';
  public lockInfiniteScroll: boolean = false;
  public isMobile: boolean;
  private lockApi: boolean = false;
  private MAX_ITEMS_ACTIVES: number = 20;
  // @ViewChild("Conent") content: Content;
  @ViewChild('listItemsDiv') listItemsDiv: ElementRef;
  public refreshing: boolean = false;
  public myUser: User;
  private subscriptionMyUser: Subscription;


  constructor(
    private router: Router,
    private _postService: PostService,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private _store: StoreService,
    private events: Events
  ) {
    this.subscriptionMyUser = this._store.$myUser.subscribe((user: User) => {
      this.myUser = user;
    }, error => { console.error(error); });

    this.events.subscribe('refresh-timeline',() => {
      this.doRefresh(null);
    });

  }

  ngOnInit() {
    this.doRefresh(null);
  }

  initUser(data: any) {
    let user: User = new User();
    user = new User();
    if (data) {
      user.avatar = data.avatar_thumbnail;
      user.nickname = data.username;
      user.id = data.identifier;
    }
    return user;
  }

  initLocation(data: any) {
    let location: Location = new Location();
    location.address = data.address_en;
    location.latitude = data.meta_location.latitude;
    location.longitude = data.meta_location.longitude;
    return location;
  }

  initHastags(data: any[]) {
    let hashTags: HashTag[] = [];
    data.forEach(t => {
      const hashtag = new HashTag();
      hashtag.hashTag = t;
      hashTags.push(hashtag);
    });
    return hashTags;
  }

  initPhoto(data: any) {
    const photo = new Photo();
    photo.id = data.identifier;
    photo.title = data.title;
    photo.image = data.image;
    photo.thumbnail = data.image_thumbnail;
    photo.isPublic = data.public;
    photo.createdAt = data.created_at;
    photo.hashTags = this.initHastags(data.image_tags);
    photo.location = this.initLocation(data);
    photo.user = this.initUser(data.user_info);
    photo.filter = data.picture_filter;
    photo.metaDate = data.meta_date;
    if (!photo.filter) { photo.filter = ''; }
    return photo;
  }

  initPhotosToAlbum(data: any[]) {
    let photos: Photo[] = [];
    data.forEach(p => { photos.push(this.initPhoto(p)) });
    return photos;
  }

  initAlbum(data: any) {
    const album = new Album();
    album.id = data.identifier;
    album.pk = data.id;
    album.title = data.title;
    album.dateRange = data.date_range;
    album.isPublic = data.public;
    album.createdAt = data.created_at;
    album.photos = this.initPhotosToAlbum(data.latest_pictures);
    album.location = this.initLocation(data);
    album.user = this.initUser(data.user_info);
    return album;
  }

  getTimeLIne(eventRefresh) {
    this.hasMoreResultsNext = '';
    this.listItems = [];

    this._postService.getTimeLime(this.hasMoreResultsPrevious).subscribe((data: any) => {
      this.hasMoreResultsNext = data.next;
      this.hasMoreResultsPrevious = data.previous;
      if (data.count > 0) {
        data.results.forEach(el => {
          switch (el.item_type) {
            case 'album':
              if (el.item_info.hide) {
                break;
              }
              let tmpAlbum: Album = this.initAlbum(el.item_info);
              this.listItems.push(tmpAlbum);
              break;
            case 'picture':
              if (el.item_info.hide) {
                break;
              }
              let tmpPhoto: Photo = this.initPhoto(el.item_info);
              this.listItems.push(tmpPhoto);
              break;
          }
        });

        try { eventRefresh.target.complete(); } catch (error) {}
        this.refreshing = false;
      }
    }, error => { 
      console.error(error);  
      try { eventRefresh.target.complete(); } catch (error) {}
      this.refreshing = false;
    });
  }


  isLoading: boolean = false;
  doInfinite(infiniteScroll) { // next
    if (this.lockInfiniteScroll) { 
      infiniteScroll.target.complete();
      this.lockInfiniteScroll = false;
      return; 
    }
    this.lockInfiniteScroll = true;
    if (!this.hasMoreResultsNext) {
      infiniteScroll.target.complete();
      this.lockInfiniteScroll = false;
      return;
    }
    this._postService.getTimeLime(this.hasMoreResultsNext).subscribe((data: any) => {
      this.hasMoreResultsNext = data.next;
      this.hasMoreResultsPrevious = data.previous;
      let actualItemId: string = this.listItems[this.listItems.length-1].id;
      let newItems = [];
      let isMax: boolean = false;
      if(this.listItems.length > this.MAX_ITEMS_ACTIVES){
        isMax = true;
      }
      if (data.count > 0) {
        data.results.forEach(el => {
          let newElement: any = null;
          switch (el.item_type) {
            case 'album':
              newElement = this.initAlbum(el.item_info);
              break;
            case 'picture':
              newElement = this.initPhoto(el.item_info);
              break;
          }
          if (newElement){ 
            if(!isMax){
              this.listItems.push(newElement);
            }else{
              newItems.push(newElement); 
            }
          }
        });
        if(isMax){
          this.isLoading = true;
          let newList = this.listItems.slice(newItems.length, this.listItems.length);
          this.listItems = newList;
          // this.content.scrollToBottom();

          setTimeout(() => {
            newItems.forEach(el => {
              newList.push(el);
            });
          }, 1000);
        }
        infiniteScroll.target.complete();
        this.lockInfiniteScroll = false;
      }

      

    }, error => { console.error(error); this.lockInfiniteScroll = false; infiniteScroll.target.complete(); });
  }

  doRefresh(event) { // previous
    if(this.refreshing){ try { event.target.complete(); } catch (error) {} return; }
    this.refreshing = true;
    this.hasMoreResultsPrevious = '';
    this.listItems = [];
    this.getTimeLIne (event); // reset all data
    setTimeout(() => {
      try { event.target.complete(); } catch (error) {}
      this.refreshing = false;
    }, 3000);
  } 


  refreshData(){
    if(this.refreshing){
      return;
    }
    this.refreshing = true;
    this.hasMoreResultsPrevious = '';
    this.listItems = [];
    this.getTimeLIne (event); 
  }

}
