import { UtilsService } from './../services/utils';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared/shared.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule, HttpClient } from '@angular/common/http';

// Services
import { StoreService } from '../providers/store/store';
import { LoginService } from './../providers/login/login.service';
import { DataService } from './../services/data-observable';
import { AuthGuardService } from '../services/auth-guard.service';
import { AuthService } from '../services/auth.service';

// API Services
import { PostService } from '../providers/post-service/post-service';
import { UserService } from '../providers/user-service/user-service';
import { FollowService } from './../providers/follow/follow.service';
import { SearchAddressService } from './../providers/search/search';
import { SearchHashtagService } from './../providers/hashtag/hashtag';

import { TabsPageModule } from './tabs/tabs.module';

import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Base64 } from '@ionic-native/base64/ngx';

// import { LoaderComponent } from './loader/loader.component';
// import { SearchResultMapComponent } from './search-result-map/search-result-map.component';
// import { SearchResultComponent } from './search-result/search-result.component';
// import { LoaderComponent } from './loader/loader.component';
// import { EditProfileComponent } from './edit-profile/edit-profile.component';

// MULTILANG
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

// Authentication
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';


import { File } from '@ionic-native/file/ngx';

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  MarkerCluster,
  Marker,
  Environment
} from '@ionic-native/google-maps';

import { Globalization } from '@ionic-native/globalization/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    SharedModule,
    BrowserModule,
    HttpClientModule,
    FormsModule,
    IonicModule.forRoot({
      navAnimation: myTransitionAnimation,
    }),
    AppRoutingModule,
    TabsPageModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    OneSignal,
    Globalization,
    GooglePlus,
    Facebook,
    LoginService,
    PostService,
    UserService,
    FollowService,
    SearchAddressService,
    StatusBar,
    SplashScreen,
    ScreenOrientation,
    Geolocation,
    Crop,
    Camera,
    Base64,
    File,
    SocialSharing,
    InAppBrowser,
    UtilsService,
    DataService,
    AuthService,
    AuthGuardService,
    StoreService,
    FingerprintAIO,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }



import { Animation, NavOptions } from '@ionic/core';
export function myTransitionAnimation(AnimationC: Animation, _: HTMLElement, opts: TransitionOptions): Promise<Animation> {

  const TRANSLATE_DIRECTION = 'translateX';
  const OFF_BOTTOM = '100%';
  const CENTER = '0px';
  const enteringEl = opts.enteringEl;
  const leavingEl = opts.leavingEl;
  const ionPageElement = getIonPageElement(enteringEl);
  const rootTransition = new AnimationC();

  rootTransition
    .addElement(ionPageElement)
    .beforeRemoveClass('ion-page-invisible');

  const backDirection = (opts.direction === 'back');

  // animate the component itself
  if (backDirection) {
    rootTransition
      .duration(opts.duration || 350)
      .easing('cubic-bezier(0.3,0,0.66,1)');

  } else {
    rootTransition
      .duration(opts.duration || 350)
      .easing('cubic-bezier(0.3,0,0.66,1)')
      .fromTo(TRANSLATE_DIRECTION, OFF_BOTTOM, CENTER, true)
      .fromTo('opacity', 1, 1, true);
  }

  // Animate toolbar if it's there
  const enteringToolbarEle = ionPageElement.querySelector('ion-toolbar');
  if (enteringToolbarEle) {
    const enteringToolBar = new AnimationC();
    enteringToolBar.addElement(enteringToolbarEle);
    rootTransition.add(enteringToolBar);
  }

  // setup leaving view
  if (leavingEl && backDirection) {
    rootTransition
      .duration(opts.duration || 350)
      .easing('cubic-bezier(0.3,0,0.66,1)');

    const leavingPage = new AnimationC();
    leavingPage
      .addElement(getIonPageElement(leavingEl))
      .fromTo(TRANSLATE_DIRECTION, CENTER, OFF_BOTTOM)
      .fromTo('opacity', 1, 1);

    rootTransition.add(leavingPage);
  }

  return Promise.resolve(rootTransition);
}

export interface TransitionOptions extends NavOptions {
  // animationCtrl: HTMLIonAnimationControllerElement;
  progressCallback?: ((ani: Animation | undefined) => void);
  window: Window;
  baseEl: any;
  enteringEl: HTMLElement;
  leavingEl: HTMLElement | undefined;
}

function getIonPageElement(element: HTMLElement) {
  if (element.classList.contains('ion-page')) {
    return element;
  }
  const ionPage = element.querySelector(':scope > .ion-page, :scope > ion-nav, :scope > ion-tabs');
  if (ionPage) {
    return ionPage;
  }
  return element;
}