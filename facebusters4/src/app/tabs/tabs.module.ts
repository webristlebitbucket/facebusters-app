import { EditPostPageModule } from './../edit-post/edit-post.module';
import { FullAlbumPageModule } from './../full-album/full-album.module';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';

import { TabsPage } from './tabs.page';
import { TimelinePageModule } from './../timeline/timeline.module';
import { SearchPageModule } from './../search/search.module';
import { SearchResultComponentModule } from './../search-result/search-result.module';
import { SearchResultMapComponentModule } from './../search-result-map/search-result-map.module';

import { NewPostPageModule } from './../new-post/new-post.module';
import { ConnectionsPageModule } from './../connections/connections.module';
import { MyProfilePageModule } from './../my-profile/my-profile.module';
import { EditProfileComponentModule } from './../edit-profile/edit-profile.module';
import { NotificationsPageModule } from './../notifications/notifications.module';

import { DetailPhotoPageModule } from './../detail-photo/detail-photo.module';
import { DetailAlbumPageModule } from './../detail-album/detail-album.module';
import { PublicProfilePageModule } from './../public-profile/public-profile.module';

import { TranslateService, TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    TimelinePageModule,
    SearchPageModule,
    SearchResultComponentModule,
    SearchResultMapComponentModule,
    NewPostPageModule,
    EditPostPageModule,
    ConnectionsPageModule,
    MyProfilePageModule,
    DetailPhotoPageModule,
    DetailAlbumPageModule,
    PublicProfilePageModule,
    FullAlbumPageModule,
    EditProfileComponentModule,
    NotificationsPageModule,
    TranslateModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {

}
