import { PostService } from './../../providers/post-service/post-service';
import { NewPostPage } from './../new-post/new-post.page';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, ViewChild, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { StoreService } from './../../providers/store/store';
import { User } from './../../models/models';
import { NotificationsPage } from './../notifications/notifications.page';
import { UserService } from './../../providers/user-service/user-service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage implements OnInit {
  activePage: any = {
    'page': 'timeline',
    'left': '20%',
    'visible': true
  }
  isMobile: boolean;
  myUser: User;
  notificationsInterval: any;

  constructor(
    private router: Router,
    public modalController: ModalController,
    private activatedRoute: ActivatedRoute,
    private _storeService: StoreService,
    private _userService: UserService,
    private _postService: PostService,
  ) {
    this.isMobile = this._storeService.get('isMobile');
  }

  tabIsChanged(event){
  }

  ngOnInit() {
    setTimeout(() => {
      this._userService.getMyUser().subscribe(data => {
        this._storeService.set('myUser', this._storeService.parserApiToModel('user', data));
        this.myUser = this._storeService.get('myUser'); 
      }, error => { console.error(error); });
    }, 2000);
  }

  async openModalNewpost() {
    const modal = await this.modalController.create({
      component: NewPostPage,
      componentProps: { value: 123 }
    });
    modal.onDidDismiss().then((data: any) => {
      // Esto pasa cuando se cierra el modal!!!
    })
    return await modal.present();
  }

  goTimeLine() {
    this.router.navigateByUrl('tabs/timeline');
  }
  goSearch() {
    this.router.navigateByUrl('tabs/search');
  }
  goConnections() {
    this.router.navigateByUrl('tabs/connections');
  }
  goMyProfile() {
    this.router.navigateByUrl('tabs/my-profile');
  }
  goNotifications() {
  }

  async presentModalNotifications() {
    const modal = await this.modalController.create({
      component: NotificationsPage,
      componentProps: { value: 123 }
    });
    return await modal.present();
  }
}
