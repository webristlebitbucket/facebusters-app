import { AuthGuardService } from './../../services/auth-guard.service';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'timeline',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: '../timeline/timeline.module#TimelinePageModule'
          }
        ]
      },
      {
        path: 'search',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: '../search/search.module#SearchPageModule'
          },
          {
            path: 'result',
            loadChildren: '../search-result/search-result.module#SearchResultComponentModule'
          }
        ]
      },
      {
        path: 'connections',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: '../connections/connections.module#ConnectionsPageModule'
          }
        ]
      },
      {
        path: 'my-profile',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: '../my-profile/my-profile.module#MyProfilePageModule'
          },
          {
            path: 'edit',
            loadChildren: '../edit-profile/edit-profile.module#EditProfileComponentModule'
          },
        ]
      },
      {
        path: 'photo/:id',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: '../detail-photo/detail-photo.module#DetailPhotoPageModule'
          }
        ]
      },
      {
        path: 'album/:id/:pk',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: '../detail-album/detail-album.module#DetailAlbumPageModule'
          }
        ]
      },
      {
        path: 'profile/:id',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: '../public-profile/public-profile.module#PublicProfilePageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/timeline',
        pathMatch: 'full',
        canActivate: [AuthGuardService]
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/timeline',
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
