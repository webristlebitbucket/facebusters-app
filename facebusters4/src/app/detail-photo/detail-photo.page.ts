import { NewPostPage } from './../new-post/new-post.page';
import { UtilsService } from './../../services/utils';
import { ModalCommentsComponent } from './../shared/modal-comments/modal-comments.component';
import { ModalTagUserComponent } from './../shared/modal-tag-user/modal-tag-user.component';
import { PostService } from './../../providers/post-service/post-service';
import { StoreService } from './../../providers/store/store';
import { Photo, Tag, User } from './../../models/models';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, Input } from '@angular/core';
import { AlertController, ModalController, ActionSheetController, LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { EditPostPage } from '../edit-post/edit-post.page';
import { Location } from '@angular/common';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
@Component({
  selector: 'app-detail-photo',
  templateUrl: './detail-photo.page.html',
  styleUrls: ['./detail-photo.page.scss'],
})
export class DetailPhotoPage {
  public photo: Photo = null;
  private modalFullScreen: any;
  private modalTagPhoto: any;
  public showTags: boolean = false;
  public isMobile: boolean;
  public loading: any;
  public isLoading: boolean = true;
  public notFound: boolean = false;

  public fromPublicProfile: boolean = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private actionSheetCtrl: ActionSheetController,
    private _store: StoreService,
    private _postService: PostService,
    private _utils: UtilsService,
    private translate : TranslateService,
    private location: Location,
    private loadingController: LoadingController,
    private socialSharing: SocialSharing
  ) {
    this.isMobile = this._store.get('isMobile');
    this.isLoading = true;
    this.notFound = false;
    this.photo = null;
  }


  ionViewWillEnter(){
    this.isLoading = true;
    this.showTags = false;
    this.notFound = false;
    this.photo = null;
    if(!window.location.href.includes('/tabs')){
      this.fromPublicProfile = true;
    }else{
      this.fromPublicProfile = false;
    }
    this.getPhotobyId();
  }

  photoSubscribe: any = null;
  getPhotoByObject(){
    this.photoSubscribe = this._store.$timelineDetailPhotoActive.subscribe((photo: Photo) => {
      
      if(photo===undefined || photo==null){
        this.getPhotobyId();
      }else{
        this.photo = photo;
      }
      this.isLoading = false;
    }, error => { 
      console.error(error);
      this.isLoading = false;
      this.presentAlertError();
    });
  }
  getPhotobyId(){
    this.photo = new Photo();
    this.photo.id = this.activatedRoute.snapshot.paramMap.get('id')
    this.getPhoto(this.photo.id);
  }

  ionViewWillLeave(){
    this.isLoading = true;
    this.photo = null;
    this.showTags = false;
  }

  ionViewDidLeave() {
    this.isLoading = true;
    this.photo = null;
    this.showTags = false;
  }

  async presentLoading() {
    this.isLoading = true;
    this.loading = await this.loadingController.create({
      message: ''
    });
    return await this.loading.present();
  }

  dismissLoading() {
    this.isLoading = false;
    try {
      return this.loading.dismiss();
    } catch (error) {
      console.error('no loading component', error);
    }
  }


  getPhoto(identifier: any) {
    this.isLoading = true;
    this._postService.getPicture(identifier).subscribe((data: any) => {
      this.photo = this._utils.initPhoto(data);
      this.isLoading = false;
      this.notFound = false;
    }, error => { 
      console.error(error);
      this.notFound = true;
      this.isLoading = false;
      this.dismissLoading(); 
      this.presentAlertError();
    });
  }

  async showModalTagPhoto(photo: Photo) {
    const modal = await this.modalCtrl.create({
      component: ModalTagUserComponent,
      componentProps: { 'photo': photo, 'album_index' : 0 }
    });
    this.showTags = false;
    modal.onDidDismiss().then((data: any) => {
      this.showUserTags(this.photo, true);
    });
    return await modal.present();
  }

  async showModalComments(photo: Photo) {
    const modal = await this.modalCtrl.create({
      component: ModalCommentsComponent,
      componentProps: { 'photo': photo }
    });
    return await modal.present();
  }

  async presentActionSheet() {
    let actionSheet; 
    if(this._store.get('myUser').id === this.photo.user.id){
      // Allow edit
      actionSheet = await this.actionSheetCtrl.create({
        buttons: [
          {
            text: this.translate.instant('MY_PHOTO_DETAILS_3DOTS_EDIT'),
            handler: () => {
              this.openModalEditpost();
            }
          },
          {
            text: this.translate.instant('MY_PHOTO_DETAIL_3DOTS_DELETE'),
            role: 'destructive',
            handler: () => {
              this.deletePhoto();
            }
          },
        ]
      });
    }else{
      actionSheet = await this.actionSheetCtrl.create({
        // Allow report if is another user
        buttons: [
          {
            text: this.translate.instant('PHOTO_DETAIL_3DOTS'),
              handler: () => {
              this.presentPromptReport();
            }
          },
        ]
      });
    }
    await actionSheet.present();
  }

  async presentPromptReport() {
    const alert = await this.alertCtrl.create({
      header: this.translate.instant('PHOTO_DETAIL_3DOTS_MESSAGE'),
      inputs: [
        {
          name: 'reason',
          placeholder: this.translate.instant('PHOTO_DETAIL_3DOTS_MESSAGE_PLACEHOLDER')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('PHOTO_DETAIL_3DOTS_BUTTON_CANCEL'),
          role: 'cancel',
          handler: data => {
            console.info('Cancel clicked');
          }
        },
        {
          text: this.translate.instant('PHOTO_DETAIL_3DOTS_BUTTON_REPORT'),
          handler: data => {
            let dataTosend: any = {
              'picture': this.photo.id,
              'comment': data.reason
            }
            this.reportPhoto(dataTosend);
          }
        }
      ]
    });
    await alert.present();
  }

  showUserTags(photo: Photo, show:boolean=null) {
    if(show){
      this.showTags = show;
    }else{
      this.showTags = !this.showTags;
    }
    if (this.showTags) {
      this.loading = true;
      this._postService.getUserTags(this.photo.id).subscribe((data: any) => {
        if (data.count === 0) {
          let myThis = this;
        } else {
          this.photo.tags = [];
          data.results.forEach(element => {
            let tagUser: Tag = new Tag();
            tagUser.id = element.identifier;
            tagUser.coords.row = element.top;
            tagUser.coords.col = element.left;
            tagUser.hide = element.hide;
            if(element.user){
              tagUser.user = new User();
              tagUser.user.id = element.user_info.identifier;
              tagUser.user.avatar = element.user_info.avatar_thumbnail;
              tagUser.user.nickname = element.user_info.username;
            }
            else{
              tagUser.nonUser = element.non_user;
            }
            this.photo.tags.push(tagUser);
          });
        }
        this.loading = false;
      }, error => { console.error(error); this.loading = false;});
    }
  }

  reportPhoto(dataToSend: any) {
    this._postService.reportPhoto(dataToSend).subscribe((data: any) => {
      this.presentAlertPhotoReported('ok');
    }, error => {
      console.error(error);
      this.presentAlertPhotoReported('nook');
    });
  }

  async presentAlertPhotoReported(status: string) {
    let title: string = '';
    if (status === 'ok') { title = this.translate.instant('PHOTO_DETAIL_3DOTS_MESSAGE_REPORT_SENT'); }
    else { title = this.translate.instant('PHOTO_DETAIL_3DOTS_MESSAGE_ALREADY_REPORT'); }
    const alert = await this.alertCtrl.create({
      header: title,
      buttons: [this.translate.instant('PHOTO_DETAIL_3DOTS_BUTTON_REPORTED')]
    });
    await alert.present();
  }

  async presentAlertPhotoDeleted() {
    const title = this.translate.instant('MY_PHOTO_DETAIL_3DOTS_DELETE_MESSAGE');
    const alert = await this.alertCtrl.create({
      header: title,
      buttons: [
          {
            text: this.translate.instant('MY_PHOTO_DETAIL_3DOTS_DELETE_BUTTON'),
            handler: (data) => {
              this.location.back();
            }
          }
      ]
    });
    await alert.present();
  }

  sharePhoto(){
    this.socialSharing.share('https://api.facebusters.com/picture_social/'+this.photo.id.toString()).then(() => {
    }).catch(() => {});
  }

  scrollToWriteComment(){
    var objDiv = document.getElementById("web-detail-photo");
    objDiv.scrollTop = objDiv.scrollHeight;
  }

  goPublicProfile(user) {
    // if(this.fromPublicProfile){ return; }
    if (this._store.get('myUser').id === user.id) {
      this.router.navigateByUrl('tabs/my-profile');
    } else {
      this.router.navigateByUrl('tabs/profile/' + user.id);
    }
  }

  async openModalEditpost() {
    const modal = await this.modalCtrl.create({
      component: NewPostPage,
      componentProps: { photoEdit: this.photo }
    });

    modal.onDidDismiss().then((data) => {
      if(data){
        if(data.data){
          this.isLoading = true;
          this.photo = data.data;
          this.photo.metaDate = data.data._datetime;
          this.isLoading = false;
        }
      }
    });
    return await modal.present();
  }

  showTagsPhotoPanel(){
    var tags_panel = document.getElementById('tag-panel-0');
    tags_panel.classList.remove("d-none");
  }

  deletePhoto(){
    this._postService.editPicture(this.photo.id, {'hide': true }).subscribe((data: any) => {
      this.presentAlertPhotoDeleted();
    }, error => { console.error(error); });
  }

  async presentAlertError(titleM: boolean = false, locationM: boolean = false, dateM: boolean = false) {
    const alert = await this.alertCtrl.create({
      message: this.translate.instant('NOTIFICATIONS_PICTURE_NOT_FOUND'),
      buttons: [
        {
          text: this.translate.instant('GENERIC_POPUP_BUTTON_OK'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.location.back();
          }
        }
      ]
    });
    await alert.present();
  }

  ngOnDestroy(){
    try {
      this.isLoading = true;
      this.notFound = false;
      this.photo = null;
      if(this.photoSubscribe){
        this.photoSubscribe.unsubscribe();
      }
    } catch (error) {
      console.error(error);
    }
  }
}
