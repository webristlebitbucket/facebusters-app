import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPhotoPage } from './detail-photo.page';

describe('DetailPhotoPage', () => {
  let component: DetailPhotoPage;
  let fixture: ComponentFixture<DetailPhotoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailPhotoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPhotoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
