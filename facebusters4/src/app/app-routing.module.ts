import { TabsPage } from './tabs/tabs.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { AuthGuardService } from '../services/auth-guard.service';

const routes: Routes = [
  { path: 'access', loadChildren: './access/access.module#AccessPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'reset-password', loadChildren: './reset-password/reset-password.module#ResetPasswordPageModule' },
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'timeline',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: './timeline/timeline.module#TimelinePageModule'
          }
        ]
      },
      {
        path: 'search',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: './search/search.module#SearchPageModule'
          },
          {
            path: 'result',
            loadChildren: './search-result/search-result.module#SearchResultComponentModule'
          }
        ]
      },
      {
        path: 'connections',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: './connections/connections.module#ConnectionsPageModule'
          }
        ]
      },
      {
        path: 'my-profile',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: './my-profile/my-profile.module#MyProfilePageModule'
          }
        ]
      },
      {
        path: 'photo/:id',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: './detail-photo/detail-photo.module#DetailPhotoPageModule'
          }
        ]
      },
      {
        path: 'album/:id/:pk',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: './detail-album/detail-album.module#DetailAlbumPageModule'
          }
        ]
      },
      {
        path: 'profile/:id',
        canActivate: [AuthGuardService],
        children: [
          {
            path: '',
            loadChildren: './public-profile/public-profile.module#PublicProfilePageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/timeline',
        pathMatch: 'full',
        canActivate: [AuthGuardService]
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/timeline',
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
