import { PostService } from './../../providers/post-service/post-service';
import { UserService } from './../../providers/user-service/user-service';
import { UtilsService } from './../../services/utils';
import { ModalTagUserComponent } from './../shared/modal-tag-user/modal-tag-user.component';
import { Platform, NavParams, ActionSheetController, ModalController } from '@ionic/angular';
import { Component, OnInit, OnDestroy, ElementRef, Input } from '@angular/core';
import { SearchAddressService } from './../../providers/search/search';
import { Post, Photo, Album, Location, HashTag, Tag, TagCoords } from './../../models/models';
import * as EXIF from 'exif-js';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.page.html',
  styleUrls: ['./edit-post.page.scss'],
})
export class EditPostPage implements OnInit {

  public pictureToEdit: Photo;
  public albumToEdit: Album;

  public isLoading: boolean = false;

  public step: number = 1;
  public post: Post = new Post();
  public base64ImageList: any[] = [];
  public metaList: any[] = [];
  public filters: any = {
    date: new Date().toISOString(),
    time: new Date().toISOString(),
    address: null,
    isPrivate: false,
    location: '',
    description: '',
    hashTagInput: ''
  }
  public metaData: any = null;
  public myLocation: any = { 'latitude': null, 'longitude': null };

  // 2 slide
  public filtersList: any[] = [];
  public photoSelected: Photo;
  public filterSelected: string = 'no-filter';

  // 3 slide
  myUserId: any;
  photoActualPosition: number = 0;
  dataToSend: any = {}; // Data To Send Backend
  postError: any = null;
  modalTagPhoto: any = null;
  postType: string = 'picture' || 'album';

  // loaders
  loading: any;

  constructor(
    public _platform: Platform,
    private _navParams: NavParams,
    private _actionSheetCtrl: ActionSheetController,
    private modalCtrl: ModalController,
    private geolocation: Geolocation,
    private _searchAddressService: SearchAddressService,
    private _utilsService: UtilsService,
    private _userService: UserService,
    private _postService: PostService,
    private alertController: AlertController,
    private translate: TranslateService,
    public loadingController: LoadingController
  ) {
    this.post = this.initPost(this.post);
    this.getCurrentLocation();
    this.filtersList = this._utilsService.getFilters();
    this.getMyUser();
  }

  ngOnInit() {

    if (this.pictureToEdit) {
      this.post.location = this.pictureToEdit.location;
      this.post.photos.push(this.pictureToEdit);
      this.post.postType = 'photo';
      this.post.title = this.pictureToEdit.title;
      this.post.isPublic = this.pictureToEdit.isPublic;
    }
    if (this.albumToEdit) {
      this.post.location = this.albumToEdit.location;
      this.post.photos = this.albumToEdit.photos;
      this.post.postType = 'album';
      this.post.title = this.albumToEdit.title;
      this.post.isPublic = this.albumToEdit.isPublic;
    }
  }

  initPost(post: Post = new Post()) {
    post.location = new Location();
    post.photos = [];
    post.postType = 'photo';
    post.title = '';
    post.isPublic = true;
    return post;
  }


  addBase64ImageToList(urlBase64Image: any) {
    this.base64ImageList.push(urlBase64Image);
    this.addNewPhotoToPost(urlBase64Image);
  }
  addNewPhotoToPost(urlBase64Image: any) {
    let photo = new Photo();
    photo.id = this.post.photos.length;
    photo.image = urlBase64Image;
    photo.thumbnail = urlBase64Image;
    photo.filter = 'no-filter';
    this.convertToBase64(photo.image, 'image/png').then(data => {
      photo._imageB64 = data.toString();
      this.post.photos.push(photo);
    });
    this.photoSelected = this.post.photos[0];
  }

  convertToBase64(url, outputFormat) {
    return new Promise((resolve, reject) => {
      let img = new Image();
      // img.crossOrigin = 'Anonymous';
      img.onload = function () {
        let canvas = <HTMLCanvasElement>document.createElement('CANVAS'), ctx = canvas.getContext('2d'), dataURL;
        canvas.height = img.height;
        canvas.width = img.width;
        ctx.drawImage(img, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        canvas = null;
        resolve(dataURL);
      };
      img.src = url;
    });
  }
  goBack() {
    this.presentLoadingText(2000);
    if (this.step > 1) {
      this.step -= 1;
    }
  }
  goToNext() {
    this.presentLoadingText(2000);

    let myThis = this;
    setTimeout(function () {
      if (myThis.post.photos.length > 0) {
        myThis.setPhotoSelected(myThis.post.photos[0]);
        if (myThis.step < 3) {
          myThis.step += 1;
        }
      }
    }, 1000);


  }
  randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  getBase64(file) {
    let myThis = this;
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () { myThis.addNewPhotoToPost(reader.result); };
    reader.onerror = function (error) { console.error('Error: ', error); };
  }
  readURL(event: any): void {
    // THis is the magic
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      this.getExif(file);   // get meta and mapping
      this.getBase64(file); // Convert to b64
    }
  }

  getExif(file) {
    let myThis = this;
    EXIF.getData(file, function () {
      myThis.metaData = EXIF.getAllTags(file);
      console.info('EXIF', myThis.metaData)
      try {
        let coords: string = null;
        let metaCoords: any = {
          'latitude': {
            'degress': parseFloat(myThis.metaData.GPSLatitude[0].numerator + ',' + myThis.metaData.GPSLatitude[0].denominator),
            'minutes': parseFloat(myThis.metaData.GPSLatitude[1].numerator + ',' + myThis.metaData.GPSLatitude[1].denominator),
            'seconds': parseFloat(myThis.metaData.GPSLatitude[2].numerator + ',' + myThis.metaData.GPSLatitude[2].denominator),
            'direction': myThis.metaData.GPSLatitudeRef
          },
          'longitude': {
            'degress': parseFloat(myThis.metaData.GPSLongitude[0].numerator + ',' + myThis.metaData.GPSLongitude[0].denominator),
            'minutes': parseFloat(myThis.metaData.GPSLongitude[1].numerator + ',' + myThis.metaData.GPSLongitude[1].denominator),
            'seconds': parseFloat(myThis.metaData.GPSLongitude[2].numerator + ',' + myThis.metaData.GPSLongitude[2].denominator),
            'direction': myThis.metaData.GPSLongitudeRef
          },
        }
        const metaDataParsed: any = {
          'latitude': myThis.convertDMSToDD(metaCoords.latitude.degress, metaCoords.latitude.minutes, metaCoords.latitude.seconds, metaCoords.latitude.direction),
          'longitude': myThis.convertDMSToDD(metaCoords.longitude.degress, metaCoords.longitude.minutes, metaCoords.longitude.seconds, metaCoords.longitude.direction),
          'date': myThis.metaData.DateTime,
          'address': null
        }
        // console.info(metaDataParsed);
        myThis.metaList.push(metaDataParsed);
        myThis.getAddressFromCoordinates(metaDataParsed.latitude, metaDataParsed.longitude);
      } catch (error) { console.error('Not Data'); myThis.metaList.push(null); }
    });
  }
  convertDMSToDD(degrees, minutes, seconds, direction) {
    let dd = degrees + minutes / 60 + seconds / (60 * 60);
    if (direction == "S" || direction == "W") { dd = dd * -1; } // Don't do anything for N or E
    return dd;
  }
  getAddressFromCoordinates(latitude: Number, longitude: Number) {
    let myThis = this;
    let address: string = null;
    this._searchAddressService.searchAddressByCoords({ 'lat': latitude, 'lon': longitude }).subscribe((data: any) => {
      myThis.metaList[myThis.metaList.length - 1].address = data.address;
      this.post.photos[this.post.photos.length - 1].location.address = this.metaList[this.metaList.length - 1].address;
      this.post.photos[this.post.photos.length - 1].location.date = this.metaList[this.metaList.length - 1].date;
      this.post.photos[this.post.photos.length - 1].location.latitude = this.metaList[this.metaList.length - 1].latitude;
      this.post.photos[this.post.photos.length - 1].location.longitude = this.metaList[this.metaList.length - 1].longitude;

      console.log('POST ==> ', this.post)
      this.post.photos.forEach(photo => {
        if (photo.location.address) {
          this.filters.address = photo.location.address;
          console.log('Address to show ==> ', this.filters.address);
          return;
        }
      });
    }, error => { console.error(error); });
  }

  reset() {
    this.post = this.initPost();
    this.filters = {
      date: new Date().toISOString(),
      time: new Date().toISOString(),
      address: null,
      description: null,
      isPrivate: false
    }
  }

  openSearchAddressModal() {
    // let searchAddressModal = this.modalCtrl.create('SearchAddressPage');
    // searchAddressModal.onDidDismiss(data => {
    //   console.log('he recibido esto del modal ==> ', data);
    //   if (data) {
    //     let addressSelected = data;
    //     this.filters.address = data.formatted;
    //     this.post.location = new Location();
    //     this.post.location.address = data.formatedd;
    //     this.post.location.latitude = data.geometry.lat;
    //     this.post.location.latitude = data.geometry.lng;
    //   }
    // });
    // searchAddressModal.present();
  }

  getCurrentLocation() {
    let myThis = this;
    this.geolocation.getCurrentPosition().then((resp) => {
      try {
        this.myLocation.latitude = resp.coords.latitude;
        this.myLocation.longitude = resp.coords.longitude;
        this.post.location.latitude = this.myLocation.latitude;
        this.post.location.longitude = this.myLocation.longitude;
        console.log('my location ==> ', this.myLocation);

        this._searchAddressService.searchAddressByCoords({ 'lat': this.myLocation.latitude, 'lon': this.myLocation.longitude }).subscribe((data: any) => {
          console.log('Current Address found ==> ', data);
          this.filters.address = data.address;
          this.post.location.address = data.address;
        }, error => { console.error(error); });
      } catch (error) { console.error(error); }
    }).catch((error) => {
      console.error('Error getting location', error);
    });
  }


  closeModal() {
    this.modalCtrl.dismiss();
  }


  // Slide 2

  slideOpts = {
    effect: 'flip',
    slidesPerView: 4
  };
  slideOptsFilters = {
    effect: 'flip',
    slidesPerView: 3
  };




  setPhotoSelected(photo: Photo = new Photo()) {
    this.photoSelected = photo;
    this.filterSelected = photo.filter;
  }

  setFilterSelected(photo: Photo, filter: string) {
    photo.filter = filter;
    this.filterSelected = filter;
  }

  goToNewPostInfo() {
    // this._navCtrl.push('NewPostInfoPage', { post: this.post });
  }

  goToBack() {
  }



  // step 3
  initPostType() {
    if (this.post) {
      if (this.post.photos.length > 1) { this.postType = 'album'; }
      else { this.postType = 'picture'; }
    }
  }

  initFilters() {
    this.filters = {
      date: '2018-05-21',
      time: '08:30',
      location: '',
      description: '',
      isPrivate: false,
      hashTagInput: ''
    }
  }

  getMyUser() {
    this._userService.getMyUser().subscribe((data: any) => {
      this.myUserId = data.id;
    }, error => { console.error(error); });
  }

  nextPhoto() {
    if (this.photoActualPosition === this.post.photos.length - 1) {
      this.presentConfirm();
    } else {
      this.photoActualPosition += 1;
    }
  }

  previousPhoto() {
    if (this.photoActualPosition === 0) {
      this.photoActualPosition = this.post.photos.length - 1;
    } else {
      this.photoActualPosition -= 1;
    }
  }
  async presentConfirm() {
    const alert = await this.alertController.create({
      header: this.translate.instant('Confirm ') + this.postType + this.translate.instant(' upload'),
      message: this.translate.instant('Do you want to upload this ') + this.postType + '?',
      buttons: [
        {
          text: this.translate.instant('Cancel'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Upload',
          handler: () => {
            console.log('Confirm Okay');
            this.uploadPost();
          }
        }
      ]
    });
    await alert.present();
  }

  async presentCompleteUpload() {
    const alert = await this.alertController.create({
      header: this.translate.instant('Upload complete!'),
      message: this.translate.instant('Your ') + this.postType + this.translate.instant(' has uploaded successful'),
      buttons: [
        {
          text: 'OK',
          handler: (blah) => {
            this.dismissLoadingText();
            this.closeModal();
          }
        }
      ]
    });
    await alert.present();
  }

  copyPrevious() {
    if (this.photoActualPosition === 0) {
      this.post.photos[this.photoActualPosition].title = this.post.title;
      this.post.photos[this.photoActualPosition]._date = this.post._date;
      this.post.photos[this.photoActualPosition]._time = this.post._time;
      this.post.photos[this.photoActualPosition].location = this.post.location;
      return;
    }
    if (this.post.photos.length > 1) {
      this.post.photos[this.photoActualPosition].title = this.post.photos[this.photoActualPosition - 1].title;
      this.post.photos[this.photoActualPosition]._date = this.post.photos[this.photoActualPosition - 1]._date;
      this.post.photos[this.photoActualPosition]._time = this.post.photos[this.photoActualPosition - 1]._time;
      this.post.photos[this.photoActualPosition].tags = this.post.photos[this.photoActualPosition - 1].tags;
      this.post.photos[this.photoActualPosition].hashTags = this.post.photos[this.photoActualPosition - 1].hashTags;
      this.post.photos[this.photoActualPosition].location = this.post.photos[this.photoActualPosition - 1].location;
    }
    return;
  }

  uploadPost() {
    this.presentLoadingText();
    if (this.post.photos.length === 1) {
      // Picture
      if (!this.post.photos[0].title) { this.post.photos[0].title = ''; }


      this.dataToSend = {
        "title": this.post.photos[0].title,
        "image": this.post.photos[0]._imageB64,
        "picture_filter": this.post.photos[0].filter,
        "meta_location": {
          "latitude": this.post.location.latitude,
          "longitude": this.post.location.longitude
        },
        "hashtags": [],
        "meta_date": this.post.location.date, // 2018-07-10T21:50:28+02:00
        "user": this.myUserId
      };
      if(!this.dataToSend.image){
        delete this.dataToSend.image;
      }

      if (!this.dataToSend.meta_location.latitude || !this.dataToSend.meta_location.longitude) {
        this.dataToSend.meta_location.latitude = this.myLocation.latitude;  // latitud por defecto si no tiene
        this.dataToSend.meta_location.longitude = this.myLocation.longitude; // longitud por defecto si no tiene
        // delete this.dataToSend.meta_location;
      }
      if (!this.dataToSend.meta_date) {
        delete this.dataToSend.meta_date;
      }
      this.post.photos[0].hashTags.forEach(hashtag => {
        if(hashtag.id){
          this.dataToSend.hashtags.push(hashtag.id);
        }
      });

      console.log('Voy a enviar esto ==> ', this.dataToSend)
      this._postService.editPicture(this.pictureToEdit.id, this.dataToSend).subscribe(data => {
        // this.navCtrl.setRoot('TabsPage', { index: 0 });
        console.log('PHOTO SUBIDA YA TENGO ID ==> ', data);
        this.saveTags(this.post.photos[0], data['identifier']);
        this.dismissLoadingText();
      }, error => {
        console.error(error);
        this.postError = error;
      });
    }
    if (this.post.photos.length > 1) {
      // create an album
      this.dataToSend = {
        "title": this.post.title,
        "meta_location": {
          "latitude": this.post.location.latitude,
          "longitude": this.post.location.longitude
        }
      };
      this._postService.createNewAlbum(this.dataToSend).subscribe((data: any) => { // Album Post
        var idAlbum = data.id;
        var count = 0;


        this.post.photos.forEach(photo => { // Picture Post
          if (!photo.location.latitude || !photo.location.longitude) {
            photo.location.latitude = this.post.location.latitude;
            photo.location.longitude = this.post.location.longitude;
          }

          console.log('SE VA A SUBIR ESTO')
          console.log(photo)

          if (!photo.title) { photo.title = ''; }
          this.dataToSend = {
            "album": idAlbum,
            "title": photo.title,
            "image": photo._imageB64,
            "picture_filter": photo.filter,
            "meta_location": {
              "latitude": photo.location.latitude,
              "longitude": photo.location.longitude
            },
            "hashtags": [],
            "meta_date": photo.location.date, // 2018-07-10T21:50:28+02:00
            "user": this.myUserId
          };
          photo.hashTags.forEach(hashtag => { // BUG
            this.dataToSend.hashtags.push(hashtag.id)
          });


          console.log('dataToSend', this.dataToSend)
          this._postService.createNewPicture(this.dataToSend).subscribe((data: any) => {
            count++;

            console.log('PHOTO SUBIDA YA TENGO ID ==> ', data);
            this.saveTags(photo, data['identifier']);

            if (count == this.post.photos.length-1) {
              this.dismissLoadingText();
              this.presentCompleteUpload();
            }
          }, error => { console.error(error); this.postError = error; });
        });
      }, error => { console.error(error); this.postError = error; });
    } else {
      this.dismissLoadingText();
      this.presentCompleteUpload();
    }
  }

  async presentLoadingText(duration: number = null) {
    if (duration) {
      this.loading = await this.loadingController.create({
        message: this.translate.instant('Loading...'),
        duration: duration
      });
    } else {
      this.loading = await this.loadingController.create({
        message: this.translate.instant('Loading...')
      });
    }
    return await this.loading.present();
  }

  dismissLoadingText() {
    this.loading.dismiss();
  }

  addHashTag(photo: Photo) {
    const hashTag: HashTag = new HashTag();
    this._postService.searchHashtag(this.filters.hashTagInput).subscribe((data: any) => {
      if (data.count === 0) { // Not exists
        this._postService.createHashtag(this.filters.hashTagInput).subscribe((data: any) => {
          hashTag.id = data.id;
          hashTag.hashTag = data.name;
          hashTag.createdAt = data.created_at;
          photo.hashTags.push(hashTag);
          this.filters.hashTagInput = '';
        }, error => { console.error(error); });
      } else { // Exists
        hashTag.id = data.results[0].id;
        hashTag.hashTag = data.results[0].name;
        hashTag.createdAt = data.results[0].created_at;
        photo.hashTags.push(hashTag);
        this.filters.hashTagInput = '';
      }
    }, error => { console.error(error); });
  }

  removeHashTag(hashtag: HashTag) {
    this.post.photos[this.photoActualPosition].hashTags.forEach((el, index) => {
      if (el === hashtag) {
        this.post.photos[this.photoActualPosition].hashTags.splice(index, 1);
        return;
      }
    });
  }


  async showModalTagPhoto(photo: Photo) {
    const modal = await this.modalCtrl.create({
      component: ModalTagUserComponent,
      componentProps: { 'photo': photo }
    });

    modal.onDidDismiss().then((data: any) => {
      console.log(data)
      photo.tags = [];
      data.data.tags.forEach(el => {
        const tag: Tag = new Tag();
        tag.user = el.user;
        tag.coords = new TagCoords();
        tag.coords.row = el.coords.row;
        tag.coords.col = el.coords.col;
        photo.tags.push(tag);
      });
    })
    return await modal.present();
  }

  saveTags(photo, idPhoto) {

    photo.tags.forEach(tag => {
      const dataToSend: any = {
        'picture': idPhoto,
        'user': tag.user.id,
        'status': 'add',
        'top': tag.coords.row,
        'left': tag.coords.col
      }
      this._postService.addUserTag(dataToSend).subscribe((data: any) => {
        console.log('el usertag se ha registrado ok!!!');
        console.log(data);
      }, error => { 
        console.error(error); 
      });
    });
  }


}
