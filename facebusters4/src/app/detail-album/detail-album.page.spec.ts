import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailAlbumPage } from './detail-album.page';

describe('DetailAlbumPage', () => {
  let component: DetailAlbumPage;
  let fixture: ComponentFixture<DetailAlbumPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailAlbumPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailAlbumPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
