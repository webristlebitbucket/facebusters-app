import { NewPostPage } from './../new-post/new-post.page';
import { FullAlbumPage } from './../full-album/full-album.page';
import { StoreService } from './../../providers/store/store';
import { PostService } from './../../providers/post-service/post-service';
import { LoadingController } from '@ionic/angular';
import { Album, Photo, User, Location, Tag, HashTag } from './../../models/models';
import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalCommentsComponent } from './../shared/modal-comments/modal-comments.component';
import { ModalTagUserComponent } from './../shared/modal-tag-user/modal-tag-user.component';
import { AlertController, ModalController, ActionSheetController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Location as LocationM } from '@angular/common';

@Component({
  selector: 'app-detail-album',
  templateUrl: './detail-album.page.html',
  styleUrls: ['./detail-album.page.scss'],
})
export class DetailAlbumPage {

  public album: Album;
  public photoActive: Photo;
  public showTags: boolean = false;
  public showTextNoUserTags: boolean = false;
  public loading: any;
  public isLoading: boolean = true;
  private modalFullScreen: any;
  private modalTagPhoto: any;
  private modalFullAlbum: any;
  public slideOpts = {
    zoom: false
  };

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private actionSheetCtrl: ActionSheetController,
    private _store: StoreService,
    private _postService: PostService,
    private translate: TranslateService,
    public loadingController: LoadingController,
    private socialSharing: SocialSharing,
    private location: LocationM
  ) { }


  ionViewWillEnter() {
    this.initData();
  }

  initData(){
    this.isLoading = true;
    this.album = new Album();
    this.album.pk = this.activatedRoute.snapshot.paramMap.get('pk')
    this.album.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.getAlbumInfo(this.activatedRoute.snapshot.paramMap.get('pk'));
  }


  getAlbumInfo(pkAlbum: string) {
    this._postService.getAlbum(pkAlbum).subscribe((data: any) => {
      this.album.title = data.title;
      this.album.dateRange = data.date_range;
      this.album.isPublic = !data.private;
      this.getAllPicturesOfAlbum(this.album.id);
    }, error => { console.error(error); this.dismissLoading(); });
  }

  ionViewWillLeave() {
    this.isLoading = true;
    this.album = null;
    this.photoActive = null;
    this.showTags = false;
    this.showTextNoUserTags = false;
  }

  ionViewDidLeave() {
    this.isLoading = true;
    this.album = null;
    this.photoActive = null;
    this.showTags = false;
    this.showTextNoUserTags = false;
  }

  async presentLoading() {
    this.isLoading = true;
    this.loading = await this.loadingController.create({
      message: ''
    });
    return await this.loading.present();
  }

  dismissLoading() {

    // this.loading.dismiss();
    this.isLoading = false;
  }


  getAllPicturesOfAlbum(identifier: any) {
    this._postService.getPicturesOfAlbum(identifier).subscribe((data: any) => {
      this.album.photos = [];
      if(data.count===0){
        this.location.back();
      }
      data.results.forEach(el => {
        this.album.photos.push(this.initPhoto(el));
      });
      this.sortArray('order', this.album.photos);
      this.photoActive = this.setPhotoActive(0);
      this.dismissLoading();
    }, error => { console.error(error); this.dismissLoading(); });
  }

  sortArray(property, array) {
    property = property.split('.');
    var l = property.length;
    return array.sort(function (a, b) {
      var i = 0;
      while (i < l) {
        a = a[property[i]];
        b = b[property[i]];
        i++;
      }
      return a < b ? -1 : 1;
    });
  }


  initPhoto(data: any) {
    const photo = new Photo();
    photo.id = data.identifier;
    photo.title = data.title;
    photo.image = data.image;
    photo.thumbnail = data.image_thumbnail;
    photo.isPublic = data.public;
    photo.metaDate = data.meta_date;
    photo.createdAt = data.created_at;
    photo.hashTags = this.initHastags(data.image_tags);
    photo.location = this.initLocation(data);
    photo.user = this.initUser(data.user_info);
    photo.filter = data.picture_filter;
    photo.order = data.order;
    if (!photo.filter) { photo.filter = ''; }
    return photo;
  }
  initUser(data: any) {
    let user: User = new User();
    user = new User();
    if (data) {
      user.avatar = data.avatar_thumbnail;
      user.nickname = data.username;
      user.id = data.identifier;
    }
    return user;
  }
  initLocation(data: any) {
    let location: Location = new Location();
    location.address = data.address_en;
    location.latitude = data.meta_location.latitude;
    location.longitude = data.meta_location.longitude;
    return location;
  }
  initHastags(data: any[]) {
    let hashTags: HashTag[] = [];
    data.forEach(t => {
      const hashtag = new HashTag();
      hashtag.hashTag = t;
      hashTags.push(hashtag);
    });
    return hashTags;
  }

  setPhotoActive(index: number = 0) {
    this.photoActive = this.album.photos[index];
    return this.photoActive;
  }

  showUserTags(photo: Photo, show: boolean = null) {
    if (show) {
      this.showTags = show;
    } else {
      this.showTags = !this.showTags;
    }
    if (this.showTags) {
      this.loading = true;
      this.album.photos.forEach(photo => {
        this._postService.getUserTags(photo.id).subscribe((data: any) => {
          if (data.count === 0 && !this.showTextNoUserTags) {
            this.showTextNoUserTags = true;
            let myThis = this;
            setTimeout(function () { ; myThis.showTextNoUserTags = false; }, 3000);
          } else {
            photo.tags = [];
            data.results.forEach(element => {
              let tagUser: Tag = new Tag();
              tagUser.id = element.identifier;
              tagUser.coords.row = element.top;
              tagUser.coords.col = element.left;
              tagUser.hide = element.hide;
              if (element.user) {
                tagUser.user = new User();
                tagUser.user.id = element.user_info.identifier;
                tagUser.user.avatar = element.user_info.avatar_thumbnail;
                tagUser.user.nickname = element.user_info.username;
              }
              if (element.non_user) {
                tagUser.nonUser = element.non_user;
              }
              photo.tags.push(tagUser);
            });
          }
          this.loading = false;
        }, error => { console.error(error); this.loading = false; });
      });
    }
  }


  async goToFullAlbum(idAlbum, pkAlbum) {
    const modal = await this.modalCtrl.create({
      component: FullAlbumPage,
      componentProps: { 'album': this.album }
    });
    return await modal.present();

    // this.router.navigateByUrl('tabs/(timeline:full-album/' + idAlbum + '/' + pkAlbum+')');
    // this.router.navigateByUrl('full-album/' + idAlbum + '/' + pkAlbum);
  }

  async showModalTagPhoto(photo: Photo, i: Number) {
    const modal = await this.modalCtrl.create({
      component: ModalTagUserComponent,
      componentProps: { 'photo': photo, "album_index": i }
    });
    this.showTags = false;
    modal.onDidDismiss().then((data: any) => {
      this.showUserTags(photo, true);
    });
    return await modal.present();
  }

  async showModalComments(photo: Photo) {
    const modal = await this.modalCtrl.create({
      component: ModalCommentsComponent,
      componentProps: { 'photo': photo }
    });
    return await modal.present();
  }

  reportPhoto(dataToSend: any) {
    this._postService.reportPhoto(dataToSend).subscribe((data: any) => {
      this.presentAlertPhotoReported('ok');
    }, error => {
      console.error(error);
      this.presentAlertPhotoReported('nook');
    });
  }

  async presentActionSheet() {
    let actionSheet;
    if (this._store.get('myUser').id === this.photoActive.user.id) {
      actionSheet = await this.actionSheetCtrl.create({ // Allow edit album
        buttons: [
          {
            text: this.translate.instant('MY_ALBUM_DETAILS_3DOTS_EDIT'),
            handler: () => {
              this.openModalEditpost();
            }
          },
          {
            text: this.translate.instant('MY_PHOTO_DETAIL_3DOTS_DELETE'),
            role: 'destructive',
            handler: () => {
              this.deletePhoto();
            }
          },
        ]
      });
    } else {
      actionSheet = await this.actionSheetCtrl.create({
        // Allow report if is another user
        buttons: [
          {
            text: this.translate.instant('PHOTO_DETAIL_3DOTS'),
            handler: () => {
              this.presentPromptReport();
            }
          },
        ]
      });
    }
    try { await actionSheet.present(); } catch (error) { }
  }

  async presentPromptReport() {
    const alert = await this.alertCtrl.create({
      header: this.translate.instant('PHOTO_DETAIL_3DOTS_MESSAGE'),
      inputs: [
        {
          name: 'reason',
          placeholder: this.translate.instant('PHOTO_DETAIL_3DOTS_MESSAGE_PLACEHOLDER')
        },
      ],
      buttons: [
        {
          text: this.translate.instant('PHOTO_DETAIL_3DOTS_BUTTON_CANCEL'),
          role: 'cancel',
          handler: data => {
          }
        },
        {
          text: this.translate.instant('Report'),
          handler: data => {
            let dataTosend: any = {
              'picture': this.photoActive.id,
              'comment': data.reason
            }
            this.reportPhoto(dataTosend);
          }
        }
      ]
    });
    await alert.present();
  }

  async presentAlertPhotoReported(status: string) {
    let title: string = '';
    if (status === 'ok') { title = this.translate.instant('PHOTO_DETAIL_3DOTS_MESSAGE_REPORT_SENT'); }
    else { title = this.translate.instant('PHOTO_DETAIL_3DOTS_MESSAGE_ALREADY_REPORT'); }
    const alert = await this.alertCtrl.create({
      header: title,
      buttons: [this.translate.instant('PHOTO_DETAIL_3DOTS_BUTTON_REPORTED')]
    });
    await alert.present();
  }

  goPublicProfile(user) {
    if (this._store.get('myUser').id === user.id) {
      this.router.navigateByUrl('tabs/my-profile');
    } else {
      this.router.navigateByUrl('tabs/profile/' + user.id);
    }
  }

  showTagsPhotoPanel(index) {
    var tags_panel = document.getElementById('tag-panel-' + index);
    tags_panel.classList.remove("d-none");
  }

  async openModalEditpost() {
    const modal = await this.modalCtrl.create({
      component: NewPostPage,
      componentProps: { albumEdit: this.album }
    });
    modal.onDidDismiss().then((data: any) => {
      if (data) {
        if (data.data) {
          this.isLoading = true;
          this.album = data.data;
          this.isLoading = false;
        }
      }
    });
    return await modal.present();
  }

  deletePhoto(){
    this._postService.editPicture(this.photoActive.id, {'hide': true }).subscribe((data: any) => {
      this.presentAlertPhotoDeleted();
      this.initData();
    }, error => { console.error(error); });
  }

  async presentAlertPhotoDeleted() {
    const title = this.translate.instant('MY_PHOTO_DETAIL_3DOTS_DELETE_MESSAGE');
    const alert = await this.alertCtrl.create({
      header: title,
      buttons: [
          {
            text: this.translate.instant('MY_PHOTO_DETAIL_3DOTS_DELETE_BUTTON'),
            handler: (data) => {
            }
          }
      ]
    });
    await alert.present();
  }

  sharePhoto() {
    this.socialSharing.share('https://api.facebusters.com/picture_social/' + this.photoActive.id.toString()).then(() => {
    }).catch(() => { });
  }
}