import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { LoginService } from './../../providers/login/login.service';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { UserService } from './../../providers/user-service/user-service';
import { PostService } from './../../providers/post-service/post-service';
import { User } from './../../models/models';
import { StoreService } from './../../providers/store/store';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, Platform, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { File, FileEntry, IFile } from '@ionic-native/file/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { LoadingController } from '@ionic/angular';
import { ModalFacebookContactsComponent } from '../shared/modal-facebook/modal-facebook';
export interface CameraDetail {
  filename: string;
  json_metadata: string;
}

export interface CameraExifDetail {
  aperture: string;
  datetime: string;
  exposureTime: string;
  flash: string;
  focalLength: string;
  gpsAltitude: string;
  gpsAltitudeRef: string;
  gpsDateStamp: string;
  gpsLatitude: string;
  gpsLatitudeRef: string;
  gpsLongitude: string;
  gpsLongitudeRef: string;
  gpsProcessingMethod: string;
  gpsTimestamp: string;
  iso: string;
  make: string;
  model: string;
  orientation: string;
  whiteBalance: string;
}

export interface Output {
  base64: string;
  exifDetail: CameraExifDetail;
}

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent {

  public user: User = null;
  public accessWithTouchId: boolean = false;
  public old_password: string = null;
  public new_password: string = null;
  public new_password_repeat: string = null;
  public selectedFile: File;
  public selectedFileB64: any;
  public uploading: boolean = false;
  public isMobile: boolean;
  public msg_error: string = null;
  public auth_error: boolean = false;
  public lang_code: string = '';
  public delete_account: boolean = null;

  public isLoading: boolean = true;
  private loading: any;
  public nApiCalls: number = 0; // 1



  constructor(
    private modalController: ModalController,
    private facebook: Facebook,
    private alertController: AlertController,
    private _store: StoreService,
    private _postService: PostService,
    private router: Router,
    private translate: TranslateService,
    private camera: Camera,
    private file: File,
    public base64: Base64,
    private iab: InAppBrowser,
    private userService: UserService,
    private loadingController: LoadingController,
    private oneSignal: OneSignal,
    private loginService: LoginService,
    private platform: Platform
  ) {
    this.isMobile = this._store.get('isMobile');
  }

  ionViewWillEnter() {
    this.old_password = null;
    this.new_password = null;
    this.new_password_repeat = null;
    // this.presentLoading();
    this.initUser();
  }


  ionViewWillLeave() {
    this.isLoading = true;
    this.user = null;
    this.nApiCalls = 0;
  }

  async presentLoading() {
    this.isLoading = true;
    this.loading = await this.loadingController.create({
      message: this.translate.instant('GENERIC_LOADING_TEXT')
    });
    return await this.loading.present();
  }

  dismissLoading() {
    // if(this.loading){
    //   setTimeout(() => { try { this.loading.dismiss(); } catch (error) { } }, 1000);
    // }else{
    //   setTimeout(() => { try { this.loading.dismiss(); } catch (error) { } }, 2000);
    // }

    this.isLoading = false;
  }

  checkIsComplete() {
    if (this.nApiCalls == 1) {
      this.dismissLoading();
    }
  }


  initUser() {
    this.user = null;
    if (!this.user) {
      this.userService.getMyUser().subscribe((data: any) => {
        this.user = this._store.parserApiToModel('user', data);
        this.nApiCalls++;
        this.checkIsComplete();
      }, error => {
        console.error(error);
        this.nApiCalls++;
        this.checkIsComplete();
      });
    } else {
      this.nApiCalls++;
      this.checkIsComplete();
    }
  }

  convertToBase64(url, outputFormat) {
    return new Promise((resolve, reject) => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = function () {
        let canvas = <HTMLCanvasElement>document.createElement('CANVAS'), ctx = canvas.getContext('2d'), dataURL;
        canvas.height = img.height;
        canvas.width = img.width;
        ctx.drawImage(img, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        canvas = null;
        resolve(dataURL);
      };
      img.src = url;
    });
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validateDataToSend() {
    let error: string = null;
    if (this.user.firstName.length <= 2 || !this.user.firstName ||  this.user.firstName === '') {
      error = 'EDIT_PROFILE_NAME_ERROR';
      this.presentAlertError(error);
      return false;
    }
    if (this.user.lastName.length <= 2 || !this.user.lastName ||  this.user.lastName === '') {
      error = 'EDIT_PROFILE_SURNAME_ERROR';
      this.presentAlertError(error);
      return false;
    }
    if (!this.validateEmail(this.user.email)) {
      error = 'EDIT_PROFILE_EMAIL_ERROR';
      this.presentAlertError(error);
      return false;
    }
    if (this.new_password) {
      if (this.new_password.length <= 4 || !this.new_password ||  this.new_password === '') {
        error = 'EDIT_PROFILE_NEW_PASSWORD_ERROR';
        this.presentAlertError(error);
        return false;
      }
      if (this.new_password != this.new_password_repeat) {
        error = 'EDIT_PROFILE_PASSWORD_REPEAT_ERROR';
        this.presentAlertError(error);
        return false;
      }
    }

    return true;
  }



  save() {
    if (!this.validateDataToSend()) { return; }
    this.translate.use(this.user.language.toLowerCase());
    if (this.uploading) { return; }
    this.uploading = true;
    const dataForm: any = {
      'first_name': this.user.firstName,
      'last_name': this.user.lastName,
      'avatar': this.selectedFileB64,
      'email': this.user.email,
      'language': this.user.language,
      'new_password': this.new_password,
      'old_password': this.old_password,
      'tag_control': this.user.tagControl,
      'delete_account': this.delete_account
    }

    let dataToSend = Object.assign({}, dataForm);
    Object.keys(dataToSend).forEach((key) => (dataToSend[key] == null) && delete dataToSend[key]);
    this._postService.editMyProfile(this.user.id, dataToSend).subscribe((data: any) => {
      this._store.eventChangeMyUser(this.user);
      this.presentAlertSuccesfull();
      this.auth_error = false;
      this.uploading = false;
    }, error => {
      console.error(error.status);
      this.uploading = false;
      if (error.status == 403) { //forbidden - Wrong password
        this.auth_error = true;
        this.msg_error = this.translate.instant('EDIT_PROFILE_PASSWORD_ERROR');
        this.presentAlertError('EDIT_PROFILE_PASSWORD_ERROR');
        this.new_password = null;
        this.old_password = null;
      }
    });
  }

  logout() {
    let idOneSignal = this._store.idOneSignal;
    if ((this.platform.is('cordova') ||  this.platform.is('ios')) && idOneSignal) {
      this.loginService.subscribeDevice(idOneSignal, 'remove').subscribe(data => {
        this._store.closeSession();
        localStorage.setItem('token', null);
        this.router.navigate(['/login'], { replaceUrl: true });
      }, err => {
        this._store.closeSession();
        localStorage.setItem('token', null);
        this.router.navigate(['/login'], { replaceUrl: true });
      });
    } else {
      this._store.closeSession();
      localStorage.setItem('token', null);
      this.router.navigate(['/login'], { replaceUrl: true });
    }
    this.modalController.dismiss();
  }

  enableTouchId() {
    this._store.set('accessWithTouchId', this.accessWithTouchId);
    if (this.accessWithTouchId) {
      localStorage.setItem('accessWithTouchId', 'enabled');
    } else {
      localStorage.setItem('accessWithTouchId', 'disabled');
    }
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    this.getBase64(this.selectedFile)
    let reader = new FileReader();
    reader.onload = (event: ProgressEvent) => {
      // this.user.avatar = (<FileReader>event.target).result;
      // TODO: ERROR AQUI
    }
    reader.readAsDataURL(event.target.files[0]);
  }

  async showCamera() {
    const alert = await this.alertController.create({
      header: this.translate.instant('EDIT_PROFILE_PHOTO_INPUT_OPEN'),
      buttons: [
        {
          text: this.translate.instant('EDIT_PROFILE_PHOTO_INPUT_CAMERA'),
          handler: () => {
            this.openCamera();
          }
        }, {
          text: this.translate.instant('EDIT_PROFILE_PHOTO_INPUT_GALLERY'),
          handler: () => {
            this.openGallery();
          }
        }
      ]
    });

    await alert.present();
  }

  async openCamera() {
    const options: CameraOptions = {
      quality: 30,
      targetWidth: 1024,
      targetHeight: 1024,
      sourceType: this.camera.PictureSourceType.CAMERA, //PHOTOLIBRARY
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {

      var thisResult = JSON.parse(imageData);
      let fileName = thisResult.filename.split('/').pop();
      let path = thisResult.filename.substring(0, thisResult.filename.lastIndexOf("/") + 1);

      this.file.readAsDataURL(path, fileName)
        .then(base64File => {
          this.user.avatar = base64File;
          this.selectedFileB64 = base64File;
          this.save();
        })
        .catch(() => {
          console.error('Error reading file');
        })

    }, (err) => {
    });
  }
  openGallery() {
    const options: CameraOptions = {
      quality: 30,
      targetWidth: 1024,
      targetHeight: 1024,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {

      var thisResult = JSON.parse(imageData);
      let fileName = thisResult.filename.split('/').pop();
      let path = thisResult.filename.substring(0, thisResult.filename.lastIndexOf("/") + 1);

      this.file.readAsDataURL(path, fileName)
        .then(base64File => {
          this.user.avatar = base64File;
          this.selectedFileB64 = base64File;
          this.save();
        })
        .catch(() => {
          console.error('Error reading file');
        })

    }, (err) => {
    });
  }

  getBase64(file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    var myThis = this;
    reader.onload = function () {
      myThis.selectedFileB64 = reader.result;
    };
    reader.onerror = function (error) {
      console.error('Error: ', error);
    };
  }

  async presentAlertSuccesfull() {
    const alert = await this.alertController.create({
      message: this.translate.instant('EDIT_PROFILE_BUTTON_SAVE'),
      buttons: [
        {
          text: this.translate.instant('EDIT_PROFILE_BUTTON_SAVED'),
          handler: () => {
            // this.goToMyProfile();
            this.modalController.dismiss();
          }
        }
      ]
    });

    await alert.present();
  }

  goToMyProfile() {
    this.router.navigateByUrl('tabs/my-profile');
  }

  /**
   * Change the lang of the app
   * @param lang strinc with lang code
   */
  changeLang(lang: string) {
    this.lang_code = lang;
    this.translate.use(lang);
  }

  OpenUrl(url: string) {
    let browser = this.iab.create(url, '_blank', { zoom: 'no', location: 'no' });
    browser.show();
  }

  async presentAlertError(error: string = this.translate.instant('GENERIC_POPUP_BODY')) {
    const alert = await this.alertController.create({
      header: this.translate.instant('GENERIC_POPUP_HEADER_ERROR'),
      subHeader: this.translate.instant(error),
      buttons: [this.translate.instant('GENERIC_POPUP_BUTTON_ERROR')]
    });
    await alert.present();
  }

  setTagControl() {
    if (this.user.tagControl) {
      this.presentAlertInfoTagControl();
    }
  }

  async presentAlertInfoTagControl() {
    const alert = await this.alertController.create({
      header: this.translate.instant('EDIT_PROFILE_TAG_CONTROL_CONFIRMATION'),
      buttons: [
        {
          text: this.translate.instant('EDIT_PROFILE_TAG_CONTROL_BUTTON_CANCEL'),
          role: 'cancel',
          handler: () => {
            this.user.tagControl = false;
          }
        }, {
          text: this.translate.instant('EDIT_PROFILE_TAG_CONTROL_BUTTON_OK'),
          handler: () => {
          }
        }
      ]
    });
    await alert.present();
  }

  async presentAlertDeleteAccount() {
    const alert = await this.alertController.create({
      header: this.translate.instant('EDIT_PROFILE_DELETE_POPUP'),
      buttons: [
        {
          text: this.translate.instant('EDIT_PROFILE_DELETE_POPUP_CANCEL'),
          role: 'cancel',
          handler: () => {
          }
        }, {
          text: this.translate.instant('EDIT_PROFILE_DELETE_POPUP_CONFIRM'),
          handler: () => {
            this.deleteAccount();
          }
        }
      ]
    });
    await alert.present();
  }

  async presentAlertLinkFacebook() {
    const alert = await this.alertController.create({
      header: this.translate.instant('EDIT_PROFILE_POPUP_LINK_FACEBOOK'),
      buttons: [
        {
          text: this.translate.instant('GENERIC_POPUP_BUTTON_CANCEL'),
          role: 'cancel',
          handler: () => { }
        }, {
          text: this.translate.instant('GENERIC_POPUP_BUTTON_OK'),
          handler: () => {
            // TODO: Cambiar esto por el de la API
            this.getFacebookContacts();
          }
        }
      ]
    });
    await alert.present();
  }


  getFacebookContacts() {
    if(!this.platform.is("cordova") ){
      this.openModalFacebookContacts(null);
      return;
    }
    this.facebook.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
        console.log('Logged into Facebook!', JSON.stringify(res));
        this.openModalFacebookContacts(res);


      })
      .catch(e => console.log('Error logging into Facebook', e));
  }


  async openModalFacebookContacts(data: any = null) {
    const modal = await this.modalController.create({
      component: ModalFacebookContactsComponent,
      componentProps: { data: data }
    });
    modal.onDidDismiss().then((data: any) => {
    });
    return await modal.present();
  }



  deleteAccount() {
    let dataToSend: any = {
      'delete_account': true
    }
    this._postService.editMyProfile(this.user.id, dataToSend).subscribe((data: any) => {
      localStorage.setItem('tokenTouchId', null);
      this.logout();
    }, error => {
      console.log(error);
    });
  }


  linkFacebook() {
    this.presentAlertLinkFacebook();
  }
}
