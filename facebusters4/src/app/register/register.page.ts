import { PostService } from './../../providers/post-service/post-service';
import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { StoreService } from './../../providers/store/store';
import { TranslateService } from '@ngx-translate/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { File, FileEntry, IFile } from '@ionic-native/file/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { Globalization } from '@ionic-native/globalization/ngx';


export interface CameraDetail {
  filename: string;
  json_metadata: string;
}

export interface CameraExifDetail {
  aperture: string;
  datetime: string;
  exposureTime: string;
  flash: string;
  focalLength: string;
  gpsAltitude: string;
  gpsAltitudeRef: string;
  gpsDateStamp: string;
  gpsLatitude: string;
  gpsLatitudeRef: string;
  gpsLongitude: string;
  gpsLongitudeRef: string;
  gpsProcessingMethod: string;
  gpsTimestamp: string;
  iso: string;
  make: string;
  model: string;
  orientation: string;
  whiteBalance: string;
}

export interface Output {
  base64: string;
  exifDetail: CameraExifDetail;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss']
})
export class RegisterPage {
  title: string = this.translate.instant('REGISTER_TITLE');
  stepRegister: number;
  msgError: string = null;
  public isMobile: boolean;
  public selectedFile: File;
  public selectedFileB64: any;
  public avatarSelected: boolean = false;

  //check input states
  public user_icon_color: string = 'gris';
  public email_icon_color: string = 'gris';
  public name_icon_color: string = 'gris';
  public lastname_icon_color: string = 'gris';
  public password_icon_color: string = 'gris';
  public repeat_password_icon_color: string = 'gris';



  dataForm: any = {
    'username': null,
    'password': null,
    'repeat_password': null,
    'email': null,
    'avatar': null,
    'name': null,
    'last_name': null,
    'language': null
  }

  constructor(
    private alertController: AlertController,
    private router: Router,
    private _postService: PostService,
    private _store: StoreService,
    private translate : TranslateService,
    private camera: Camera,
    private file: File,
    public base64: Base64,
    private iab: InAppBrowser,
    private globalization: Globalization
  ) {
    this.stepRegister = 1;
    this.isMobile = this._store.get('isMobile');
    this.globalization.getPreferredLanguage().then(res => {
      this.dataForm.language = res.value;
    }).catch(e => console.error(e));
  }

  goToLogin() {
    this.router.navigateByUrl('login');
  }

  nextStep() {
    this.stepRegister++;
  }

  previousStep() {
    this.stepRegister--;
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
    this.getBase64(this.selectedFile)
    let reader = new FileReader();
    reader.onload = (event: ProgressEvent) => {
      this.dataForm.avatar = (<FileReader>event.target).result;
      this.avatarSelected = true;
    }
    reader.readAsDataURL(event.target.files[0]);
  }

  getBase64(file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    var myThis = this;
    reader.onload = function () {
      myThis.selectedFileB64 = reader.result;
      myThis.dataForm.avatar = myThis.selectedFileB64;
  
    };
    reader.onerror = function (error) {
      console.error('Error: ', error);
      myThis.presentAlertError();  
    };
  }

  async showCamera(){
    const alert = await this.alertController.create({
      header: this.translate.instant('REGISTER_PHOTO_INPUT'),
      buttons: [
        {
          text: this.translate.instant('REGISTER_PHOTO_INPUT_CAMERA'),
          handler: () => {
            this.openCamera();
          }
        }, {
          text: this.translate.instant('REGISTER_PHOTO_INPUT_GALLERY'),
          handler: () => {
            this.openGallery();
          }
        },{
          text: this.translate.instant('REGISTER_PHOTO_INPUT_BUTTON'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {}
        }
      ]
    });

    await alert.present();
  }

  async openCamera(){
    const options: CameraOptions = {
      quality: 30,
      targetWidth: 1024,
      targetHeight: 1024,
      sourceType: this.camera.PictureSourceType.CAMERA, //PHOTOLIBRARY
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }
    this.camera.getPicture(options).then((imageData) => {

      var thisResult = JSON.parse(imageData);
      let fileName = thisResult.filename.split('/').pop();
      let path = thisResult.filename.substring(0, thisResult.filename.lastIndexOf("/") + 1);

      this.file.readAsDataURL(path, fileName)
        .then(base64File => {
          this.dataForm.avatar = base64File;
          this.avatarSelected = true;
          this.selectedFileB64 = base64File;
          this.photoUploaded();
        })
        .catch(() => {
            console.error('Error reading file');
            this.presentAlertError();  
        })

    }, (err) => {
  });
}

  openGallery(){
    const options: CameraOptions = {
      quality: 30,
      targetWidth: 1024,
      targetHeight: 1024,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {

      var thisResult = JSON.parse(imageData);
      let fileName = thisResult.filename.split('/').pop();
      let path = thisResult.filename.substring(0, thisResult.filename.lastIndexOf("/") + 1);

      this.file.readAsDataURL(path, fileName)
        .then(base64File => {
          this.dataForm.avatar = base64File;
          this.avatarSelected = true;
          this.selectedFileB64 = base64File;
          this.photoUploaded();
        })
        .catch(() => {
            console.error('Error reading file');
            this.presentAlertError();  
        })

    }, (err) => {
  });
}
  //step1 check inputs
  checkUser(){
    if (!this.dataForm.username || this.dataForm.username.length< 4) {
      this.user_icon_color = 'pink';
      this.msgError = this.translate.instant('REGISTER_USERNAME_ERROR');
      this.presentAlertError(this.msgError);  
      return false;
    } else {
      this.user_icon_color = 'green';
      this.msgError = null;
      return true;
    }
  }
  checkMail(){
    if (!this.validateEmail(this.dataForm.email)) {
      this.email_icon_color = 'pink';
      this.msgError = this.translate.instant('REGISTER_EMAIL_ERROR');
      this.presentAlertError(this.msgError);  
      return false;
    } else {
      this.email_icon_color = 'green';
      this.msgError = null;
      return true;
    }
  }

  //step2 check inputs
  checkName(){
    if (!this.dataForm.name || this.dataForm.name.length<= 2) {
      this.name_icon_color = 'pink';
      this.msgError = this.translate.instant('REGISTER_NAME_ERROR');
      this.presentAlertError(this.msgError);  
      return false;
    } else {
      this.name_icon_color = 'green';
      this.msgError = null;
      return true;
    }
  }
  checkLastName(){
    if (!this.dataForm.last_name || this.dataForm.last_name.length<= 2) {
      this.lastname_icon_color = 'pink';
      this.msgError = this.translate.instant('REGISTER_SURNAME_ERROR');
      this.presentAlertError(this.msgError);  
      return false;
    } else {
      this.lastname_icon_color = 'green';
      this.msgError = null;
      return true;
    }
  }
  checkPassword(){
    if (!this.dataForm.password || this.dataForm.password.length<=4) {
      this.password_icon_color = 'pink';
      this.msgError = this.translate.instant('REGISTER_PASSWORD_ERROR');
      this.presentAlertError(this.msgError);  
      return false;
    } else {
      this.password_icon_color = 'green';
      this.msgError = null;
      return true;
    }
  }
  checkRepeatPassword(){
    if (this.dataForm.password != this.dataForm.repeat_password) {
      this.repeat_password_icon_color = 'pink';
      this.msgError = this.translate.instant('REGISTER_PASSWORD_REPEAT_ERROR');
      this.presentAlertError(this.msgError);      
      return false;
    } else {
      this.repeat_password_icon_color = 'green';
      this.msgError = null;
      return true;
    }
  }


  validateStep1(){
    if(!this.dataForm.avatar){
      this.msgError = this.translate.instant('REGISTER_AVATAR_ERROR');
      this.presentAlertError(this.msgError);      
      return;
    }
    if (this.checkUser() && this.checkMail() && this.dataForm.avatar) {
      // check if email is available
      this._postService.checkEmailExists(this.dataForm.email).subscribe((data: any) => {
        this.nextStep();
        this.msgError = null;
      }, error => { 
        this.msgError = this.translate.instant('REGISTER_EMAIL_ERROR');
        this.presentAlertError(this.msgError);
        console.error(error); 
      });
    }else{
      // this.msgError = 'GENERIC_POPUP_BODY';
      // this.presentAlertError(this.msgError);      
    }
  }
  validateStep2(){
    if (this.checkName() && this.checkLastName() && this.checkPassword() && this.checkRepeatPassword()) {
      this.register();
    }
  }
  validateStep3(){
    return;
  }

  register() {
    this.dataForm.first_name = this.dataForm.name;
    let dataToSend = Object.assign({}, this.dataForm);
    this._postService.registerUser(dataToSend).subscribe((data: any) => {
      this.nextStep();
    }, error => { console.error(error); });
  }


  validateAccount() {
    this.msgError = null;
    if (!this.dataForm.verificationCode || this.dataForm.verificationCode.length <= 4) {
      this.msgError = this.translate.instant('REGISTER_CODE_ERROR');
      this.presentAlertError(this.msgError);
      return;
    }else{
      this.msgError = null;
    }
    let dataToSend: any = {
      'verification_code': this.dataForm.verificationCode,
      'email': this.dataForm.email
    }
    this._postService.verifyAccount(dataToSend).subscribe((data: any) => {
      this.msgError = null;
      this.showAlert();
    }, error => { 
      this.presentAlertError(); 
      console.error(error); 
    });
  }

  async showAlert() {
    const alert = await this.alertController.create({
      header: this.translate.instant('GENERIC_POPUP_HEADER_OK'),
      subHeader: this.translate.instant('REGISTER_VALIDATION_MESSAGE'),
      message: this.translate.instant('REGISTER_VALIDATION_MESSAGE_SUBLINE'),
      buttons: [
        {
          text: this.translate.instant('REGISTER_BUTTON_VALIDATION'),
          handler: () => {
            this.goToLogin();
          }
        }
      ]
    });

    await alert.present();
  }

  async photoUploaded() {
    const alert = await this.alertController.create({
      header: this.translate.instant('GENERIC_POPUP_HEADER_OK'),
      message: this.translate.instant('REGISTER_PHOTO_UPLOADED'),
      buttons: [
        {
          text: this.translate.instant('GENERIC_POPUP_BUTTON_OK'),
          handler: () => {
          }
        }
      ]
    });

    await alert.present();
  }

  OpenUrl(url: string)
  {
    let browser = this.iab.create(url,'_blank',{zoom:'no',location:'no'});
    browser.show();
  }

  async presentAlertError(error:string = this.translate.instant('GENERIC_POPUP_BODY')) {
    const alert = await this.alertController.create({
      header: this.translate.instant('GENERIC_POPUP_HEADER_ERROR'),
      subHeader: this.translate.instant(error),
      buttons: [this.translate.instant('GENERIC_POPUP_BUTTON_ERROR')]
    });
    await alert.present();
  }

}
