import { OneSignal } from '@ionic-native/onesignal/ngx';
import { AlertController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { UserService } from './../../providers/user-service/user-service';
import { LoginService } from './../../providers/login/login.service';
import { StoreService } from './../../providers/store/store';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';

import { GooglePlus } from '@ionic-native/google-plus/ngx';
@Component({
  selector: 'page-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    private googlePlus: GooglePlus,
    private faio: FingerprintAIO,
    private fb: Facebook,
    private _store: StoreService,
    private _loginService: LoginService,
    private _userService: UserService,
    private router: Router,
    private translate: TranslateService,
    private alertCtrl: AlertController,
    private platform: Platform,
    private oneSignal: OneSignal
  ) {
    this.isMobile = this._store.get('isMobile');
  }

  @ViewChild('usernameField') usernameField: ElementRef;
  public form: any = {
    'username': null,
    'password': null
  };
  public error_user = false;
  public error_pwd = false;
  public isMobile: boolean;
  public hasTokenTouchID = false;
  public errorMsg: string = null;
  public socialLoginData: any = {};

  editUsername(): void {
    this.usernameField.nativeElement.focus();
  }


  loginWithFingerPrint() {
    const tokenTouchId = localStorage.getItem('tokenTouchId');
    if (!tokenTouchId) {
      this.presentAlertError('LOGIN_ERROR_USER_PASS');
      return;
    }
    this.faio.show({
      clientId: 'Fingerprint-Facebusters',
      clientSecret: 'password', // Only necessary for Android
      disableBackup: true,  // Only for Android(optional)
      localizedFallbackTitle: 'Use Pin', // Only for iOS
      localizedReason: 'Please authenticate' // Only for iOS
    }).then((result: any) => {
      if (tokenTouchId) {
        this._store.set('token', tokenTouchId);
        localStorage.setItem('token', tokenTouchId);
        this.getMyUser();
      }
    }).catch((error: any) => {
      console.error(error);
      this.presentAlertError('GENERIC_POPUP_BODY');
    });
  }

  goToTabs() {
    this.router.navigateByUrl('tabs/timeline');
  }
  goToResetPassword() {
    this.router.navigateByUrl('/reset-password');
  }
  goToRegister() {
    this.router.navigateByUrl('/register');
  }

  login() {
    if (this.checkFormIsValid() === true) {
      this.form['username'] = this.form['username'].toLowerCase();
      this._loginService.getToken(this.form).subscribe(
        data => {
          this._store.set('token', data['token']);
          localStorage.setItem('token', data['token']);
          this._store.set('tokenTouchId', data['token']);
          localStorage.setItem('tokenTouchId', data['token']);
          this.getMyUser();
        }, error => {
          console.error(error);
          this.presentAlertError('LOGIN_VALIDATION_ERROR');
        });
    }
  }

  getMyUser() {
    this._userService.getMyUser().subscribe(data => {
      this._store.eventChangeMyUser(this._store.parserApiToModel('user', data));
      if (this.platform.is('cordova')) {
        this.handlerNotifications();
      }
      setTimeout(() => { this.goToTabs(); }, 500);
    }, error => {
      console.error(error);
      this.errorMsg = 'Error getting user';
      this.presentAlertError('GENERIC_POPUP_BODY');
    });
  }

  checkFormIsValid() {
    if (!this.form['username'] && !this.form['password']) {
      this.errorMsg = this.translate.instant('Username and Password are required');
      this.error_user = true;
      this.error_pwd = true;
      this.presentAlertError('LOGIN_ERROR_USER_PASS');
      return false;
    }
    if (!this.form['username']) {
      this.errorMsg = this.translate.instant('Username is required');
      this.error_user = true;
      this.error_pwd = false;
      this.presentAlertError('LOGIN_ERROR_USERNAME');
      return false;
    }
    if (!this.form['password']) {
      this.errorMsg = this.translate.instant('Password is required');
      this.error_user = false;
      this.error_pwd = true;
      this.presentAlertError('LOGIN_ERROR_PASS');
      return false;
    }
    this.errorMsg = null;
    this.error_user = false;
    this.error_pwd = false;
    return true;
  }

  ionViewWillEnter() {
    console.log('will enter login');
  }

  ionViewEnter() {
    console.log('enter login');
  }

  ngOnInit() {
    console.log('init login');
    if (localStorage.getItem('tokenTouchId')) { // check if touchFaceIdToken exists
      this.hasTokenTouchID = true;
    }
  }

  async presentAlertError(error: string) {
    const alert = await this.alertCtrl.create({
      header: this.translate.instant('GENERIC_POPUP_HEADER_ERROR'),
      message: this.translate.instant(error),
      buttons: [
        {
          text: this.translate.instant('GENERIC_POPUP_BUTTON_ERROR'),
          handler: (blah) => {
          }
        }
      ]
    });
    await alert.present();
  }

  private handlerNotifications() {
    this.oneSignal.startInit('2a33a5f9-5598-4c08-9480-4a692335cb7f', '592496334026');
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    this.oneSignal.handleNotificationOpened().subscribe((jsonData: any) => {
      console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    });
    this.oneSignal.endInit();
    this.oneSignal.getIds().then((id) => {
      // console.info('playerids ==> ', JSON.stringify(id.userId));
      this._store.idOneSignal = id.userId;
      this._loginService.subscribeDevice(id.userId, 'add').subscribe(data => {
        console.log('Registrado ==> ', JSON.stringify(data));
      }, err => { console.log(err); });
    });
  }

  dataLoginFacebook = null;
  dataLoginGoogle = null;
  socialMobileSignIn(provider: string) {
    switch (provider) {
      case 'facebook':
        try {
          this.fb.logout();
        } catch (error) {
          alert(JSON.stringify(error));
        }
        this.fb.login(['public_profile', 'email', 'user_friends']).then((res: FacebookLoginResponse) => {
          console.log('Logged into Facebook!', JSON.stringify(res));
          console.log(res)
          this.fb.api('me?fields=id,name,last_name,picture,email', ['email']).then(response => {
            // console.log('data 2 facebook ==> ',  + JSON.stringify(response));
            this.dataLoginFacebook = { 'provider': 'facebook', 'data': response }
            // console.log(dataLoginFacebook);
            this._loginService.loginSocial(this.dataLoginFacebook).subscribe((data: any) => {
              this._store.set('token', data['token']);
              localStorage.setItem('token', data['token']);
              this._store.set('tokenTouchId', data['token']);
              localStorage.setItem('tokenTouchId', data['token']);
              this.getMyUser();
            }, error => {
              console.error(error);
              // this.presentAlertError('LOGIN_VALIDATION_ERROR');
              alert(JSON.stringify(error));
            });
          })
        }).catch(error => {
          console.error('Error logging into Facebook', error);
          alert(JSON.stringify(error));
        }
        );
        break;
      case 'google':
        try {
          this.googlePlus.logout();
        } catch (error) {
          alert(JSON.stringify(error));
        }
        this.googlePlus.login({ 'scopes': '' }).then(response => {
          this.dataLoginGoogle = { 'provider': 'google', 'data': response }
          // console.log(dataLoginGoogle);
          this._loginService.loginSocial(this.dataLoginGoogle).subscribe((data: any) => {
            this._store.set('token', data['token']);
            localStorage.setItem('token', data['token']);
            this._store.set('tokenTouchId', data['token']);
            localStorage.setItem('tokenTouchId', data['token']);
            this.getMyUser();
          }, error => {
            alert(JSON.stringify(error));
            console.error(error);
            // this.presentAlertError('LOGIN_VALIDATION_ERROR');
          });
        }, error => {
          console.error(error);
          alert(JSON.stringify(error));
          // this.presentAlertError('LOGIN_VALIDATION_ERROR');
        });
        break;
    }
  }

}

