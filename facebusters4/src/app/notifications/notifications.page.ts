import { UserService } from './../../providers/user-service/user-service';
import { Component } from '@angular/core';
import { NavController, ModalController, AlertController, Platform, LoadingController } from '@ionic/angular';
import { StoreService } from './../../providers/store/store';
import { PostService } from './../../providers/post-service/post-service';
import { Router } from '@angular/router';
import { Notification, User, Photo } from './../../models/models';
import { UtilsService } from './../../services/utils';
import { TranslateService } from '@ngx-translate/core';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage {
  public notificationsList: Notification[] = [];
  public notificationsNewCount: number = 0;
  public notificationsNext: string = '';
  public user: User = null;
  public isMobile: boolean;
  public loading: any;

  constructor(
    public alertCtrl: AlertController,
    private router: Router,
    private _store: StoreService,
    private _postService: PostService,
    private _utils: UtilsService,
    private modalCtrl: ModalController,
    private platform: Platform,
    private translate: TranslateService,
    public loadingController: LoadingController,
    private toastController: ToastController,
    private userService: UserService
  ) {
    this.isMobile = this._store.get('isMobile');
    this.platform = platform;
  }

  ionViewWillEnter() {
    this._store.eventChangeNotificationsCount(0);
    this.initUser();
    this._store.set('newNotifications', false);
    // this.presentLoadingText(1000);
  }

  ionViewWillLeave() {
    this._store.eventChangeNotificationsCount(0);
    this.notificationsList = [];
    this.notificationsNewCount = 0;
    this.user = null;
  }

  initUser() {
    this.userService.getMyUser().subscribe((data: any) => {
      this.user = this._store.parserApiToModel('user', data);
      this.getNotifications();
    }, error => {
      console.error(error);
    });
  }

  async presentLoadingText(duration: number = null) {
    if (duration) {
      this.loading = await this.loadingController.create({
        message: this.translate.instant(''),
        duration: duration
      });
    } else {
      this.loading = await this.loadingController.create({
        message: this.translate.instant('')
      });
    }
    return await this.loading.present();
  }

  dismissLoadingText() {
    try {
      this.loading.dismiss();
    } catch (error) {
      console.error(error);
    }
  }

  getNotifications() {

    if(this.notificationsNext===null){ return; }
    this._postService.getNotifications(this.notificationsNext).subscribe((response: any) => {
      this.notificationsNext = response.next;
      response.results.forEach(notificationItem => {
        let notification: Notification = new Notification();
        notification.id = notificationItem.id;
        notification.status = notificationItem.status;
        notification.key = notificationItem.get_notification_info.key;
        notification.notificationType = notificationItem.get_notification_info.notification_type;
        switch (notificationItem.get_notification_info.key) {
          case 'SOMEONE-TAGGED-YOU':
            notification.title = this.translate.instant('NOTIFICATIONS_TAG');
            break;
          case 'SOMEONE-TAGGED-YOUR-PICTURE':
            notification.title = this.translate.instant('NOTIFICATIONS_TAG');
            break;
          case 'ENABLE_USER_TAG':
            notification.title = this.translate.instant('NOTIFICATIONS_TAG_CONTROL');
            break;
          case 'NEW-PICS':
            notification.title = this.translate.instant('NOTIFICATIONS_NEW_UPLOAD');
            break;
          case 'NEW_PICTURES_PLACE':
            notification.title = this.translate.instant('NOTIFICATIONS_NEW_PICTURES_PLACE');
            break;
        } 
        notification.linkPage = notificationItem.get_notification_info.linked_view;
        notification.createdAt = notificationItem.created_at;
        notification.image = notificationItem.get_picture_info;
        if (notificationItem.related_item_key != null) {
          notification.relatedItemKey = notificationItem.related_item_key;
        }else{
          notification.image = null;
        }
        if (notification.status === 'received') {
          notification.isNew = true;
          this.notificationsNewCount += 1;
          this.markNotificationAsReaded(notification.id);
        }
        this.notificationsList.push(notification);
      });
      // this._store.eventChangeNotificationsCount(0);
    }, error => { console.error(<any>error); });
  }

  markNotificationAsReaded(id) {
    this._postService.markNotificationAsReceived(id).subscribe((response: any) => {
    }, error => { console.error(<any>error); });
  }

  goToDetailPhoto(id) {
    if (!id) {
      this.presentAlertError();
      return;
    }
    this._store.eventChangeTimelineDetailPhotoActive(null);
    this.router.navigateByUrl('tabs/photo/' + id);
    this.modalCtrl.dismiss();
  }

  deleteNotification(notification: Notification, withToast: boolean=true) {
    notification.readed = true;
    this._postService.markNotificationAsReaded(notification.id).subscribe(
      response => {
        this.notificationsList = this.notificationsList.filter(noti => noti.id != notification.id);
        if(withToast){
          this.presentToastDeleted();
        }
      }, error => { console.error(<any>error); }
    );
  }

  markAllNotificationsAsReaded() {
    this._postService.deleteAllNotifications().subscribe(
      response => {
        this.notificationsList = [];
        this.presentToastDeletedAll();
      }, error => { console.error(<any>error); }
    );
  }


  async presentToastDeleted() {
    const toast = await this.toastController.create({
      message: this.translate.instant('NOTIFICATIONS_DELETE_SINGLE'),
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  async presentToastDeletedAll() {
    const toast = await this.toastController.create({
      message: this.translate.instant('NOTIFICATIONS_REMOVE_ACTION'),
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  async presentToastAcceptUserTag() {
    const toast = await this.toastController.create({
      message: this.translate.instant('NOTIFICATIONS_TAG_ACCEPTED'),
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  async presentToastRejectUserTag() {
    const toast = await this.toastController.create({
      message: this.translate.instant('NOTIFICATIONS_TAG_REJECTED'),
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  closeModal() {
    this.modalCtrl.dismiss();
  }

  acceptTag(notification: Notification) {
    let dataToSend: any = {
      'picture': notification.relatedItemKey,
      'status': 'accept'
    };
    this._postService.acceptUserTag(dataToSend).subscribe((response: any) => {
      this.presentToastAcceptUserTag();
      this.deleteNotification(notification, false);
    });
  }
  rejectTag(notification: Notification) {
    let dataToSend: any = {
      'picture': notification.relatedItemKey,
      'status': 'reject'
    };
    this._postService.acceptUserTag(dataToSend).subscribe((response: any) => {
      this.presentToastRejectUserTag();
      this.deleteNotification(notification, false);
    });

    this.deleteNotification(notification);
  }

  async presentAlertError(titleM: boolean = false, locationM: boolean = false, dateM: boolean = false) {
    const alert = await this.alertCtrl.create({
      message: this.translate.instant('NOTIFICATIONS_PICTURE_NOT_FOUND'),
      buttons: [
        {
          text: this.translate.instant('GENERIC_POPUP_BUTTON_OK'),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {}
        }
      ]
    });
    await alert.present();
  }




  doInfinite(infiniteScroll) { // next
    this.getNotifications();
    setTimeout(() => { infiniteScroll.target.complete(); }, 1000);
  }

}
