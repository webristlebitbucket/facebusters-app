import { Subscription } from 'rxjs/Subscription';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from './../../providers/user-service/user-service';
import { Router, NavigationEnd } from '@angular/router';
import { PostService } from './../../providers/post-service/post-service';
import { StoreService } from './../../providers/store/store';
import { User, Photo, Album, Location, HashTag } from './../../models/models';
import { Component, OnInit } from '@angular/core';
import { LoadingController, Events } from '@ionic/angular';
@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit {

  public segmentSelected: string = 'albums' || 'photos' || 'tagged';
  public user: User = null;

  public photos: Photo[] = [];
  public photosPrivate: Photo[] = [];
  public photosTagged: Photo[] = [];
  public nextPhotos: string = null;
  public nextPhotosPrivate: string = null;
  public nextPhotosTagged: string = null;

  public albums: Album[] = [];
  public albumsPrivate: Album[] = [];
  public nextAlbums: string = null;
  public nextAlbumsPrivate: string = null;

  public isPrivate: boolean = false;
  private loading: any;

  public firstTimeAlbums: boolean = true
  public firstTimePhotos: boolean = true;
  public firstTimeAlbumsPrivate: boolean = true;
  public firstTimePhotosPrivate: boolean = true;
  public firstTimePhotosTagged: boolean = true;

  private myUserSubscribe: Subscription;

  constructor(
    private router: Router,
    private _store: StoreService,
    private _postService: PostService,
    private loadingController: LoadingController,
    private userService: UserService,
    private translate: TranslateService,
    private events: Events
  ) {
    this.segmentSelected = 'photos';
    this.myUserSubscribe = this._store.$myUser.subscribe((user: User) => {
      this.user = user;
      if (!this.user) {
        this.initUser();
      } else {
        this.user._fullName();
      }
    });

    this.events.subscribe('refresh-my-profile',() => {
      this.doRefresh(null);
    });
  }

  ngOnInit() {
    this.doRefresh(null);
  }

  ionViewWillEnter() {
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: this.translate.instant('')
    });
    return await this.loading.present();
  }

  setPublicOrPrivate(isPrivate: boolean = false) {
    this.isPrivate = isPrivate;
  }

  setSegmentSelected(value: string) {
    this.segmentSelected = value;
  }

  segmentChanged(event) {
    this.setSegmentSelected = event.detail.value;
  }

  initUser() {
    this.user = this._store.myUser;
    if(!this.user || this.user===undefined){
      this.userService.getMyUser().subscribe((data: any) => {
        this.user = this._store.parserApiToModel('user', data);
        this._store.eventChangeMyUser(this.user);
      }, error => {
        console.error(error);
      });
    } else {
    }
  }

  initPhoto(data: any) {
    const photo = new Photo();
    photo.id = data.identifier;
    photo.title = data.title;
    if (data.image) { photo.image = data.image; }
    if (data.image_thumbnail) { photo.thumbnail = data.image_thumbnail; }
    photo.isPublic = data.public;
    photo.hashTags = [];
    data.image_tags.forEach(t => {
      const hashtag = new HashTag();
      hashtag.hashTag = t;
      photo.hashTags.push(hashtag);
    });
    photo.location = new Location();
    photo.location.address = data.address_en;
    photo.createdAt = data.created_at;
    photo.user = this.user;
    photo.metaDate = data.meta_date;
    photo.filter = data.picture_filter;
    if (!photo.filter) { photo.filter = ''; }
    return photo;
  }

  initAlbum(data: any) {
    const album = new Album();
    album.id = data.identifier;
    album.pk = data.id;
    album.dateRange = data.date_range;
    album.title = data.title;
    album.location = new Location();
    album.location.address = data.address_en;
    album.photos = [];
    album.user = this.user;
    album.createdAt = data.created_at;
    data.latest_pictures.forEach(p => {
      const photo = new Photo();
      photo.id = p.identifier;
      if (p.image) { photo.image = p.image; }
      if (p.image_thumbnail) { photo.thumbnail = p.image_thumbnail; }
      photo.title = p.title;
      photo.isPublic = p.public;
      photo.createdAt = p.created_at;
      photo.user = this.user;
      photo.filter = data.picture_filter;
      if (!photo.filter) { photo.filter = ''; }
      album.photos.push(photo);
    });
    return album;
  }

  initPhotos() {
    this.photos = [];
    this.photosPrivate = [];
    this.nextPhotos = null;
    this.nextPhotosPrivate = null;
    this.firstTimePhotos = true;
    this.firstTimePhotosPrivate = true;
    this.getPhotos();
  }
  initAlbums() {
    this.albums = [];
    this.albumsPrivate = [];
    this.nextAlbums = null;
    this.nextAlbumsPrivate = null;
    this.firstTimeAlbums = true;
    this.firstTimeAlbumsPrivate = true;
    this.getAlbums();
  }
  initPhotosTagged() {
    this.photosTagged = [];
    this.nextPhotosTagged = null;
    this.firstTimePhotosTagged = true;
    this.getPhotosTagged();
  }

  goToDetailPhoto(photo: Photo) {
    // this._store.eventChangeTimelineDetailPhotoActive(photo);
    this.router.navigateByUrl('tabs/photo/' + photo.id );
  }

  goToDetailAlbum(album: Album) {
    this.router.navigateByUrl('tabs/album/' + album.id + '/' + album.pk);
  }

  getPhotos() {
    if (!this.nextPhotos && !this.firstTimePhotos) { return; }
    this._postService.getMyPublicPictures(this.nextPhotos).subscribe((data: any) => {
      this.firstTimePhotos = false;
      this.nextPhotos = data.next;
      if (data.count > 0) {
        data.results.forEach(e => {
          if (!e.hide) {
            this.photos.push(this.initPhoto(e));
          }
        });
      }
    }, error => { console.error(error); });
    if (!this.nextPhotosPrivate && !this.firstTimePhotosPrivate) { return; }
    this._postService.getMyPrivatePictures(this.nextPhotosPrivate).subscribe((data: any) => {
      this.firstTimePhotosPrivate = false;
      this.nextPhotosPrivate = data.next;
      if (data.count > 0) {
        data.results.forEach(e => {
          if (!e.hide) {
            this.photosPrivate.push(this.initPhoto(e));
          }
        });
      }
    }, error => { console.error(error); });
  }

  getAlbums() {
    if (!this.nextAlbums && !this.firstTimeAlbums) { return; }
    this._postService.getMyAlbums(this.nextAlbums).subscribe((data: any) => {
      this.firstTimeAlbums = false;
      this.nextAlbums = data.next;
      if (data.count > 0) {
        data.results.forEach(e => {
          if (e.latest_pictures.length > 0) {
            this.albums.push(this.initAlbum(e));
          }
        });
      }
    }, error => { console.error(error); });
    if (!this.nextAlbumsPrivate && !this.firstTimeAlbumsPrivate) { return; }
    this._postService.getMyPrivateAlbums(this.nextAlbumsPrivate).subscribe((data: any) => {
      this.firstTimeAlbumsPrivate = false;
      this.nextAlbumsPrivate = data.next;
      if (data.count > 0) {
        data.results.forEach(e => {
          if (e.latest_pictures.length > 0) {
            this.albumsPrivate.push(this.initAlbum(e));
          }
        });
      }
    }, error => { console.error(error); });
  }

  getPhotosTagged() {
    if (!this.nextPhotosTagged && !this.firstTimePhotosTagged) { return; }
    this._postService.getPicturesTagged(null, this.nextPhotosTagged).subscribe((data: any) => {
      this.firstTimePhotosTagged = false;
      this.nextPhotosTagged = data.next;
      if (data.count > 0) {
        data.results.forEach(e => {
          if (!e.hide) {
            this.photosTagged.push(this.initPhoto(e));
          }
        });
      }
    }, error => { console.error(error); });
  }

  doInfinite(event) {
    this.getAlbums();
    this.getPhotos();
    this.getPhotosTagged();
    setTimeout(() => { try { event.target.complete(); } catch (error) { } }, 1000);
  }

  doRefresh(event) {
    this.albums = [];
    this.albumsPrivate = [];
    this.photos = [];
    this.photosPrivate = [];
    this.photosTagged = [];
    this.nextAlbums = null;
    this.nextAlbumsPrivate = null;
    this.nextPhotos = null;
    this.nextPhotosPrivate = null;
    this.nextPhotosTagged = null;
    this.firstTimeAlbums = true;
    this.firstTimeAlbumsPrivate = true;
    this.firstTimePhotos = true;
    this.firstTimePhotosPrivate = true;
    this.firstTimePhotosTagged = true;
    this.getAlbums();
    this.getPhotos();
    this.getPhotosTagged();
    setTimeout(() => { try { event.target.complete(); } catch (error) { } }, 1000);
  }

}
