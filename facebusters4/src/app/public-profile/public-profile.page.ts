import { LoadingController } from '@ionic/angular';
import { UserService } from './../../providers/user-service/user-service';
import { ActivatedRoute, Router } from '@angular/router';
import { PostService } from './../../providers/post-service/post-service';
import { StoreService } from './../../providers/store/store';
import { User, Photo, Album, Location, HashTag } from './../../models/models';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-public-profile',
  templateUrl: './public-profile.page.html',
  styleUrls: ['./public-profile.page.scss'],
})
export class PublicProfilePage implements OnInit{

  public segmentSelected: string = 'albums' || 'photos' || 'tagged';
  public user: User = null;
  private idUser: string = null;

  public photos: Photo[] = [];
  public nextPhotos: string = null;

  public albums: Album[] = [];
  public albumsPrivate: Album[] = [];
  public nextAlbums: string = null;
  public nextAlbumsPrivate: string = null;

  public photosTagged: Photo[] = [];
  public nextPhotosTagged: string = null;

  public isLoading: boolean = true;
  private loading: any;

  public firstTime: boolean = true;
  public firstTimePhotos: boolean = true;
  public firstTimePhotosTagged: boolean = true;

  constructor(
    private router: Router,
    private _store: StoreService,
    private _postService: PostService,
    private activatedRoute: ActivatedRoute,
    private _userService: UserService,
    private loadingController: LoadingController,
    private translate: TranslateService
  ) {
    this.segmentSelected = 'photos';
  }

  ngOnInit(){
    this.isLoading = true;
    this.user = null;
    this.photos = [];
    this.albums = [];
    this.nextPhotos = null;
  }

  ionViewWillEnter() {
    this.user = null;
    this.photos = [];
    this.albums = [];
    this.nextPhotos = null;
    this.idUser = this.activatedRoute.snapshot.paramMap.get('id');
    this.initUser(this.idUser);
    this.initPhotos();
    this.initAlbums();
    this.initPhotosTagged();
  }


  ionViewWillLeave() {
    this.isLoading = true;
    this.user = null;
    this.photos = [];
    this.albums = [];
    this.nextPhotos = null;
    this.nextAlbums = null;
  }

  async presentLoading() {
    this.isLoading = true;
    this.loading = await this.loadingController.create({
      message: this.translate.instant('Loading...')
    });
    return await this.loading.present();
  }

  dismissLoading() {
    this.isLoading = false;
  }

  setSegmentSelected(value: string) {
    this.segmentSelected = value;
  }

  segmentChanged(event) {
    this.setSegmentSelected = event.detail.value;
  }

  initUser(id: any) {
    this._userService.getPublicProfile(id).subscribe((data: any) => {
      this.user = new User();
      this.user.id = id;
      const parseUser = {
        'id': 'id',
        'identifier': 'id',
        'username': 'nickname',
        'avatar_thumbnail': 'avatar',
        'email': 'email',
        'first_name': 'firstName',
        'last_name': 'lastName',
        'num_albums': 'numAlbums',
        'num_connections': 'numConnections',
        'num_followers': 'numFollowers',
        'num_following': 'numFollowing',
        'num_pictures': 'numPictures',
        'num_tagged_in': 'numTaggedIn',
        'following': 'following'
      }
      for (let k in data) { if (parseUser.hasOwnProperty(k)) { this.user[parseUser[k]] = data[k]; } }
      this.isLoading = false;
    }, error => { console.error(error); });
  }

  initPhoto(data: any) {
    const photo = new Photo();
    photo.id = data.identifier;
    photo.title = data.title;
    if (data.image) { photo.image = data.image; }
    if (data.image_thumbnail) { photo.thumbnail = data.image_thumbnail; }
    photo.isPublic = data.public;
    photo.hashTags = [];
    data.image_tags.forEach(t => {
      const hashtag = new HashTag();
      hashtag.hashTag = t;
      photo.hashTags.push(hashtag);
    });
    photo.location = new Location();
    photo.location.address = data.address_en;
    photo.createdAt = data.created_at;
    photo.user = this.user;
    photo.metaDate = data.meta_date;
    photo.filter = data.picture_filter;
    if (!photo.filter) { photo.filter = ''; }
    return photo;
  }

  initAlbum(data: any) {
    const album = new Album();
    album.id = data.identifier;
    album.pk = data.id;
    album.title = data.title;
    album.dateRange = data.date_range;
    album.location = new Location();
    album.location.address = data.address_en;
    album.photos = [];
    album.user = this.user;
    album.createdAt = data.created_at;
    data.latest_pictures.forEach(p => {
      const photo = new Photo();
      photo.id = p.identifier;
      if (p.image) { photo.image = p.image; }
      if (p.image_thumbnail) { photo.thumbnail = p.image_thumbnail; }
      photo.title = p.title;
      photo.isPublic = p.public;
      photo.createdAt = p.created_at;
      photo.user = this.user;
      photo.filter = data.picture_filter;
      if (!photo.filter) { photo.filter = ''; }
      album.photos.push(photo);
    });
    return album;
  }

  initPhotos() {
    this.photos = [];
    this.nextPhotos = null;
    this.firstTimePhotos = true;
    this.firstTime = true;
    this.getPhotos();
  }
  initAlbums() {
    this.albums = [];
    this.nextAlbums = null;
    this.firstTime = true;
    this.getAlbums();
  }
  initPhotosTagged() {
    this.photosTagged = [];
    this.nextPhotosTagged = null;
    this.firstTimePhotosTagged = true;
    this.getPhotosTagged();
  }

  getPhotos() {
    if (!this.nextPhotos && !this.firstTimePhotos) { return; }
    this._postService.getPicturesOfUser(this.idUser, this.nextPhotos).subscribe((data: any) => {
      this.firstTimePhotos = false;
      this.nextPhotos = data.next;
      if (data.count > 0) {
        data.results.forEach(e => { this.photos.push(this.initPhoto(e)); });
      }
    }, error => { console.error(error); });
  }
  getAlbums() {
    if (!this.nextAlbums && !this.firstTime) { return; }
    this._postService.getAlbumsOfUser(this.idUser, this.nextAlbums).subscribe((data: any) => {
      this.firstTime = false;
      this.nextAlbums = data.next;
      if (data.count > 0) {
        data.results.forEach(e => {
          if (!e.private && e.latest_pictures.length > 0) {
            this.albums.push(this.initAlbum(e));
          } else if (e.private && e.latest_pictures.length > 0) {
            this.albumsPrivate.push(this.initAlbum(e));
          }
        });
      }
    }, error => { console.error(error); });
  }
  getPhotosTagged() {
    if (!this.nextPhotosTagged && !this.firstTimePhotosTagged) { return; }
    this._postService.getPicturesTagged(this.idUser, this.nextPhotosTagged).subscribe((data: any) => {
      this.firstTimePhotosTagged = false;
      this.nextPhotosTagged = data.next;
      if (data.count > 0) {
        data.results.forEach(e => { this.photosTagged.push(this.initPhoto(e)); });
      }
    }, error => { console.error(error); });
  }


  goToDetailPhoto(photo: Photo) {
    this._store.eventChangeTimelineDetailPhotoActive(photo);
    this.router.navigateByUrl('tabs/photo/' + photo.id);
  }

  goToDetailAlbum(album: Album) {
    this.router.navigateByUrl('tabs/album/' + album.id+'/'+ album.pk);
  }

  doInfinite(event) {
    this.getAlbums();
    this.getPhotos();
    this.getPhotosTagged();
    setTimeout(() => { try { event.target.complete(); } catch (error) { } }, 1000);
  }

  doRefresh(event) {
    this.albums = [];
    this.photos = [];
    this.photosTagged = [];
    this.nextAlbums = null;
    this.nextPhotos = null;
    this.firstTime = true;
    this.firstTimePhotos = true;
    this.firstTimePhotosTagged = true;
    this.getAlbums();
    this.getPhotos();
    this.getPhotosTagged();
    setTimeout(() => { try { event.target.complete(); } catch (error) { } }, 1000);
  }

}
