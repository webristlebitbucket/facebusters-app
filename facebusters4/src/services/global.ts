import { User } from './../models/models';
import { Injectable } from '@angular/core';

@Injectable()
export class GlobalService {

  private myUser: User;

  constructor() {
    this.myUser = this.initMyUser();
  }

  initMyUser() {
    let user: User = new User();
    return user;
  }

  getMyUser() {
    return this.myUser;
  }

}
