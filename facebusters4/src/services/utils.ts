import { Injectable } from '@angular/core';
import { Photo, Album, User, Tag, Comment, HashTag, Location } from "../models/models";
@Injectable()
export class UtilsService {
  private pathAssets: string = 'assets/';
  private pathsImages: string = this.pathAssets + 'imgs/';

  private staticIMGS: any = {
    'logo': this.pathsImages + 'logo.jpg',
    'fake': this.pathsImages + 'fake.png',
    'fake1': this.pathsImages + 'fake/ironman.jpg',
    'fake2': this.pathsImages + 'fake/skeleton.jpg'
  }

  private filters: string [] = [
    'no-filter', '_1977', 'aden', 'amaro', 'brannan', 'brooklyn',
    'clarendon', 'gingham', 'hudson', 'inkwell',
    'kelvin', 'lark', 'lofi', 'mayfair', 'moon',
    'nashville', 'perpetua', 'reyes', 'rise', 'slumber',
    'stinson', 'toaster', 'valencia', 'walden', 'willow', 'xpro2'
  ]

  constructor() { }

  getFilters(){
    return this.filters;
  }
  getImgs() {
    return this.staticIMGS;
    ;
  }



  initUser(data: any) {
    let user: User = new User();
    user = new User();
    if (data) {
      user.avatar = data.avatar_thumbnail;
      user.nickname = data.username;
      user.id = data.identifier;
    }
    return user;
  }

  initLocation(data: any) {
    let location: Location = new Location();
    location.address = data.address_en;
    location.latitude = data.meta_location.latitude;
    location.longitude = data.meta_location.longitude;
    return location;
  }

  initHastags(data: any[]) {
    let hashTags: HashTag[] = [];
    data.forEach(t => {
      const hashtag = new HashTag();
      hashtag.hashTag = t;
      hashTags.push(hashtag);
    });
    return hashTags;
  }

  initPhoto(data: any) {
    const photo = new Photo();
    photo.id = data.identifier;
    photo.title = data.title;
    photo.image = data.image;
    photo.thumbnail = data.image_thumbnail;
    photo.isPublic = data.public;
    photo.createdAt = data.created_at;
    photo.hashTags = this.initHastags(data.image_tags);
    photo.location = this.initLocation(data);
    photo.user = this.initUser(data.user_info);
    photo.filter = data.picture_filter;
    photo.metaDate = data.meta_date;
    if (!photo.filter) { photo.filter = ''; }
    return photo;
  }

  initPhotosToAlbum(data: any[]) {
    let photos: Photo[] = [];
    data.forEach(p => { photos.push(this.initPhoto(p)) });
    return photos;
  }
  
  initAlbum(data: any) {
    const album = new Album();
    album.id = data.identifier;
    album.title = data.title;
    album.isPublic = data.public;
    album.createdAt = data.created_at;
    album.photos = this.initPhotosToAlbum(data.latest_pictures);
    album.location = this.initLocation(data);
    album.user = this.initUser(data.user_info);
    return album;
  }
}
