// src/app/auth/auth.service.ts
import { Injectable } from '@angular/core';
import { StoreService } from './../providers/store/store';


@Injectable()
export class AuthService {
  constructor(
    private _store: StoreService
  ) {}

  public isAuthenticated(): boolean {
    this._store.set('token', localStorage.getItem('token')); // Set Token From LocalStorage
    if (this._store.get('token') == 'null' || this._store.get('token') == null) {
        // console.info("auth: No token - you can not access");
        return false;
    } else {
        // console.info("auth: token - you can access", this._store.get('token'));
        return true;
    }
  }
}