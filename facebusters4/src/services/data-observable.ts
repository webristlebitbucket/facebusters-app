import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class DataService {


  private search_filters = new BehaviorSubject<any>({});
  currentSearchFilters = this.search_filters.asObservable();

  private search_results = new BehaviorSubject<any>({});
  currentSearchResults = this.search_results.asObservable();

  constructor() { }


  updateSearchFilters(data: any){
    this.search_filters.next(data);
  }

  updateSearchResults(data: any){
    this.search_results.next(data);
  }

}
