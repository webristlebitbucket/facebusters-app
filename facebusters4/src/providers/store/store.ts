import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User, Connection, Photo, Album } from './../../models/models';
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable'
import { getCall } from "../api-calls/apicalls.service";
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class StoreService {

    private data: any = {
        'token': null,
        'tokenTouchId': null,
        'myUser': null,
        'accessWithTouchId': false,
        'photos': [],
        'albums': [],
        'users': [],
        'isMobile': true,
        'notificationsCount': 0,
        'newNotifications': null
    }

    public filters: any = null;
    public notificationsCount: number;
    public $notificationsCount: any;
    private notificationsCountInterval: number = 30000;

    public timelineDetailPhotoActive: Photo;
    public $timelineDetailPhotoActive: any;

    public language: string;
    public $language: any;

    public myUser: User;
    public $myUser: any;

    public idOneSignal: any = null;


    constructor(
        public platform: Platform,
        private translate: TranslateService,
        private _httpClient: HttpClient
    ) {
        if (!this.platform.is('mobile')) { this.data.isMobile = false; }
        this.$notificationsCount= new BehaviorSubject<number>(this.notificationsCount);
        this.$timelineDetailPhotoActive= new BehaviorSubject<Photo>(this.timelineDetailPhotoActive);
        this.$myUser= new BehaviorSubject<User>(this.myUser);
        this.$language= new BehaviorSubject<string>(this.language);

        setTimeout(() => {
            this.checkNotificationsCount();
        }, 5000);

        setInterval(() => {
            this.checkNotificationsCount();
        }, this.notificationsCountInterval);
    }

    checkNotificationsCount(){
        if(!this.data.token || this.data.token===null || this.data.token==='null'){ console.info('no token no notis'); return; }
        this.getNotificationsCount().subscribe((data:any) => {
            if(data.notification != this.notificationsCount){
                this.notificationsCount = data.notification;
                this.eventChangeNotificationsCount(this.notificationsCount);
            }
        }, error => { console.error(error); });
    }

    eventChangeNotificationsCount(count: number) {
        this.notificationsCount = count;
        this.$notificationsCount.next(this.notificationsCount);
    }
    eventChangeTimelineDetailPhotoActive(photo: Photo){
        this.timelineDetailPhotoActive = photo;
        this.$timelineDetailPhotoActive.next(this.timelineDetailPhotoActive);
    }
    eventChangeMyUser(user: User){
        this.myUser = user;
        this.set('myUser', user);
        this.$myUser.next(this.myUser);
    }
    eventChangeLanguage(language: string){
        this.language = language;
        this.$language.next(this.language);
    }

    resetAll() {
        this.data = {
            'token': null,
            'myUser': null
        }
    }

    closeSession() {
        this.data.token = null;
        this.data.myUser = null;
    }

    setPhoto(photo: Photo) {
        let exists: boolean = false;
        this.data.photos.forEach(el => {
            if (photo.id === el.id && !exists) {
                el = photo;
                exists = true;
            }
        });
        if (!exists) {
            this.data.photos.push(photo);
        }
    }
    setAlbum(album: Album) {
        let exists: boolean = false;
        this.data.albums.forEach(el => {
            if (album.id === el.id && !exists) {
                el = album;
                exists = true;
            }
        });
        if (!exists) {
            this.data.albums.push(album);
        }
    }

    getPhoto(id: string) {
        let photoReturn: Photo = null;
        this.data.photos.forEach(photo => {
            if (photo.id == id && !photoReturn) {
                photoReturn = photo;
            }
        });
        return photoReturn;
    }
    getAlbum(id: string) {
        let albumReturn: Album = null;
        this.data.albums.forEach(album => {
            if (album.id == id && !albumReturn) {
                albumReturn = album;
            }
        });
        return albumReturn;
    }

    getAll() {
        return this.data;
    }

    get(key) {
        return this.data[key];
    }

    set(key, value) {
        this.data[key] = value;
        return this.data[key];
    }

    // parsers
    parseUser = {
        'id': 'id',
        'identifier': 'id',
        'username': 'nickname',
        'avatar': 'avatar',
        'avatar_thumbnail': 'avatar',
        'email': 'email',
        'first_name': 'firstName',
        'last_name': 'lastName',
        'num_albums': 'numAlbums',
        'num_connections': 'numConnections',
        'num_followers': 'numFollowers',
        'num_following': 'numFollowing',
        'num_pictures': 'numPictures',
        'num_tagged_in': 'numTaggedIn',
        'language': 'language',
        'tag_control': 'tagControl'
    }

    parserApiToModel(key, data) {
        let dP, parse; // dataParsed
        switch (key) {
            case 'user':
                dP = new User();
                parse = this.parseUser;
                for (let k in data) { if (parse.hasOwnProperty(k)) { dP[parse[k]] = data[k]; } }
                if (dP['language']) {
                    this.translate.use(dP['language'].toLowerCase());
                }
                break;
            default:
                return data;
        }
        return dP;
    }


    getOptions() {
        return { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Token ' + this.get('token') }) };
    }
    getNotificationsCount() {
        return this._httpClient.get(getCall('notificationsCount', null, {}), this.getOptions()).pipe(map(r => r));
    }

}
