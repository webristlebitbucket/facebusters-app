import { StoreService } from './../store/store';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { getCall } from "../api-calls/apicalls.service";

@Injectable()
export class LoginService {

    constructor(public _httpClient: HttpClient, private _store:StoreService) { }

    getOptionsWithToken() {
        return { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Token ' + this._store.get('token') }) };
    }
    getOptions() {
        return { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    }

    getToken(data: any) {
        console.info('[API] getToken ', data);
        return this._httpClient.post(getCall('getToken'), data, this.getOptions()).pipe(map(r => r));
    }

    loginSocial(data: any) {
        return this._httpClient.post(getCall('loginSocial'), data, this.getOptions()).pipe(map(r => r));
    }

    resetPasswordSendCode(email: string) {
        const data = { 'email': email };
        return this._httpClient.post(getCall('sendCodeResetPassword'), data, this.getOptions()).pipe(map(r => r));
    }

    resetPassword(email: string, code: string, new_password: string) {
        const data = { 'email': email, 'code': code, 'new_password': new_password };
        return this._httpClient.post(getCall('resetPassword'), data, this.getOptions()).pipe(map(r => r));
    }

    subscribeDevice(player_id: string, status: string) { // status ==> add | remove
        const data = { 'player_id': player_id, 'status': status };
        return this._httpClient.post(getCall('subscribe_device'), data, this.getOptionsWithToken()).pipe(map(r => r));
    }

}
