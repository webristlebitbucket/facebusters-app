import { StoreService } from './../store/store';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { getCall } from '../api-calls/apicalls.service';
@Injectable()
export class PostService {
  constructor(public _httpClient: HttpClient, private _store: StoreService) { }
  getOptions() {
    return { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Token ' + this._store.get('token') }) };
  }
  getPublicOptions() {
    return { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  }
  getTimeLime(next: string = null) {
    if (!next) {
      return this._httpClient.get(getCall('getTimeLine', null, {}), this.getOptions()).pipe(map(r => r));
    } else {
      return this._httpClient.get(next, this.getOptions()).pipe(map(r => r));
    }
  }
  getMyPublicPictures(next: string = null) {
    if (!next) {
      return this._httpClient.get(getCall('getPictures', null, { 'public': '' }), this.getOptions()).pipe(map(r => r));
    } else {
      return this._httpClient.get(next, this.getOptions()).pipe(map(r => r));
    }
  }
  getMyPrivatePictures(next: string = null) {
    if (!next) {
      return this._httpClient.get(getCall('getPictures', null, { 'private': '' }), this.getOptions()).pipe(map(r => r));
    } else {
      return this._httpClient.get(next, this.getOptions()).pipe(map(r => r));
    }
  }
  getAlbum(idAlbum: string) {
    return this._httpClient.get(getCall('getAlbums', idAlbum, {}), this.getOptions()).pipe(map(r => r));
  }
  getMyAlbums(next: string = null) {
    if (!next) {
      return this._httpClient.get(getCall('getMyAlbums', null, { 'private': 'False' }), this.getOptions()).pipe(map(r => r));
    } else {
      return this._httpClient.get(next, this.getOptions()).pipe(map(r => r));
    }
  }
  getMyPrivateAlbums(next: string = null) {
    if (!next) {
      return this._httpClient.get(getCall('getMyAlbums', null, { 'private': 'True' }), this.getOptions()).pipe(map(r => r));
    } else {
      return this._httpClient.get(next, this.getOptions()).pipe(map(r => r));
    }
  }
  getAllPictures() {
    return this._httpClient.get(getCall('getPictures', null, { 'all': '' }), this.getOptions()).pipe(map(r => r));
  }
  getAllAlbums() {
    return this._httpClient.get(getCall('getAlbums', null, { 'all': '' }), this.getOptions()).pipe(map(r => r));
  }
  getPicturesOfUser(idUser: any, next: string = null) {
    if (!next) {
      return this._httpClient.get(getCall('getPictures', null, { 'user': idUser }), this.getOptions()).pipe(map(r => r));
    } else {
      return this._httpClient.get(next, this.getOptions()).pipe(map(r => r));
    }
  }
  getPicturesTagged(idUser: any= null, next: string = null) {
    if (!next) {
      if (idUser === null) {
        return this._httpClient.get(getCall('getPicturesTagged', null, { 'tagged_in': '' }), this.getOptions()).pipe(map(r => r));
      } else {
        // console.log('llamada ==> ', getCall('getPicturesTagged', null, { 'tagged_in':'', 'user':idUser }));
        return this._httpClient.get(getCall('getPicturesTagged', null, { 'tagged_in': '', 'user': idUser }), this.getOptions()).pipe(map(r => r));
      }
    } else {
      return this._httpClient.get(next, this.getOptions()).pipe(map(r => r));
    }
  }
  getPicture(idPicture: any) {
    return this._httpClient.get(getCall('getPictures', idPicture, {}), this.getOptions()).pipe(map(r => r));
  }
  getPicturesOfAlbum(idAlbum: any) {
    return this._httpClient.get(getCall('getPictures', null, { 'album': idAlbum }), this.getOptions()).pipe(map(r => r));
  }
  getAlbumsOfUser(idUser: any, next: string = null) {
    if (!next) {
      return this._httpClient.get(getCall('getAlbums', null, { 'user': idUser }), this.getOptions()).pipe(map(r => r));
    } else {
      return this._httpClient.get(next, this.getOptions()).pipe(map(r => r));
    }
  }
  getCommentsOfPicture(idPhoto: any) {
    return this._httpClient.get(getCall('getCommentsOfPhoto', null, { 'picture': idPhoto }), this.getOptions()).pipe(map(r => r));
  }
  newComment(data: any) {
    return this._httpClient.post(getCall('newComment'), data, this.getOptions()).pipe(map(r => r));
  }
  createNewAlbum(data: any) {
    return this._httpClient.post(getCall('createAlbum'), data, this.getOptions()).pipe(map(r => r));
  }
  createNewPicture(data: any) {
    return this._httpClient.post(getCall('createPicture'), data, this.getOptions()).pipe(map(r => r));
  }
  editPicture(id: any, data: any) {
    return this._httpClient.patch(getCall('editPicture', id), data, this.getOptions()).pipe(map(r => r));
  }
  editAlbum(id: any, data: any) {
    return this._httpClient.patch(getCall('editAlbum', id), data, this.getOptions()).pipe(map(r => r));
  }
  searchHashtag(nameHashtag: string) {
    return this._httpClient.get(getCall('searchHashTag', null, { 'text': nameHashtag }), this.getOptions()).pipe(map(r => r));
  }
  createHashtag(nameHashtag: string) {
    const data = { 'name': nameHashtag };
    return this._httpClient.post(getCall('createHashTag'), data, this.getOptions()).pipe(map(r => r));
  }
  addUserTag(data: any) {
    return this._httpClient.post(getCall('addUserTag'), data, this.getOptions()).pipe(map(r => r));
  }
  removeUserTag(data: any) {
    return this._httpClient.post(getCall('removeUserTag'), data, this.getOptions()).pipe(map(r => r));
  }
  getUserTags(idPhoto: string) {
    return this._httpClient.get(getCall('getUserTags', null, { 'picture': idPhoto }), this.getOptions()).pipe(map(r => r));
  }
  reportPhoto(data: any) {
    return this._httpClient.post(getCall('report'), data, this.getOptions()).pipe(map(r => r));
  }
  search(data: any) {
    return this._httpClient.get(getCall('search', null, data, false), this.getOptions()).pipe(map(r => r));
  }
  searchNext(next_url: string) {
    return this._httpClient.get(next_url, this.getOptions()).pipe(map(r => r));
  }
  getNotifications(next_url: string = null) {
    let url = next_url;
    if (!url) { url = getCall('notifications', null, {}); }
    return this._httpClient.get(url, this.getOptions()).pipe(map(r => r));
  }
  getNotificationsCount() {
    return this._httpClient.get(getCall('notificationsCount', null, {}), this.getOptions()).pipe(map(r => r));
  }
  markNotificationAsReaded(identifier) {
    return this._httpClient.patch(getCall('notifications', identifier, {}), { 'status': 'deleted' }, this.getOptions()).pipe(map(r => r));
  }
  markNotificationAsReceived(identifier) {
    return this._httpClient.patch(getCall('notifications', identifier, {}), { 'status': 'readed' }, this.getOptions()).pipe(map(r => r));
  }
  deleteAllNotifications() {
    const data = { 'status': 'deleted' };
    const url = getCall('delete_notifications', null, null);
    return this._httpClient.post(getCall('markNotifications', null, {}), data, this.getOptions()).pipe(map(r => r));
  }
  editMyProfile(id: string, data: any) {
    // first_name
    // last_name
    // email
    // password
    // avatar
    return this._httpClient.patch(getCall('editMyProfile', id), data, this.getOptions()).pipe(map(r => r));
  }
  registerUser(data: any) {
    return this._httpClient.post(getCall('registerAccount'), data, this.getPublicOptions()).pipe(map(r => r));
  }
  verifyAccount(data: any) {
    // verification_code
    return this._httpClient.post(getCall('verifyAccount'), data, this.getPublicOptions()).pipe(map(r => r));
  }
  acceptUserTag(data: any) {
    // verification_code
    return this._httpClient.post(getCall('acceptUserTag'), data, this.getOptions()).pipe(map(r => r));
  }
  checkEmailExists(email: string) {
    return this._httpClient.post(getCall('checkEmailExists'), {'email': email }, this.getPublicOptions()).pipe(map(r => r));
  }
  linkFacebook(data: any) {
    // TODO: update this reference
    // return this._httpClient.post(getCall('linkFacebook'), data, this.getOptions()).pipe(map(r => r));
  }

}
