import { StoreService } from './../store/store';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { getCall } from "../api-calls/apicalls.service";

@Injectable()
export class FollowService {

    constructor(public _httpClient: HttpClient, private _store: StoreService) { }

    getOptions() {
        return { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Token ' + this._store.get('token') }) };
    }
    getFollowers() {
        return this._httpClient.get(getCall('getFollowers', null, { 'followers': '' }), this.getOptions()).pipe(map(r => r));
    }
    getFollowing() {
        return this._httpClient.get(getCall('getFollowers', null, { 'following': '' }), this.getOptions()).pipe(map(r => r));
    }
    followUser(id: any){
        const data = { "user":id, "status":"follow" };
        return this._httpClient.post(getCall('followUser'), data, this.getOptions()).pipe(map(r => r));
    }
    unfollowUser(id: any){
        const data = { "user":id, "status":"unfollow" };
        return this._httpClient.post(getCall('unfollowUser'), data, this.getOptions()).pipe(map(r => r));
    }
}
