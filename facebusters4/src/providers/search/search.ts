import { StoreService } from './../store/store';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { getCall } from "../api-calls/apicalls.service";
@Injectable()
export class SearchAddressService {
    constructor(public _httpClient: HttpClient, private _store: StoreService) { }
    getOptions() {
        return { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Token ' + this._store.get('token') }) };
    }
    searchAddress(address: string = null) {
        return this._httpClient.get(getCall('searchAddressByCoords', null, {'address':address}), this.getOptions()).pipe(map(r => r));
    }
    searchAddressByCoords(coords: any){ //lat lon
        return this._httpClient.get(getCall('searchAddressByCoords', null, coords), this.getOptions()).pipe(map(r => r));
    }
    searchAddressByPlaceId(place_id:string){
        return this._httpClient.get(getCall('searchAddressByPlaceId', null, {'place_id': place_id}), this.getOptions()).pipe(map(r => r));
    }
}
