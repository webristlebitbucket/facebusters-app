import { ENV } from './../../environments/environment';
// NOTE: Dictionay of All Calls

const apiVersion = 'api/v1/';
export const calls = {
  'getToken': apiVersion + 'api-token-auth',
  'getMyUser': apiVersion + 'user',
  'getFollowers': apiVersion + 'user_follow',
  'getFollowing': apiVersion + 'user_follow',
  'followUser': apiVersion + 'user_follow',
  'unfollowUser': apiVersion + 'user_follow',
  'getPublicProfile': apiVersion + 'public_profile',
  'getMyPictures': apiVersion + 'picture',
  'getPicturesOfAlbum': apiVersion + 'picture',
  'getMyAlbums': apiVersion + 'album',
  'getPictures': apiVersion + 'picture',
  'getPicturesTagged': apiVersion + 'picture',
  'getAlbums': apiVersion + 'album',
  'getCommentsOfPhoto': apiVersion + 'comment',
  'newComment': apiVersion + 'comment',
  'searchUser': apiVersion + 'user_search',
  'createAlbum': apiVersion + 'album',
  'createPicture': apiVersion + 'picture',
  'editPicture': apiVersion + 'picture',
  'searchHashTag': apiVersion + 'hashtag',
  'createHashTag': apiVersion + 'hashtag',
  'addUserTag': apiVersion + 'usertag',
  'removeUserTag': apiVersion + 'usertag',
  'getUserTags': apiVersion + 'usertag',
  'getTimeLine': apiVersion + 'timeline',
  'report': apiVersion + 'report',
  'search': apiVersion + 'picture/?search',
  'searchAddressByCoords': apiVersion + 'get_address',
  'searchAddressByPlaceId': apiVersion + 'get_address',
  'notifications': apiVersion + 'notification',
  'notificationsCount': apiVersion + 'notification-count',
  'markNotifications': apiVersion + 'bulk-notification',
  'editMyProfile': apiVersion + 'user',
  'registerAccount': apiVersion + 'register',
  'verifyAccount': apiVersion + 'verify_account',
  'loginSocial': apiVersion + 'login_social',
  'acceptUserTag': apiVersion + 'accept_usertag',
  'sendCodeResetPassword': apiVersion + 'reset_password',
  'resetPassword': apiVersion + 'reset_password_from_mail_code',
  'editAlbum': apiVersion + 'album',
  'checkEmailExists': apiVersion + 'check_email_exists',
  'linkFacebook': apiVersion + 'link_facebook',
  'subscribe_device': apiVersion + 'subscribe_device'
}

export function getCall(call, id = null, params = {}, question = true) {
  let url = ENV.urlAPI + calls[call];
  if (question){
    url += '/';
  }
  if (id != '' && id != null && id != undefined) { url += id + '/' }
  if (params) {
    if (question) {
      url += '?';
    } else {
      url += '&';
    }
    for (let key in params) { url += key + '=' + params[key] + '&'; }
    url = url.slice(0, -1);
  }
  return url
}
