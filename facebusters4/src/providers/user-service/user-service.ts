import { StoreService } from './../store/store';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { getCall } from "../api-calls/apicalls.service";
@Injectable()
export class UserService {

  constructor(public _httpClient: HttpClient, private _store: StoreService) { }

  getOptions() {
    return { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Token ' + this._store.get('token') }) };
  }

  getMyUser() {
    return this._httpClient.get(getCall('getMyUser'), this.getOptions()).pipe(map(r => r));
  }

  getPublicProfile(id: any) {
    return this._httpClient.get(getCall('getPublicProfile', id), this.getOptions()).pipe(map(r => r));
  }

  getAll() {
    // NOTE: Remove this
    return this._httpClient.get('https://randomuser.me/api/?results=10');
  }

  searchUser(txt: string){
    return this._httpClient.get(getCall('searchUser', null, {'text': txt}), this.getOptions()).pipe(map(r => r));
  }

}

