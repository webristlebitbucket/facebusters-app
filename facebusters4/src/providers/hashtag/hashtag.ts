import { StoreService } from './../store/store';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { getCall } from "../api-calls/apicalls.service";
@Injectable()
export class SearchHashtagService {
    constructor(public _httpClient: HttpClient, private _store: StoreService) { }
    getOptions() {
        return { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'Token ' + this._store.get('token') }) };
    }
    searchHashtag(hashtag: string = null) {
        const apiUrl: string = '';
        return this._httpClient.get(apiUrl).pipe(map(r => r));
    }
}
