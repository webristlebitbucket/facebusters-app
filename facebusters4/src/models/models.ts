import * as moment from 'moment';
export class User {
  constructor(
    public id: any = null,
    public nickname: string = '@nickname',
    public avatar: string = '', // avatar_thumbnail
    public email: string = null,
    public firstName: string = null,// first_name
    public lastName: string = null, // last_name
    public language: string = null, // language
    public numAlbums: number = null, // num_albums
    public numPictures: number = null, // num_pictures
    public numConnections: number = null, // num_albums
    public numFollowers: number = null, // num_followers
    public numFollowing: number = null, // num_following
    public numTaggedIn: number = null, // num_tagged_in,
    public following: boolean = false,
    public connected: boolean = false,
    public tagControl: boolean = false,
    public _identifier: any = null,
    public _fullName = function (){
      return this.firstName + ' ' + this.lastName;
    },
    public _numPublications = function (){
      return this.numPictures;
    }
  ) {}
}
export class TagCoords{
  constructor(
    public row: number = null,
    public col: number = null
  ) {}
}
export class Tag {
  constructor(
    public id: any = null,
    public user: User = null,
    public nonUser: string = null,
    public coords: TagCoords = new TagCoords(),
    public hide: boolean = false,
    public _isNew: boolean = false,
    public createdAt: string = null
  ) { }
}

export class Comment {
  constructor(
    public id: any = null,
    public user: User = null,
    public comment: string = null ,
    public createdAt: string = null,
    public _dateToShow = function (){
      try{
        //2018-07-24 T13:38:49.011562+02:00
        return this.createdAt.slice(0, 10) + ' ' + this.createdAt.substring(11,19);
      }
      catch(err){ return this.createdAt }
    }
  ) { }
}

export class HashTag {
  constructor(
    public id: any = null,
    public hashTag: string = null,
    public createdAt: string = null,
    public _hashTagShow = function (){
      try{
        return '#'+ this.hashTag.replace('#','');
      }
      catch(err){ return this.createdAt }
    }
  ) { }
}

export class Location {
  constructor(
    public id: any = null,
    public address: string = null,
    public latitude: string = null,
    public longitude: string = null,
    public date: any = null
  ) { }
}

export class Photo {
  constructor(
    public id: any = null,
    public user: User = new User(),
    public title: string = null,
    public image: string = '',
    public thumbnail: string = '',
    public tags: Tag[] = [],
    public comments: Comment[] = [],
    public hashTags: HashTag[] = [],
    public location: Location = new Location(),
    public meta: any = null,
    public metaDate: any = null,
    public filter: any = '',
    public isPublic: boolean = true,
    public order: number = 0,
    public createdAt: string = null,
    public _imageB64: any = null,
    public _hashtags: string[] = [],
    public _date: any = new Date().toLocaleString().replace(' ','T').replace('/','-').replace('/','-'),
    public _time: any = new Date().toLocaleString().replace(' ','T').replace('/','-').replace('/','-'),
    public _datetime: any = moment().format(),
    public _origin: string = null,
    public _hasMeta: boolean = null,
    public _dateToShow = function (){
      try{
        //2018-07-24 T13:38:49.011562+02:00
        return this.metaDate;
        // return this.createdAt.slice(0, 10) + ' ' + this.createdAt.substring(11,19);
      }
      catch(err){ return this.createdAt }
    },
    public _itemType = function(){
      return 'picture';
    }
  ) { }
}

export class Album {
  constructor(
    public id: any = null,
    public pk: any = null,
    public title: string = null,
    public user: User = null,
    public photos: Photo[] = [],
    public location: Location = null,
    public isPublic: boolean = true,
    public createdAt: string = null,
    public updatedAt: string = null,
    public dateRange: string = null,
    public _dateToShow = function (){
      try{
        //2018-07-24 T13:38:49.011562+02:00
        return this.createdAt.slice(0, 10) + ' ' + this.createdAt.substring(11,19);
      }
      catch(err) { return this.createdAt }
    },
    public _itemType = function(){
      return 'album';
    }
  ) { }
}
export class Post {
  constructor(
    public id: any = null,
    public title: string = null,
    public postType: string = null,
    public isPublic: boolean = true,
    public photos: Photo[] = [],
    public user: User = null,
    public location: Location = null,
    public createdAt: string = null,
    public _date: any = new Date().toISOString(),
    public _time: any = new Date().toISOString()
  ) { }
}
export class Notification {
  constructor(
    public id: any = null,
    public key: string = null,
    public relatedItemKey: string = null,
    public notificationType: string = null,
    public title: string = null,
    public linkPage: string = null,
    public description: string = null,
    public user: User = null,
    public readed: boolean = false,
    public status: string = null,
    public createdAt: string = null,
    public image: string = null,
    public photo: Photo = null,
    public isNew: boolean = false,
    public _dateToShow = function () {
      try{
        // 2018-07-24 T13:38:49.011562+02:00
        return this.createdAt.slice(0, 10) + ' ' + this.createdAt.substring(11,19);
      } catch(err) { return this.createdAt; }
    }
  ) { }
}
  export class Connection {
    constructor(
      public id: any = null,
      public connectionType: string = 'following' || 'follower' || 'connection',
      public user: User = null,
      public fb_follow: boolean = false,
      public createdAt: string = null
    ) { }
}
