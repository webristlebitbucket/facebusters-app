export const ENV = {
  production: true,
  mode: 'Production',
  urlAPI: 'http://api.facebusters.com/',
  network: false,
  reversedIdGoogleIos: 'com.googleusercontent.apps.592496334026-4c867jnng3atcu3nb8rmkrn2kpmgbn59'
};